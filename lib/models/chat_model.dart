import 'package:freezed_annotation/freezed_annotation.dart';

import 'chat_message_model.dart';
import 'chat_user_participant_model.dart';

part 'chat_model.freezed.dart';

part 'chat_model.g.dart';

@freezed
class ChatEntity with _$ChatEntity {
  factory ChatEntity({
    required int id,
    required String? name,
    @JsonKey(name: "is_private") required int is_private,
    @JsonKey(name: "created_at") required String created_at,
    @JsonKey(name: "updated_at") required String updated_at,
    @JsonKey(name: "last_message") ChatMessageEntity? last_message,
    required List<ChatParticipantEntity> participants,
  }) = _ChatEntity;

  factory ChatEntity.fromJson(Map<String, dynamic> json) =>
      _$ChatEntityFromJson(json);
}
