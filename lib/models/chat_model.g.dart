// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ChatEntity _$$_ChatEntityFromJson(Map<String, dynamic> json) =>
    _$_ChatEntity(
      id: json['id'] as int,
      name: json['name'] as String?,
      is_private: json['is_private'] as int,
      created_at: json['created_at'] as String,
      updated_at: json['updated_at'] as String,
      last_message: json['last_message'] == null
          ? null
          : ChatMessageEntity.fromJson(
              json['last_message'] as Map<String, dynamic>),
      participants: (json['participants'] as List<dynamic>)
          .map((e) => ChatParticipantEntity.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ChatEntityToJson(_$_ChatEntity instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'is_private': instance.is_private,
      'created_at': instance.created_at,
      'updated_at': instance.updated_at,
      'last_message': instance.last_message,
      'participants': instance.participants,
    };
