class ChatModel {
  Data? data;
  bool? success;
  String? message;

  ChatModel({this.data, this.success, this.message});

  ChatModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    success = json['success'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['success'] = this.success;
    data['message'] = this.message;
    return data;
  }
}

class Data {
  int? id;
  int? createdBy;
  String? name;
  int? isPrivate;
  String? createdAt;
  String? updatedAt;
  List<Participants>? participants;
  LastMessage? lastMessage;

  Data(
      {this.id,
      this.createdBy,
      this.name,
      this.isPrivate,
      this.createdAt,
      this.updatedAt,
      this.participants,
      this.lastMessage});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdBy = json['created_by'];
    name = json['name'];
    isPrivate = json['is_private'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['participants'] != null) {
      participants = <Participants>[];
      json['participants'].forEach((v) {
        participants!.add(new Participants.fromJson(v));
      });
    }
    lastMessage = json['last_message'] != null
        ? new LastMessage.fromJson(json['last_message'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['created_by'] = this.createdBy;
    data['name'] = this.name;
    data['is_private'] = this.isPrivate;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.participants != null) {
      data['participants'] = this.participants!.map((v) => v.toJson()).toList();
    }
    if (this.lastMessage != null) {
      data['last_message'] = this.lastMessage!.toJson();
    }
    return data;
  }
}

class Participants {
  int? id;
  int? chatId;
  int? userId;
  String? createdAt;
  String? updatedAt;
  User? user;

  Participants(
      {this.id,
      this.chatId,
      this.userId,
      this.createdAt,
      this.updatedAt,
      this.user});

  Participants.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    chatId = json['chat_id'];
    userId = json['user_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['chat_id'] = this.chatId;
    data['user_id'] = this.userId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class User {
  int? id;
  String? name;
  String? email;
  int? category;
  int? age;
  int? year;
  int? isDoctor;
  String? photoPath;
  int? isAdmin;
  String? livestreamId;

  User(
      {this.id,
      this.name,
      this.email,
      this.category,
      this.age,
      this.year,
      this.isDoctor,
      this.photoPath,
      this.isAdmin,
      this.livestreamId});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    category = json['category'];
    age = json['age'];
    year = json['year'];
    isDoctor = json['isDoctor'];
    photoPath = json['photo_path'];
    isAdmin = json['isAdmin'];
    livestreamId = json['livestream_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['category'] = this.category;
    data['age'] = this.age;
    data['year'] = this.year;
    data['isDoctor'] = this.isDoctor;
    data['photo_path'] = this.photoPath;
    data['isAdmin'] = this.isAdmin;
    data['livestream_id'] = this.livestreamId;
    return data;
  }
}

class LastMessage {
  int? id;
  int? chatId;
  int? userId;
  String? message;
  String? createdAt;
  String? updatedAt;
  User? user;

  LastMessage(
      {this.id,
      this.chatId,
      this.userId,
      this.message,
      this.createdAt,
      this.updatedAt,
      this.user});

  LastMessage.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    chatId = json['chat_id'];
    userId = json['user_id'];
    message = json['message'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['chat_id'] = this.chatId;
    data['user_id'] = this.userId;
    data['message'] = this.message;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class User1 {
  int? id;
  String? name;
  String? email;
  int? category;
  int? age;
  int? year;
  int? isDoctor;
  String? photoPath;
  int? isAdmin;
  String? livestreamId;

  User1(
      {this.id,
      this.name,
      this.email,
      this.category,
      this.age,
      this.year,
      this.isDoctor,
      this.photoPath,
      this.isAdmin,
      this.livestreamId});

  User1.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    category = json['category'];
    age = json['age'];
    year = json['year'];
    isDoctor = json['isDoctor'];
    photoPath = json['photo_path'];
    isAdmin = json['isAdmin'];
    livestreamId = json['livestream_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['category'] = this.category;
    data['age'] = this.age;
    data['year'] = this.year;
    data['isDoctor'] = this.isDoctor;
    data['photo_path'] = this.photoPath;
    data['isAdmin'] = this.isAdmin;
    data['livestream_id'] = this.livestreamId;
    return data;
  }
}
