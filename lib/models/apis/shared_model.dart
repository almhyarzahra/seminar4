class SharedModel {
  String? data;
  bool? success;
  String? message;

  SharedModel({this.data, this.success, this.message});

  SharedModel.fromJson(Map<String, dynamic> json) {
    data = json['data'];
    success = json['success'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    data['success'] = this.success;
    data['message'] = this.message;
    return data;
  }
}
