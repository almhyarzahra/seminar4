class LaboratoryModel {
  List<String>? data;
  bool? success;
  String? message;

  LaboratoryModel({this.data, this.success, this.message});

  LaboratoryModel.fromJson(Map<String, dynamic> json) {
    data = json['data'].cast<String>();
    success = json['success'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    data['success'] = this.success;
    data['message'] = this.message;
    return data;
  }
}
