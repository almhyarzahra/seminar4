class CategoryUserModel {
  List<Data>? data;
  bool? success;
  String? message;

  CategoryUserModel({this.data, this.success, this.message});

  CategoryUserModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
    success = json['success'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    data['message'] = this.message;
    return data;
  }
}

class Data {
  int? id;
  String? name;
  String? email;
  int? category;
  int? age;
  int? year;
  int? isDoctor;
  String? photoPath;
  int? isAdmin;
  String? livestreamId;
  int? presence;
  int? absence;
  List<Subjects>? subjects;

  Data(
      {this.id,
      this.name,
      this.email,
      this.category,
      this.age,
      this.year,
      this.isDoctor,
      this.photoPath,
      this.isAdmin,
      this.livestreamId,
      this.presence,
      this.absence,
      this.subjects});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    category = json['category'];
    age = json['age'];
    year = json['year'];
    isDoctor = json['isDoctor'];
    photoPath = json['photo_path'];
    isAdmin = json['isAdmin'];
    livestreamId = json['livestream_id'];
    presence = json['presence'];
    absence = json['absence'];
    if (json['subjects'] != null) {
      subjects = <Subjects>[];
      json['subjects'].forEach((v) {
        subjects!.add(new Subjects.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['category'] = this.category;
    data['age'] = this.age;
    data['year'] = this.year;
    data['isDoctor'] = this.isDoctor;
    data['photo_path'] = this.photoPath;
    data['isAdmin'] = this.isAdmin;
    data['livestream_id'] = this.livestreamId;
    data['presence'] = this.presence;
    data['absence'] = this.absence;
    if (this.subjects != null) {
      data['subjects'] = this.subjects!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Subjects {
  int? id;
  int? year;
  int? duration;
  String? name;
  String? specialty;
  int? category;
  String? department;
  String? laboratory;
  int? dR;
  Pivot? pivot;

  Subjects(
      {this.id,
      this.year,
      this.duration,
      this.name,
      this.specialty,
      this.category,
      this.department,
      this.laboratory,
      this.dR,
      this.pivot});

  Subjects.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    year = json['year'];
    duration = json['duration'];
    name = json['name'];
    specialty = json['specialty'];
    category = json['category'];
    department = json['department'];
    laboratory = json['laboratory'];
    dR = json['DR'];
    pivot = json['pivot'] != null ? new Pivot.fromJson(json['pivot']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['year'] = this.year;
    data['duration'] = this.duration;
    data['name'] = this.name;
    data['specialty'] = this.specialty;
    data['category'] = this.category;
    data['department'] = this.department;
    data['laboratory'] = this.laboratory;
    data['DR'] = this.dR;
    if (this.pivot != null) {
      data['pivot'] = this.pivot!.toJson();
    }
    return data;
  }
}

class Pivot {
  int? userId;
  int? subjectId;
  String? createdAt;
  String? updatedAt;
  int? passed;
  int? presence;
  int? absence;

  Pivot(
      {this.userId,
      this.subjectId,
      this.createdAt,
      this.updatedAt,
      this.passed,
      this.presence,
      this.absence});

  Pivot.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    subjectId = json['subject_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    passed = json['passed'];
    presence = json['presence'];
    absence = json['absence'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['subject_id'] = this.subjectId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['passed'] = this.passed;
    data['presence'] = this.presence;
    data['absence'] = this.absence;
    return data;
  }
}
