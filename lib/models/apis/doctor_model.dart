class DoctorModel {
  List<Data>? data;
  bool? success;
  String? message;

  DoctorModel({this.data, this.success, this.message});

  DoctorModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
    success = json['success'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    data['message'] = this.message;
    return data;
  }
}

class Data {
  int? id;
  String? name;
  String? email;
  int? category;
  int? age;
  int? year;
  int? isDoctor;
  String? photoPath;
  int? isAdmin;
  String? livestreamId;

  Data(
      {this.id,
      this.name,
      this.email,
      this.category,
      this.age,
      this.year,
      this.isDoctor,
      this.photoPath,
      this.isAdmin,
      this.livestreamId});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    category = json['category'];
    age = json['age'];
    year = json['year'];
    isDoctor = json['isDoctor'];
    photoPath = json['photo_path'];
    isAdmin = json['isAdmin'];
    livestreamId = json['livestream_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['category'] = this.category;
    data['age'] = this.age;
    data['year'] = this.year;
    data['isDoctor'] = this.isDoctor;
    data['photo_path'] = this.photoPath;
    data['isAdmin'] = this.isAdmin;
    data['livestream_id'] = this.livestreamId;
    return data;
  }
}
