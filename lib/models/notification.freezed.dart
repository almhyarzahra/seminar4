// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'notification.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

notification _$notificationFromJson(Map<String, dynamic> json) {
  return _notification.fromJson(json);
}

/// @nodoc
mixin _$notification {
  String get senderName => throw _privateConstructorUsedError;
  String get message => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $notificationCopyWith<notification> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $notificationCopyWith<$Res> {
  factory $notificationCopyWith(
          notification value, $Res Function(notification) then) =
      _$notificationCopyWithImpl<$Res, notification>;
  @useResult
  $Res call({String senderName, String message});
}

/// @nodoc
class _$notificationCopyWithImpl<$Res, $Val extends notification>
    implements $notificationCopyWith<$Res> {
  _$notificationCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? senderName = null,
    Object? message = null,
  }) {
    return _then(_value.copyWith(
      senderName: null == senderName
          ? _value.senderName
          : senderName // ignore: cast_nullable_to_non_nullable
              as String,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_notificationCopyWith<$Res>
    implements $notificationCopyWith<$Res> {
  factory _$$_notificationCopyWith(
          _$_notification value, $Res Function(_$_notification) then) =
      __$$_notificationCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String senderName, String message});
}

/// @nodoc
class __$$_notificationCopyWithImpl<$Res>
    extends _$notificationCopyWithImpl<$Res, _$_notification>
    implements _$$_notificationCopyWith<$Res> {
  __$$_notificationCopyWithImpl(
      _$_notification _value, $Res Function(_$_notification) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? senderName = null,
    Object? message = null,
  }) {
    return _then(_$_notification(
      senderName: null == senderName
          ? _value.senderName
          : senderName // ignore: cast_nullable_to_non_nullable
              as String,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_notification implements _notification {
  const _$_notification({required this.senderName, required this.message});

  factory _$_notification.fromJson(Map<String, dynamic> json) =>
      _$$_notificationFromJson(json);

  @override
  final String senderName;
  @override
  final String message;

  @override
  String toString() {
    return 'notification(senderName: $senderName, message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_notification &&
            (identical(other.senderName, senderName) ||
                other.senderName == senderName) &&
            (identical(other.message, message) || other.message == message));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, senderName, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_notificationCopyWith<_$_notification> get copyWith =>
      __$$_notificationCopyWithImpl<_$_notification>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_notificationToJson(
      this,
    );
  }
}

abstract class _notification implements notification {
  const factory _notification(
      {required final String senderName,
      required final String message}) = _$_notification;

  factory _notification.fromJson(Map<String, dynamic> json) =
      _$_notification.fromJson;

  @override
  String get senderName;
  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  _$$_notificationCopyWith<_$_notification> get copyWith =>
      throw _privateConstructorUsedError;
}
