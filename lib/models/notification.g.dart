// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_notification _$$_notificationFromJson(Map<String, dynamic> json) =>
    _$_notification(
      senderName: json['senderName'] as String,
      message: json['message'] as String,
    );

Map<String, dynamic> _$$_notificationToJson(_$_notification instance) =>
    <String, dynamic>{
      'senderName': instance.senderName,
      'message': instance.message,
    };
