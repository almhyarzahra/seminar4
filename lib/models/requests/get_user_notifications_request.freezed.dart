// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'get_user_notifications_request.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

GetUserNotificationRequest _$GetUserNotificationRequestFromJson(
    Map<String, dynamic> json) {
  return _GetUserNotificationRequest.fromJson(json);
}

/// @nodoc
mixin _$GetUserNotificationRequest {
  @JsonKey(name: 'user_id')
  int get user_id => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $GetUserNotificationRequestCopyWith<GetUserNotificationRequest>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GetUserNotificationRequestCopyWith<$Res> {
  factory $GetUserNotificationRequestCopyWith(GetUserNotificationRequest value,
          $Res Function(GetUserNotificationRequest) then) =
      _$GetUserNotificationRequestCopyWithImpl<$Res,
          GetUserNotificationRequest>;
  @useResult
  $Res call({@JsonKey(name: 'user_id') int user_id});
}

/// @nodoc
class _$GetUserNotificationRequestCopyWithImpl<$Res,
        $Val extends GetUserNotificationRequest>
    implements $GetUserNotificationRequestCopyWith<$Res> {
  _$GetUserNotificationRequestCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? user_id = null,
  }) {
    return _then(_value.copyWith(
      user_id: null == user_id
          ? _value.user_id
          : user_id // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_GetUserNotificationRequestCopyWith<$Res>
    implements $GetUserNotificationRequestCopyWith<$Res> {
  factory _$$_GetUserNotificationRequestCopyWith(
          _$_GetUserNotificationRequest value,
          $Res Function(_$_GetUserNotificationRequest) then) =
      __$$_GetUserNotificationRequestCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({@JsonKey(name: 'user_id') int user_id});
}

/// @nodoc
class __$$_GetUserNotificationRequestCopyWithImpl<$Res>
    extends _$GetUserNotificationRequestCopyWithImpl<$Res,
        _$_GetUserNotificationRequest>
    implements _$$_GetUserNotificationRequestCopyWith<$Res> {
  __$$_GetUserNotificationRequestCopyWithImpl(
      _$_GetUserNotificationRequest _value,
      $Res Function(_$_GetUserNotificationRequest) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? user_id = null,
  }) {
    return _then(_$_GetUserNotificationRequest(
      user_id: null == user_id
          ? _value.user_id
          : user_id // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_GetUserNotificationRequest implements _GetUserNotificationRequest {
  const _$_GetUserNotificationRequest(
      {@JsonKey(name: 'user_id') required this.user_id});

  factory _$_GetUserNotificationRequest.fromJson(Map<String, dynamic> json) =>
      _$$_GetUserNotificationRequestFromJson(json);

  @override
  @JsonKey(name: 'user_id')
  final int user_id;

  @override
  String toString() {
    return 'GetUserNotificationRequest(user_id: $user_id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetUserNotificationRequest &&
            (identical(other.user_id, user_id) || other.user_id == user_id));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, user_id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetUserNotificationRequestCopyWith<_$_GetUserNotificationRequest>
      get copyWith => __$$_GetUserNotificationRequestCopyWithImpl<
          _$_GetUserNotificationRequest>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_GetUserNotificationRequestToJson(
      this,
    );
  }
}

abstract class _GetUserNotificationRequest
    implements GetUserNotificationRequest {
  const factory _GetUserNotificationRequest(
          {@JsonKey(name: 'user_id') required final int user_id}) =
      _$_GetUserNotificationRequest;

  factory _GetUserNotificationRequest.fromJson(Map<String, dynamic> json) =
      _$_GetUserNotificationRequest.fromJson;

  @override
  @JsonKey(name: 'user_id')
  int get user_id;
  @override
  @JsonKey(ignore: true)
  _$$_GetUserNotificationRequestCopyWith<_$_GetUserNotificationRequest>
      get copyWith => throw _privateConstructorUsedError;
}
