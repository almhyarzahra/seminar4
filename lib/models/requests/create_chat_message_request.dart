import 'package:freezed_annotation/freezed_annotation.dart';

part 'create_chat_message_request.freezed.dart';
part 'create_chat_message_request.g.dart';

@freezed
class CreateChaMessageRequest with _$CreateChaMessageRequest {

  const factory CreateChaMessageRequest({
    @JsonKey(name:'chat_id') required int chatId,
    required String message

}) = _CreateChaMessageRequest;

  factory CreateChaMessageRequest.fromJson(Map<String, dynamic> json) =>
      _$CreateChaMessageRequestFromJson(json);
}
