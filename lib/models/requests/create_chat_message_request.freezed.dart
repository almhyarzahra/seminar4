// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'create_chat_message_request.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CreateChaMessageRequest _$CreateChaMessageRequestFromJson(
    Map<String, dynamic> json) {
  return _CreateChaMessageRequest.fromJson(json);
}

/// @nodoc
mixin _$CreateChaMessageRequest {
  @JsonKey(name: 'chat_id')
  int get chatId => throw _privateConstructorUsedError;
  String get message => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CreateChaMessageRequestCopyWith<CreateChaMessageRequest> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateChaMessageRequestCopyWith<$Res> {
  factory $CreateChaMessageRequestCopyWith(CreateChaMessageRequest value,
          $Res Function(CreateChaMessageRequest) then) =
      _$CreateChaMessageRequestCopyWithImpl<$Res, CreateChaMessageRequest>;
  @useResult
  $Res call({@JsonKey(name: 'chat_id') int chatId, String message});
}

/// @nodoc
class _$CreateChaMessageRequestCopyWithImpl<$Res,
        $Val extends CreateChaMessageRequest>
    implements $CreateChaMessageRequestCopyWith<$Res> {
  _$CreateChaMessageRequestCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chatId = null,
    Object? message = null,
  }) {
    return _then(_value.copyWith(
      chatId: null == chatId
          ? _value.chatId
          : chatId // ignore: cast_nullable_to_non_nullable
              as int,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CreateChaMessageRequestCopyWith<$Res>
    implements $CreateChaMessageRequestCopyWith<$Res> {
  factory _$$_CreateChaMessageRequestCopyWith(_$_CreateChaMessageRequest value,
          $Res Function(_$_CreateChaMessageRequest) then) =
      __$$_CreateChaMessageRequestCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({@JsonKey(name: 'chat_id') int chatId, String message});
}

/// @nodoc
class __$$_CreateChaMessageRequestCopyWithImpl<$Res>
    extends _$CreateChaMessageRequestCopyWithImpl<$Res,
        _$_CreateChaMessageRequest>
    implements _$$_CreateChaMessageRequestCopyWith<$Res> {
  __$$_CreateChaMessageRequestCopyWithImpl(_$_CreateChaMessageRequest _value,
      $Res Function(_$_CreateChaMessageRequest) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chatId = null,
    Object? message = null,
  }) {
    return _then(_$_CreateChaMessageRequest(
      chatId: null == chatId
          ? _value.chatId
          : chatId // ignore: cast_nullable_to_non_nullable
              as int,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CreateChaMessageRequest implements _CreateChaMessageRequest {
  const _$_CreateChaMessageRequest(
      {@JsonKey(name: 'chat_id') required this.chatId, required this.message});

  factory _$_CreateChaMessageRequest.fromJson(Map<String, dynamic> json) =>
      _$$_CreateChaMessageRequestFromJson(json);

  @override
  @JsonKey(name: 'chat_id')
  final int chatId;
  @override
  final String message;

  @override
  String toString() {
    return 'CreateChaMessageRequest(chatId: $chatId, message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CreateChaMessageRequest &&
            (identical(other.chatId, chatId) || other.chatId == chatId) &&
            (identical(other.message, message) || other.message == message));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, chatId, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CreateChaMessageRequestCopyWith<_$_CreateChaMessageRequest>
      get copyWith =>
          __$$_CreateChaMessageRequestCopyWithImpl<_$_CreateChaMessageRequest>(
              this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CreateChaMessageRequestToJson(
      this,
    );
  }
}

abstract class _CreateChaMessageRequest implements CreateChaMessageRequest {
  const factory _CreateChaMessageRequest(
      {@JsonKey(name: 'chat_id') required final int chatId,
      required final String message}) = _$_CreateChaMessageRequest;

  factory _CreateChaMessageRequest.fromJson(Map<String, dynamic> json) =
      _$_CreateChaMessageRequest.fromJson;

  @override
  @JsonKey(name: 'chat_id')
  int get chatId;
  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  _$$_CreateChaMessageRequestCopyWith<_$_CreateChaMessageRequest>
      get copyWith => throw _privateConstructorUsedError;
}
