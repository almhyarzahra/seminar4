// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_user_notifications_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_GetUserNotificationRequest _$$_GetUserNotificationRequestFromJson(
        Map<String, dynamic> json) =>
    _$_GetUserNotificationRequest(
      user_id: json['user_id'] as int,
    );

Map<String, dynamic> _$$_GetUserNotificationRequestToJson(
        _$_GetUserNotificationRequest instance) =>
    <String, dynamic>{
      'user_id': instance.user_id,
    };
