import 'package:freezed_annotation/freezed_annotation.dart';

part 'get_user_notifications_request.freezed.dart';
part 'get_user_notifications_request.g.dart';

@freezed
class GetUserNotificationRequest with _$GetUserNotificationRequest {
  const factory GetUserNotificationRequest({  @JsonKey(name:'user_id')  required int user_id}) = _GetUserNotificationRequest;

  factory GetUserNotificationRequest.fromJson(Map<String, dynamic> json) =>
      _$GetUserNotificationRequestFromJson(json);
}
