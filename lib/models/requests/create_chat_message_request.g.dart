// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_chat_message_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CreateChaMessageRequest _$$_CreateChaMessageRequestFromJson(
        Map<String, dynamic> json) =>
    _$_CreateChaMessageRequest(
      chatId: json['chat_id'] as int,
      message: json['message'] as String,
    );

Map<String, dynamic> _$$_CreateChaMessageRequestToJson(
        _$_CreateChaMessageRequest instance) =>
    <String, dynamic>{
      'chat_id': instance.chatId,
      'message': instance.message,
    };
