import 'package:equatable/equatable.dart';

// import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'app_response.g.dart';

@JsonSerializable(explicitToJson: true, genericArgumentFactories: true)
class AppResponse<T> extends Equatable {
  final bool success;

  final String message;

  final T? data;

  final int statusCode;

  final String statusMessage;

  const AppResponse._({
    required this.message,
    required this.data,
    required this.statusCode,
    required this.statusMessage,
    required this.success,
  });

  factory AppResponse({
    required String message,
    T? data,
    int? statusCode,
    String? statusMessage,
    required bool success,
  }) {
    return AppResponse._(
        message: message,
        data: data,
        statusCode: statusCode ?? 200,
        statusMessage: statusMessage ?? 'the request has success',
        success: success);
  }

  @override
  // TODO: implement props
  List<Object?> get props => [success, message, data ?? ""];

  factory AppResponse.fromJson(
      Map<String, dynamic> json, T Function(Object? json) fromJsonT) {
    return _$AppResponseFromJson(json, fromJsonT);
  }

  Map<String, dynamic> toJson(
    Object? Function(T? values) toJsonT,
  ) {
    return _$AppResponseToJson(this, toJsonT);
  }

  @override
  bool get stringify => true;
}
