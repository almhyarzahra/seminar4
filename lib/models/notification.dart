import 'package:freezed_annotation/freezed_annotation.dart';

part 'notification.freezed.dart';
part 'notification.g.dart';

@freezed
class notification with _$notification {
  const factory notification({

 required String senderName,required String message}) = _notification;

  factory notification.fromJson(Map<String, dynamic> json) =>
      _$notificationFromJson(json);
}
