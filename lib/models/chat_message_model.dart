import 'package:dash_chat_2/dash_chat_2.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:intl/intl.dart';
import 'package:school_erp/models/user_model.dart';
import 'package:school_erp/utils/formatting.dart';

part 'chat_message_model.freezed.dart';

part 'chat_message_model.g.dart';

@freezed
class ChatMessageEntity with _$ChatMessageEntity {
  ChatMessageEntity._();

   factory ChatMessageEntity({
    required int id,
    required String message,
    @JsonKey(name: "chat_id") required int chatId,
    @JsonKey(name: "user_id") required int userId,
    @JsonKey(name: "created_at") required String createdAt,
    @JsonKey(name: "updated_at") required String updatedAt,
    required UserModel user,
  }) = _ChatMessageEntity;

  factory ChatMessageEntity.fromJson(Map<String, dynamic> json) =>
      _$ChatMessageEntityFromJson(json);

  ChatMessage get getChatMessage {
    return ChatMessage(
        user: user.getChatUser,
        createdAt:
       parseDateTime(createdAt),
        text: message);
  }
}
