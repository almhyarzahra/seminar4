import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../models/user_model.dart';
import 'package:school_erp/repositories/repositories.dart';
part 'user_event.dart';
part 'user_state.dart';
part 'user_bloc.freezed.dart';

class UserBloc extends Bloc<UserEvent, UserState> {

  UserRepository _userRepository;

  UserBloc({required UserRepository userRepository})


      :_userRepository=userRepository, super(const UserState.initial()) {
    on<UserStarted>((event, emit)async {
      var users=await _userRepository.getUsers();
      emit(Loaded(users.data??[]));
    });
  }
}
