import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:school_erp/models/notification.dart';
import 'package:school_erp/models/requests/get_user_notifications_request.dart';
import 'package:school_erp/repositories/notification/notification_repository.dart';
import 'package:school_erp/utils/logger.dart';

import '../../models/models.dart';
import '../../repositories/repositories.dart';

part 'notification_event.dart';
part 'notification_state.dart';
part 'notification_bloc.freezed.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  NotificationBloc() : super(NotificationState.initial()) {
    on<AddNotification>((event, emit) {
      emit(state.copyWith(
          notifications: [...state.notifications, ...event.notifications],
          count: state.count + 1,
          notRead: state.notRead + 1));
    });
    on<ClearNotRead>((event, emit) {
      emit(state.copyWith(notRead: 0));
    });
    on<Started>((event, emit) async {
      var notificationRepository = NotificationRepository();
      var response = await notificationRepository.getNotification(
          request: GetUserNotificationRequest(user_id: event.id));
      wLog(response.data);
      emit(state.copyWith(notifications: response.data!));
    });
  }
}
