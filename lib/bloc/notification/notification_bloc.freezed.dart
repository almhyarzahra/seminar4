// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'notification_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$NotificationEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int id) started,
    required TResult Function(List<notification> notifications) addNotification,
    required TResult Function() clearNotRead,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int id)? started,
    TResult? Function(List<notification> notifications)? addNotification,
    TResult? Function()? clearNotRead,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int id)? started,
    TResult Function(List<notification> notifications)? addNotification,
    TResult Function()? clearNotRead,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Started value) started,
    required TResult Function(AddNotification value) addNotification,
    required TResult Function(ClearNotRead value) clearNotRead,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Started value)? started,
    TResult? Function(AddNotification value)? addNotification,
    TResult? Function(ClearNotRead value)? clearNotRead,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Started value)? started,
    TResult Function(AddNotification value)? addNotification,
    TResult Function(ClearNotRead value)? clearNotRead,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NotificationEventCopyWith<$Res> {
  factory $NotificationEventCopyWith(
          NotificationEvent value, $Res Function(NotificationEvent) then) =
      _$NotificationEventCopyWithImpl<$Res, NotificationEvent>;
}

/// @nodoc
class _$NotificationEventCopyWithImpl<$Res, $Val extends NotificationEvent>
    implements $NotificationEventCopyWith<$Res> {
  _$NotificationEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$StartedCopyWith<$Res> {
  factory _$$StartedCopyWith(_$Started value, $Res Function(_$Started) then) =
      __$$StartedCopyWithImpl<$Res>;
  @useResult
  $Res call({int id});
}

/// @nodoc
class __$$StartedCopyWithImpl<$Res>
    extends _$NotificationEventCopyWithImpl<$Res, _$Started>
    implements _$$StartedCopyWith<$Res> {
  __$$StartedCopyWithImpl(_$Started _value, $Res Function(_$Started) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_$Started(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$Started implements Started {
  const _$Started({required this.id});

  @override
  final int id;

  @override
  String toString() {
    return 'NotificationEvent.started(id: $id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Started &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$StartedCopyWith<_$Started> get copyWith =>
      __$$StartedCopyWithImpl<_$Started>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int id) started,
    required TResult Function(List<notification> notifications) addNotification,
    required TResult Function() clearNotRead,
  }) {
    return started(id);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int id)? started,
    TResult? Function(List<notification> notifications)? addNotification,
    TResult? Function()? clearNotRead,
  }) {
    return started?.call(id);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int id)? started,
    TResult Function(List<notification> notifications)? addNotification,
    TResult Function()? clearNotRead,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(id);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Started value) started,
    required TResult Function(AddNotification value) addNotification,
    required TResult Function(ClearNotRead value) clearNotRead,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Started value)? started,
    TResult? Function(AddNotification value)? addNotification,
    TResult? Function(ClearNotRead value)? clearNotRead,
  }) {
    return started?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Started value)? started,
    TResult Function(AddNotification value)? addNotification,
    TResult Function(ClearNotRead value)? clearNotRead,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class Started implements NotificationEvent {
  const factory Started({required final int id}) = _$Started;

  int get id;
  @JsonKey(ignore: true)
  _$$StartedCopyWith<_$Started> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$AddNotificationCopyWith<$Res> {
  factory _$$AddNotificationCopyWith(
          _$AddNotification value, $Res Function(_$AddNotification) then) =
      __$$AddNotificationCopyWithImpl<$Res>;
  @useResult
  $Res call({List<notification> notifications});
}

/// @nodoc
class __$$AddNotificationCopyWithImpl<$Res>
    extends _$NotificationEventCopyWithImpl<$Res, _$AddNotification>
    implements _$$AddNotificationCopyWith<$Res> {
  __$$AddNotificationCopyWithImpl(
      _$AddNotification _value, $Res Function(_$AddNotification) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? notifications = null,
  }) {
    return _then(_$AddNotification(
      notifications: null == notifications
          ? _value._notifications
          : notifications // ignore: cast_nullable_to_non_nullable
              as List<notification>,
    ));
  }
}

/// @nodoc

class _$AddNotification implements AddNotification {
  const _$AddNotification({required final List<notification> notifications})
      : _notifications = notifications;

  final List<notification> _notifications;
  @override
  List<notification> get notifications {
    if (_notifications is EqualUnmodifiableListView) return _notifications;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_notifications);
  }

  @override
  String toString() {
    return 'NotificationEvent.addNotification(notifications: $notifications)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddNotification &&
            const DeepCollectionEquality()
                .equals(other._notifications, _notifications));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_notifications));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AddNotificationCopyWith<_$AddNotification> get copyWith =>
      __$$AddNotificationCopyWithImpl<_$AddNotification>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int id) started,
    required TResult Function(List<notification> notifications) addNotification,
    required TResult Function() clearNotRead,
  }) {
    return addNotification(notifications);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int id)? started,
    TResult? Function(List<notification> notifications)? addNotification,
    TResult? Function()? clearNotRead,
  }) {
    return addNotification?.call(notifications);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int id)? started,
    TResult Function(List<notification> notifications)? addNotification,
    TResult Function()? clearNotRead,
    required TResult orElse(),
  }) {
    if (addNotification != null) {
      return addNotification(notifications);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Started value) started,
    required TResult Function(AddNotification value) addNotification,
    required TResult Function(ClearNotRead value) clearNotRead,
  }) {
    return addNotification(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Started value)? started,
    TResult? Function(AddNotification value)? addNotification,
    TResult? Function(ClearNotRead value)? clearNotRead,
  }) {
    return addNotification?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Started value)? started,
    TResult Function(AddNotification value)? addNotification,
    TResult Function(ClearNotRead value)? clearNotRead,
    required TResult orElse(),
  }) {
    if (addNotification != null) {
      return addNotification(this);
    }
    return orElse();
  }
}

abstract class AddNotification implements NotificationEvent {
  const factory AddNotification(
      {required final List<notification> notifications}) = _$AddNotification;

  List<notification> get notifications;
  @JsonKey(ignore: true)
  _$$AddNotificationCopyWith<_$AddNotification> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ClearNotReadCopyWith<$Res> {
  factory _$$ClearNotReadCopyWith(
          _$ClearNotRead value, $Res Function(_$ClearNotRead) then) =
      __$$ClearNotReadCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ClearNotReadCopyWithImpl<$Res>
    extends _$NotificationEventCopyWithImpl<$Res, _$ClearNotRead>
    implements _$$ClearNotReadCopyWith<$Res> {
  __$$ClearNotReadCopyWithImpl(
      _$ClearNotRead _value, $Res Function(_$ClearNotRead) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ClearNotRead implements ClearNotRead {
  const _$ClearNotRead();

  @override
  String toString() {
    return 'NotificationEvent.clearNotRead()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ClearNotRead);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int id) started,
    required TResult Function(List<notification> notifications) addNotification,
    required TResult Function() clearNotRead,
  }) {
    return clearNotRead();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int id)? started,
    TResult? Function(List<notification> notifications)? addNotification,
    TResult? Function()? clearNotRead,
  }) {
    return clearNotRead?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int id)? started,
    TResult Function(List<notification> notifications)? addNotification,
    TResult Function()? clearNotRead,
    required TResult orElse(),
  }) {
    if (clearNotRead != null) {
      return clearNotRead();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Started value) started,
    required TResult Function(AddNotification value) addNotification,
    required TResult Function(ClearNotRead value) clearNotRead,
  }) {
    return clearNotRead(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Started value)? started,
    TResult? Function(AddNotification value)? addNotification,
    TResult? Function(ClearNotRead value)? clearNotRead,
  }) {
    return clearNotRead?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Started value)? started,
    TResult Function(AddNotification value)? addNotification,
    TResult Function(ClearNotRead value)? clearNotRead,
    required TResult orElse(),
  }) {
    if (clearNotRead != null) {
      return clearNotRead(this);
    }
    return orElse();
  }
}

abstract class ClearNotRead implements NotificationEvent {
  const factory ClearNotRead() = _$ClearNotRead;
}

/// @nodoc
mixin _$NotificationState {
  int get notRead => throw _privateConstructorUsedError;
  List<notification> get notifications => throw _privateConstructorUsedError;
  int get count => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $NotificationStateCopyWith<NotificationState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NotificationStateCopyWith<$Res> {
  factory $NotificationStateCopyWith(
          NotificationState value, $Res Function(NotificationState) then) =
      _$NotificationStateCopyWithImpl<$Res, NotificationState>;
  @useResult
  $Res call({int notRead, List<notification> notifications, int count});
}

/// @nodoc
class _$NotificationStateCopyWithImpl<$Res, $Val extends NotificationState>
    implements $NotificationStateCopyWith<$Res> {
  _$NotificationStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? notRead = null,
    Object? notifications = null,
    Object? count = null,
  }) {
    return _then(_value.copyWith(
      notRead: null == notRead
          ? _value.notRead
          : notRead // ignore: cast_nullable_to_non_nullable
              as int,
      notifications: null == notifications
          ? _value.notifications
          : notifications // ignore: cast_nullable_to_non_nullable
              as List<notification>,
      count: null == count
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_NotificationStateCopyWith<$Res>
    implements $NotificationStateCopyWith<$Res> {
  factory _$$_NotificationStateCopyWith(_$_NotificationState value,
          $Res Function(_$_NotificationState) then) =
      __$$_NotificationStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int notRead, List<notification> notifications, int count});
}

/// @nodoc
class __$$_NotificationStateCopyWithImpl<$Res>
    extends _$NotificationStateCopyWithImpl<$Res, _$_NotificationState>
    implements _$$_NotificationStateCopyWith<$Res> {
  __$$_NotificationStateCopyWithImpl(
      _$_NotificationState _value, $Res Function(_$_NotificationState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? notRead = null,
    Object? notifications = null,
    Object? count = null,
  }) {
    return _then(_$_NotificationState(
      notRead: null == notRead
          ? _value.notRead
          : notRead // ignore: cast_nullable_to_non_nullable
              as int,
      notifications: null == notifications
          ? _value._notifications
          : notifications // ignore: cast_nullable_to_non_nullable
              as List<notification>,
      count: null == count
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_NotificationState extends _NotificationState {
  const _$_NotificationState(
      {required this.notRead,
      required final List<notification> notifications,
      required this.count})
      : _notifications = notifications,
        super._();

  @override
  final int notRead;
  final List<notification> _notifications;
  @override
  List<notification> get notifications {
    if (_notifications is EqualUnmodifiableListView) return _notifications;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_notifications);
  }

  @override
  final int count;

  @override
  String toString() {
    return 'NotificationState(notRead: $notRead, notifications: $notifications, count: $count)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_NotificationState &&
            (identical(other.notRead, notRead) || other.notRead == notRead) &&
            const DeepCollectionEquality()
                .equals(other._notifications, _notifications) &&
            (identical(other.count, count) || other.count == count));
  }

  @override
  int get hashCode => Object.hash(runtimeType, notRead,
      const DeepCollectionEquality().hash(_notifications), count);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_NotificationStateCopyWith<_$_NotificationState> get copyWith =>
      __$$_NotificationStateCopyWithImpl<_$_NotificationState>(
          this, _$identity);
}

abstract class _NotificationState extends NotificationState {
  const factory _NotificationState(
      {required final int notRead,
      required final List<notification> notifications,
      required final int count}) = _$_NotificationState;
  const _NotificationState._() : super._();

  @override
  int get notRead;
  @override
  List<notification> get notifications;
  @override
  int get count;
  @override
  @JsonKey(ignore: true)
  _$$_NotificationStateCopyWith<_$_NotificationState> get copyWith =>
      throw _privateConstructorUsedError;
}
