part of 'notification_bloc.dart';

@freezed
class NotificationState with _$NotificationState {

   const NotificationState._();
  factory NotificationState.initial(){

     return NotificationState(notifications:[],count:0,notRead:0);
   }
const   factory NotificationState({
  required int notRead,
  required List<notification>
notifications,required int count })=_NotificationState;
   }
