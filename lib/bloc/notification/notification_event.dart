part of 'notification_bloc.dart';

@freezed
class NotificationEvent with _$NotificationEvent {
  const factory NotificationEvent.started({required int id}) = Started;
  const factory NotificationEvent.addNotification({required List<notification> notifications}) = AddNotification;
  const factory NotificationEvent.clearNotRead() = ClearNotRead;
}
