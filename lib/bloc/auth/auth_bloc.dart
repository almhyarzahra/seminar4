import 'package:bloc/bloc.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:school_erp/models/models.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

import '../../utils/logger.dart';

part 'auth_event.dart';

part 'auth_state.dart';

part 'auth_bloc.freezed.dart';

class AuthBloc extends HydratedBloc<AuthEvent, AuthState> {
  AuthBloc() : super(AuthState.initial()) {
    on<Authenticated>((event, emit) {
      emit(state.copyWith(
          isAuthenticated: event.isAuthenticated,
          token: event.token,
          user: event.user));
    });
  }

  @override
  AuthState? fromJson(Map<String, dynamic> json) {
    return AuthState(
        isAuthenticated: json['isAuthenticated'],
        user: json['user'] != null ? UserModel.fromJson(json['user']) : null,
        token: json['token']);
  }

  @override
  Map<String, dynamic>? toJson(AuthState state) {
    return {
      'isAuthenticated': state.isAuthenticated,
      'token': state.token,
      'user': state.user != null ? state.user!.toJson() : null
    };
  }
}
