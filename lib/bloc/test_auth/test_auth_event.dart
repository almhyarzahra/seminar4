part of 'test_auth_bloc.dart';

@immutable
abstract class TestAuthEvent {}
class Auth extends TestAuthEvent
{
  bool isAuth;
  String token;
  UserModel user;

  Auth(this.isAuth, this.token, this.user);
}
class NotAuth extends TestAuthEvent
{
  bool isAuth;
  String? token;
  UserModel? user;

  NotAuth(this.isAuth, this.token, this.user);
}