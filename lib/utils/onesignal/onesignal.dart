import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:school_erp/utils/logger.dart';

const oneSignalAppId = "69a10cfe-3f5f-4196-a590-aceeb6101ba8";
Future<void> initOneSignal() async {
  final oneSignalShared = OneSignal.shared;
  oneSignalShared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);
  oneSignalShared.setRequiresUserPrivacyConsent(true);
  await oneSignalShared.setAppId(oneSignalAppId);
}

registerOneSignalEventListener(
    {required Function(OSNotificationOpenedResult) onOpened,
    required Function(OSNotificationReceivedEvent) onReceivedInForeground}) {
  final oneSignalShared = OneSignal.shared;
  oneSignalShared.setNotificationOpenedHandler(onOpened);
  oneSignalShared
      .setNotificationWillShowInForegroundHandler(onReceivedInForeground);
}

// const tagName="userId";
sendUserTag(int userId, String tagName) {
  // OneSignal.shared.
  OneSignal.shared.sendTag(tagName, userId.toString()).then((value) {
    vLog(value);
  }).catchError((error) {
    vLog(error);
  });

  OneSignal.shared.sendTag("broadcast", '1').then((value) {
    vLog(value);
  }).catchError((error) {
    vLog(error);
  });

  OneSignal.shared.sendTag("notification", userId.toString()).then((value) {
    vLog(value);
  }).catchError((error) {
    vLog(error);
  });
}

deleteUserTag(String tagName) {
  OneSignal.shared.deleteTag(tagName).then((value) {
    vLog(value);
  }).catchError((error) {
    vLog(error);
  });

  OneSignal.shared.deleteTag("notification").then((value) {
    vLog(value);
  }).catchError((error) {
    vLog(error);
  });

  OneSignal.shared.deleteTag("broadcast").then((value) {
    vLog(value);
  }).catchError((error) {
    vLog(error);
  });
}
