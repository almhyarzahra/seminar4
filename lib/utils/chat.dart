import 'package:dash_chat_2/dash_chat_2.dart';
import 'package:flutter/cupertino.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/utils/logger.dart';

import '../models/models.dart';

String getChatName(
    List<ChatParticipantEntity> participants, UserModel currentUser) {
  final otherParticipant = participants
      .where((element) => element.userId != currentUser.id)
      .toList();
  if (otherParticipant.isNotEmpty) return otherParticipant[0].user.name.toCapitalized();
  return "None";
}



String getOtherUserPhoto(
    List<ChatParticipantEntity> participants, UserModel currentUser) {
  final otherParticipant = participants
      .where((element) => element.userId != currentUser.id)
      .toList();
  if (otherParticipant.isNotEmpty) {
    // iLog(otherParticipant[0].user.photo_path);
    // +getOtherUserPhoto(chat_item.participants, user)
    return otherParticipant[0].user.photo_path;
  }return "None";
}
int getOtherUserId(
    List<ChatParticipantEntity> participants, UserModel currentUser) {
  final otherParticipant = participants
      .where((element) => element.userId != currentUser.id).toList();
return otherParticipant[0].user.id!;

}
//
//  final Widget Function(
//     ChatUser, Function? onPressAvatar, Function? onLongPressAvatar)?
// avatarBuilder= Widget Function(
//     ChatUser(),Chat(id) onPressAvatar, (){} onLongPressAvatar)?{};