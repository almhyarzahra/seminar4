import 'dart:io';

import 'package:dio/dio.dart';
import 'package:school_erp/bloc/auth/auth_bloc.dart';
import 'package:school_erp/models/app_response.dart';

class AppInterceptors extends Interceptor {
  static AppInterceptors? _singleton;
AppInterceptors._internal();
  factory AppInterceptors() {
    return _singleton ?? AppInterceptors._internal();
  }

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    // TODO: implement onRequest
    if (!options.headers.containsKey(HttpHeaders.authorizationHeader)) {
     final state=AuthBloc().state;
     if(state.token!=null)
    options.headers[HttpHeaders.authorizationHeader] = 'Bearer ${state.token}';
    }
    return handler.next(options);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    final responseData = mapResponseData(
        requestOptions: response.requestOptions,
        response: response

    );

    return handler.resolve(responseData);
  }

  // @override
  // void onError(DioError err, ErrorInterceptorHandler handler) {
  //
  //
  //   final errorMessage=getErrorMessage(err.type,err.response?.statusCode);
  //   final responseData=mapResponseData(requestOptions: err.requestOptions,
  //   response: err.response,
  //     customMessage: errorMessage,
  //     isErrorResponse: true
  //   );
  //
  //
  //
  // }
// String getErrorMessage(DioErrorType errorType,int? statusCode)
// {
//   String errorMessage="";
//   switch (errorType)
//       {
//     case DioErrorType.connectTimeout:
//     case DioErrorType.sendTimeout:
//     case DioErrorType.receiveTimeout:
//       errorMessage=DioError
//       breake;
//     case DioErrorType.response:
//       switch(statusCode)
//           {
//             case 400:
//             breake;
//             case 401:
//             breake;
//             case 404:
//             breake;
//             case 409:
//             breake;
//             case 500:
//             breake;
//
//
//
//       }
//
//
//
//
//   }
//
//
//
// }
  Response<dynamic> mapResponseData
      ({
    Response<dynamic>? response,
    required RequestOptions requestOptions,
    String customMessage = "",
    bool isErrorResponse = false,


  }) {
    final bool hasResponseData = response?.data != null;
    Map<String, dynamic>? responseData = response?.data;

    if (hasResponseData) {
      responseData!.addAll({
        "statusCode": response?.statusCode,
        "statusMessage": response?.statusMessage
      });
    }
    return Response(

        requestOptions: requestOptions, data: hasResponseData ? responseData :
AppResponse(message: customMessage, success: isErrorResponse?false:true,
statusCode: response?.statusCode,
statusMessage: response?.statusMessage,).toJson((values) => null)

    );
  }


}
