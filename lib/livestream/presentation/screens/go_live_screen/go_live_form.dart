import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:school_erp/ui/shared/colors.dart';

import '../../../logic/cubits/create_livestream_cubit.dart';
import 'custom_form_field.dart';
import '../../global_widgets/spinner_indicator.dart';
import 'focus_nodes_provider.dart';
import 'go_live_button.dart';
import 'subjects_form_field.dart';

class GOLiveForm extends StatefulWidget {
  final GlobalKey<FormState> formKey;

  const GOLiveForm({super.key, required this.formKey});
  @override
  _GOLiveFormState createState() => _GOLiveFormState();
}

class _GOLiveFormState extends State<GOLiveForm> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _subjectController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _titleController.dispose();
    _descriptionController.dispose();
    _subjectController.dispose();

    super.dispose();
  }

  String? _validateTitleField(String? value) {
    if (value != null && value.isNotEmpty) {
      return null;
    }
    return "Please Enter Title";
  }

  String? _validateDescriptionField(String? value) {
    if (value != null && value.length > 5) {
      return null;
    }
    return "Description must be +5 chars";
  }

  @override
  Widget build(BuildContext context) {
    final focusNodes = FocusNodesProvider.of(context);
    return Form(
      key: widget.formKey,
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CustomFormField(
                color: AppColors.mainBlue10Color,
                controller: _titleController,
                focusNode: focusNodes.titleFocusNode,
                hintText: "Title",
                inputType: TextInputType.name,
                obscureText: false,
                validator: _validateTitleField,
              ),
              SizedBox(
                height: 30,
              ),
              CustomFormField(
                color: AppColors.mainBlue250Color,
                controller: _descriptionController,
                hintText: 'Descritpion',
                focusNode: focusNodes.descriptionNode,
                validator: _validateDescriptionField,
                inputType: TextInputType.text,
                allowMultiLine: true,
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                  width: 150,
                  child: SubjectFormFIeld(
                    color: AppColors.mainBlue10Color,
                    controller: _subjectController,
                  )),
              BlocBuilder<CreateLivestreamCubit, CreateLivestreamState>(
                builder: (context, state) {
                  if (state is CreateLivestreamInProgress) {
                    return SizedBox(
                        width: MediaQuery.of(context).size.width * 0.5,
                        child: Center(child: SpinnerIndicator()));
                    ;
                  }
                  return GoLiveButton(
                    color: AppColors.mainBlue10Color,
                    widget: widget,
                    titleController: _titleController,
                    descriptionController: _descriptionController,
                    subjectController: _subjectController,
                    focusNodes: focusNodes,
                  );
                },
              ),
            ],
          ),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
