import 'package:flutter/material.dart';

class FocusNodesProvider extends InheritedWidget {
  final FocusNode titleFocusNode;
  final FocusNode descriptionNode;

  const FocusNodesProvider({
    super.key,
    required this.titleFocusNode,
    required this.descriptionNode,
    required Widget child,
  }) : super(child: child);

  static FocusNodesProvider of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<FocusNodesProvider>()!;
  }

  @override
  bool updateShouldNotify(FocusNodesProvider oldWidget) {
    return titleFocusNode != oldWidget.titleFocusNode ||
        descriptionNode != oldWidget.descriptionNode;
  }
}
