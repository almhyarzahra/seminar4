import 'package:animated_button/animated_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../../../bloc/auth/auth_bloc.dart';
import '../../../../ui/shared/colors.dart';
import '../../../logic/cubits/create_livestream_cubit.dart';
import '../../../logic/cubits/sync_livestreams_cubit.dart';
import '../../global_widgets/utils/user_subjects_fetcher.dart';
import 'focus_nodes_provider.dart';
import 'go_live_form.dart';

class GoLiveButton extends StatelessWidget {
  const GoLiveButton({
    super.key,
    required this.widget,
    required TextEditingController titleController,
    required TextEditingController descriptionController,
    required TextEditingController subjectController,
    required this.focusNodes,
    required this.color,
  })  : _titleController = titleController,
        _subjectController = subjectController,
        _descriptionController = descriptionController;

  final GOLiveForm widget;
  final TextEditingController _titleController;
  final TextEditingController _descriptionController;
  final TextEditingController _subjectController;
  final Color color;

  final FocusNodesProvider focusNodes;

  @override
  Widget build(BuildContext context) {
    return AnimatedButton(
      color: color,
      width: 150,
      height: 60,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            'GO LIVE!',
            style: TextStyle(
                color: AppColors.mainBlackColor,
                fontSize: 20,
                fontWeight: FontWeight.bold),
          ),
          Icon(
            Icons.video_camera_front_outlined,
            color: AppColors.mainBlackColor,
            size: 20,
          ),
        ],
      ),
      onPressed: () {
        if (widget.formKey.currentState!.validate()) {
          focusNodes.titleFocusNode.unfocus();
          focusNodes.descriptionNode.unfocus();
          if (BlocProvider.of<SyncLivestreamsCubit>(context).state
              is LsDisconnected) {
            Fluttertoast.showToast(
                fontSize: 15, msg: "Failed check you internet connection !");
            return;
          }
          BlocProvider.of<CreateLivestreamCubit>(context).createLivestream(
              title: _titleController.text,
              description: _descriptionController.text,
              uuid: context.read<AuthBloc>().state.user!.id!,
              subjectID: UserSubjectsFetcher.subjects
                  .firstWhere(
                      (element) => element.Name == _subjectController.text)
                  .Id);
        }
      },
    );
  }
}
