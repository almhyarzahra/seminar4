import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../bloc/auth/auth_bloc.dart';
import '../../../../ui/shared/colors.dart';
import '../../global_widgets/utils/user_subjects_fetcher.dart';

class SubjectsButtomSheet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      padding: EdgeInsets.all(16.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onVerticalDragDown: (_) {},
            onTap: () {},
            child: Container(
              width: 40.0,
              height: 4.0,
              decoration: BoxDecoration(
                color: Colors.grey,
                borderRadius: BorderRadius.circular(2.0),
              ),
            ),
          ),
          SizedBox(height: 16.0),
          FutureBuilder<int>(
            future: UserSubjectsFetcher.fetchSubjects(
                context.read<AuthBloc>().state.user!.id!),
            builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return SizedBox(
                  height: 125,
                  child: Center(child: CircularProgressIndicator()),
                ); // Display a progress indicator while loading
              } else {
                if (snapshot.data == 0) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.wifi_off,
                        size: 80,
                        color: AppColors.mainBlue250Color,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Please check your internet connection!",
                        style: TextStyle(
                          color: AppColors.mainBlackColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  );
                }
                return Expanded(
                  child: ListView.builder(
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Row(
                          children: [
                            Icon(
                              Icons.school,
                              color: AppColors.mainBlue250Color,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(
                              UserSubjectsFetcher.subjects[index].Name,
                              style: TextStyle(
                                  color: AppColors.mainBlackColor,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        onTap: () => Navigator.of(context)
                            .pop(UserSubjectsFetcher.subjects[index]),
                      );
                    },
                    itemCount: UserSubjectsFetcher.subjects.length,
                  ),
                );
              }
            },
          )
        ],
      ),
    );
  }
}
