import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:school_erp/bloc/auth/auth_bloc.dart';

import '../../../../ui/shared/colors.dart';
import '../../../../ui/shared/custom_widget/custom_text.dart';
import '../../../data/network_services/livestream_ns.dart';
import '../../../data/repositories/livestream_chat_repository.dart';
import '../../../data/repositories/streaning_repository.dart';
import '../../../../locator.dart';
import '../../../logic/cubits/create_livestream_cubit.dart';
import '../../../logic/cubits/streaming_chat_cubit.dart';
import '../../../logic/cubits/streaming_cubit.dart';
import '../../../logic/cubits/sync_livestreams_cubit.dart';
import '../streaming_screen/streaming_screen_wrapper.dart';
import 'focus_nodes_provider.dart';
import 'go_live_form.dart';

class GoLiveScreen extends StatefulWidget {
  const GoLiveScreen({super.key});

  @override
  GoLiveScreenState createState() => GoLiveScreenState();
}

class GoLiveScreenState extends State<GoLiveScreen> {
  late FocusNode _f1, _f2;
  late GlobalKey<FormState> _formKey;
  @override
  void initState() {
    int id = context.read<AuthBloc>().state.user!.id!;
    BlocProvider.of<SyncLivestreamsCubit>(context).connect(id: id.toString());
    _f1 = FocusNode();
    _f2 = FocusNode();
    _formKey = GlobalKey<FormState>();

    super.initState();
  }

  @override
  void dispose() {
    _f1.dispose();
    _f2.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return BlocListener<CreateLivestreamCubit, CreateLivestreamState>(
        listener: (context, state) async {
          if (state is CreateLiveStreamSuccess) {
            goToStreamingScreen(state);
          } else if (state is CreateLiveStreamFailed) {
            Fluttertoast.showToast(
                fontSize: 15, msg: "Failed check you internet connection !");
          }
        },
        child: GestureDetector(
          onTap: () {
            _f1.unfocus();
            _f2.unfocus();
          },
          child: Container(
              color: AppColors.mainBlue250Color,
              child: ListView(
                children: [
                  SizedBox(
                    height: 300,
                    width: 300,
                    child: Image.asset('images/go_live_logo.png'),
                  ),
                  Padding(
                    padding:
                        EdgeInsetsDirectional.only(start: 20, end: 30, top: 10),
                    child: CustomText(
                      content: "Online lecture",
                      colorText: AppColors.mainBlackColor,
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsetsDirectional.only(
                        start: (20), top: (10), bottom: 10),
                    child: CustomText(
                      content: "Start an online lecture now!",
                      colorText: AppColors.mainBlackColor,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        top: 20,
                        right: size.width * 0.05,
                        left: size.width * 0.05),
                    decoration: BoxDecoration(
                        color: AppColors.mainWhiteColor,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(45),
                          topRight: Radius.circular(45),
                        )),
                    child: FocusNodesProvider(
                      titleFocusNode: _f1,
                      descriptionNode: _f2,
                      child: GOLiveForm(formKey: _formKey),
                    ),
                  ),
                ],
              )),
        ));
  }

  void goToStreamingScreen(CreateLiveStreamSuccess state) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) {
        StreamingCubitArgumnets args = StreamingCubitArgumnets(
            isHost: true,
            livestream: state.createdLs,
            repo: StreamingRepository(
                livestream: state.createdLs, ns: locator<LivestreamNS>()),
            userUUID: context.read<AuthBloc>().state.user!.id!);
        return MultiBlocProvider(providers: [
          BlocProvider<StreamingCubit>(
            create: (context) => StreamingCubit(args: args),
          ),
          BlocProvider<StreamingChatCubit>(
            create: (context) => StreamingChatCubit(
                livestreamId: args.livestream.id,
                repo: LivestreamChatRepository(ns: locator<LivestreamNS>())),
          )
        ], child: StreamingScreenWrapper());
      },
    ));
  }
}
