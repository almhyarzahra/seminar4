import 'dart:math';

import 'package:flutter/material.dart';

class CustomFormField extends StatefulWidget {
  final TextEditingController controller;
  final String hintText;
  final TextInputType inputType;
  final bool obscureText;
  final FocusNode focusNode;
  final FormFieldValidator<String>? validator;
  final bool? allowMultiLine;
  final Color color; // New color property

  const CustomFormField({
    Key? key,
    required this.controller,
    required this.hintText,
    this.inputType = TextInputType.text,
    this.obscureText = false,
    this.validator,
    required this.focusNode,
    this.allowMultiLine,
    required this.color, // Pass the color property in the constructor
  }) : super(key: key);

  @override
  _CustomFormFieldState createState() => _CustomFormFieldState();
}

class _CustomFormFieldState extends State<CustomFormField>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  void _handleErrorAnimation() {
    _animationController.reset();
    _animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    final Color primaryColor = widget.color;
    final Color borderColor = Color.alphaBlend(
      primaryColor.withOpacity(0.7),
      Colors.grey,
    );

    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        final animationValue = _animationController.value;
        final shakeTransform = Matrix4.translationValues(
          sin(animationValue * pi * 10) * 10,
          0,
          0,
        );
        return Transform(
          transform: shakeTransform,
          child: child,
        );
      },
      child: TextFormField(
        style: TextStyle(color: Colors.black),
        maxLines: widget.allowMultiLine != null ? (3) : 1,
        controller: widget.controller,
        keyboardType: widget.inputType,
        obscureText: widget.obscureText,
        focusNode: widget.focusNode,
        validator: (value) {
          if (widget.validator != null) {
            final result = widget.validator!(value);
            if (result != null) {
              _handleErrorAnimation();
            }
            return result;
          }
          return null;
        },
        decoration: InputDecoration(
          hintText: widget.hintText,
          filled: true,
          hintStyle: TextStyle(color: Colors.black),
          fillColor: primaryColor, // Use primaryColor as the background color
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: BorderSide(
                color: borderColor), // Use borderColor for the border
          ),
        ),
        onEditingComplete: () {
          widget.focusNode.nextFocus();
        },
      ),
    );
  }
}
