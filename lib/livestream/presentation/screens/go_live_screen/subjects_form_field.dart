import 'dart:math';

import 'package:flutter/material.dart';
import 'package:school_erp/livestream/presentation/screens/go_live_screen/subjects_buttom_sheet.dart';

import '../../../../ui/shared/colors.dart';
import '../../global_widgets/utils/user_subjects_fetcher.dart';

class SubjectFormFIeld extends StatefulWidget {
  final TextEditingController controller;
  final Color color;

  const SubjectFormFIeld({
    super.key,
    required this.controller,
    required this.color,
  });
  @override
  SubjectFormFIeldState createState() => SubjectFormFIeldState();
}

class SubjectFormFIeldState extends State<SubjectFormFIeld>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;

  FocusNode f = FocusNode();
  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );

    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    f.dispose();
    super.dispose();
  }

  void _handleErrorAnimation() {
    _animationController.reset();
    _animationController.forward();
  }

  void _showSubjectBottomSheet(BuildContext context) async {
    final selectedSubject = await showModalBottomSheet<UserSubject>(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(16.0)),
      ),
      builder: (BuildContext context) {
        return SubjectsButtomSheet();
      },
    );

    if (selectedSubject != null) {
      setState(() {
        widget.controller.text = selectedSubject.Name;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        final animationValue = _animationController.value;
        final shakeTransform = Matrix4.translationValues(
          sin(animationValue * pi * 10) * 10,
          0,
          0,
        );
        return Transform(
          transform: shakeTransform,
          child: child,
        );
      },
      child: TextFormField(
        focusNode: f,
        validator: (value) {
          if (value == null || value.isEmpty) {
            _handleErrorAnimation();
            return "Please select a subject";
          }
        },
        controller: widget.controller,
        readOnly: true,
        decoration: InputDecoration(
          hintText: 'Select subject',
          filled: true,
          hintStyle: TextStyle(color: AppColors.mainBlackColor),
          fillColor: AppColors
              .mainBlue250Color, // Use primaryColor as the background color
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: BorderSide(
                color: widget.color), // Use borderColor for the border
          ),
        ),
        onTap: () {
          _showSubjectBottomSheet(context);
          f.unfocus();
        },
      ),
    );
  }
}
