import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../../logic/cubits/streaming_cubit.dart';
import 'streaming_screen_landscape.dart';
import 'streaming_screen_portrait.dart';

class StreamingScreenWrapper extends StatefulWidget {
  const StreamingScreenWrapper({super.key});

  @override
  State<StreamingScreenWrapper> createState() => _StreamingScreenWrapperState();
}

class _StreamingScreenWrapperState extends State<StreamingScreenWrapper> {
  bool closed = false;
  @override
  Widget build(BuildContext context) {
    print("sliman buit streaming_screen_wrapper");

    return BlocListener<StreamingCubit, StreamingState>(
      listenWhen: (previous, current) =>
          current is StreamingEnded || current is StreamingStarted,
      listener: (context, state) {
        if (state is StreamingEnded) {
          if (state.endedByBroadcaster) {
            Fluttertoast.showToast(msg: "Broadcaster ended the Broadcast");
          }
          if (!closed) {
            closed = true;
            Navigator.of(context).pop();
          }
        }
      },
      child: SafeArea(
        child: OrientationBuilder(
          builder: (context, orientation) {
            if (orientation == Orientation.portrait) {
              return const StreamingScreenPortrait();
            }
            return const StreamingScreenLandscape();
          },
        ),
      ),
    );
  }
}
