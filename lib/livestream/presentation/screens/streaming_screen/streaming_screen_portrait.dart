import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_expandable_fab/flutter_expandable_fab.dart';
import 'package:school_erp/livestream/presentation/screens/streaming_screen/widgets/video_view.dart';

import '../../../../ui/shared/colors.dart';
import '../../../logic/cubits/streaming_cubit.dart';
import 'widgets/chat_view.dart';
import 'widgets/controls_fab.dart';
import 'widgets/description_view.dart';

class StreamingScreenPortrait extends StatefulWidget {
  const StreamingScreenPortrait({
    super.key,
  });
  @override
  StreamingScreenPortraitState createState() => StreamingScreenPortraitState();
}

class StreamingScreenPortraitState extends State<StreamingScreenPortrait> {
  final FocusNode _focusNode = FocusNode();
  bool _showFab = true;
  double _commentSectionOffset = -400;
  Duration _chatSectionAnimateDuration = const Duration(milliseconds: 300);

  @override
  void initState() {
    super.initState();
  }

  Future<bool> _onWillPop() async {
    if (_commentSectionOffset == 0) {
      setState(() {
        _commentSectionOffset = -400;
        _showFab = true;
      });
      return false;
    }
    bool leave = await confirmLeaving(context.read<StreamingCubit>().isHost);
    if (leave) {
      // ignore: use_build_context_synchronously
      context.read<StreamingCubit>().leaveLivestream();
    }
    return false;
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  void _hideChatSection() {
    setState(() {
      _commentSectionOffset = -400;
      _showFab = true;
    });
  }

  void _showChatSecction() {
    setState(() {
      _commentSectionOffset = 0;
      _showFab = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    print("sliman buit streamingscreenprotrait");
    return BlocListener<StreamingCubit, StreamingState>(
      listenWhen: (previous, current) => current is VideoDimensionsChanged,
      listener: (context, state) {
        if (state is VideoDimensionsChanged) {
          setState(() {});
        }
      },
      child: WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: AppColors.mainBlue20Color,
          body: Stack(
            children: [
              Column(
                children: [
                  Builder(builder: (context) {
                    return AnimatedContainer(
                        color: AppColors.mainBlue20Color,
                        duration: const Duration(milliseconds: 200),
                        height: context
                            .read<StreamingCubit>()
                            .videoHeight
                            .toDouble(),
                        child: Stack(
                          children: [
                            BackdropFilter(
                              filter: ImageFilter.blur(sigmaX: 40, sigmaY: 40),
                              child: VideoView(
                                backPressed: _onWillPop,
                                isPortrait: true,
                              ),
                            )
                          ],
                        ));
                  }),
                  Expanded(
                    child: DescriptionView(showChatSection: _showChatSecction),
                  ),
                ],
              ),
              AnimatedPositioned(
                duration: _chatSectionAnimateDuration,
                bottom: _commentSectionOffset,
                left: 0,
                right: 0,
                height: 400,
                child: ChatView(
                    focusNode: _focusNode,
                    close: _hideChatSection,
                    handleDragEnd: _handleDragEnd,
                    handleDragUpdate: _handleDragUpdate),
              ),
            ],
          ),
          floatingActionButtonLocation: ExpandableFab.location,
          floatingActionButton: context.read<StreamingCubit>().isHost
              ? Visibility(
                  maintainState: true,
                  visible: _showFab,
                  child: const ControlsFab())
              : null,
        ),
      ),
    );
  }

  void _handleDragUpdate(DragUpdateDetails details) {
    _chatSectionAnimateDuration = Duration.zero;
    _commentSectionOffset -= details.delta.dy;
    if (_commentSectionOffset < -400) {
      _commentSectionOffset = -400;
    } else if (_commentSectionOffset > 0) {
      _commentSectionOffset = 0;
    }
    setState(() {});
  }

  void _handleDragEnd(DragEndDetails details) {
    _chatSectionAnimateDuration = const Duration(milliseconds: 300);
    if (_commentSectionOffset < -200) {
      _commentSectionOffset = -400;
      _showFab = true;
    } else {
      _commentSectionOffset = 0;
      _showFab = false;
    }
    setState(() {});
  }

  Future<bool> confirmLeaving(bool isHost) async {
    bool result = await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          //actionsAlignment: MainAxisAlignment.spaceBetween,
          shadowColor: Colors.black.withOpacity(0.1),
          backgroundColor: AppColors.mainBlue20Color,
          title: const Text(
            "Are you sure ?.",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
          content: Text(
            "Do you want to ${isHost ? "end" : "leave"} the livestream",
            style: const TextStyle(color: Colors.white),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context, true);
              },
              child: Text(
                "Yes",
                style: TextStyle(color: Colors.white),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.pop(context, false);
              },
              child: const Text(
                "No",
                style: TextStyle(color: Colors.red),
              ),
            ),
          ],
        );
      },
    );
    return result;
  }
}
