import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../logic/cubits/streaming_cubit.dart';
import 'widgets/video_view.dart';

class StreamingScreenLandscape extends StatelessWidget {
  const StreamingScreenLandscape({super.key});

  @override
  Widget build(BuildContext context) {
    print("sliman buit streaming_screen_landscape");

    return WillPopScope(
        onWillPop: () async {
          context.read<StreamingCubit>().changeScreenOreintation(false);
          return false;
        },
        child: Scaffold(
          body: VideoView(
            isPortrait: false,
          ),
        ));
  }
}
