import 'package:animated_button/animated_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:school_erp/ui/shared/colors.dart';

import '../../../../logic/cubits/streaming_cubit.dart';
import '../../../global_widgets/spinner_indicator.dart';

class DescriptionView extends StatelessWidget {
  final VoidCallback showChatSection;
  const DescriptionView({super.key, required this.showChatSection});

  @override
  Widget build(BuildContext context) {
    print("sliman built description_view");
    final livestream = context.read<StreamingCubit>().livestream;

    return Column(
      children: [
        Expanded(
          flex: 1,
          child: Padding(
            padding: const EdgeInsets.only(top: 20, right: 25, left: 25),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      livestream.title,
                      style: const TextStyle(
                          color: Colors.white,
                          fontSize: 23,
                          fontWeight: FontWeight.w500,
                          overflow: TextOverflow.ellipsis),
                    ),
                    IconButton(
                      onPressed: () {
                        /* showModalBottomSheet(
                          context: context,
                          builder: (context) {
                            return Container(
                              height: 400,
                              child: Text(livestream.description),
                            );
                          },
                        ); */
                      },
                      icon: const Icon(Icons.info, color: Colors.white),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Icon(Icons.remove_red_eye_outlined,
                        color: Colors.white54),
                    BlocBuilder<StreamingCubit, StreamingState>(
                      buildWhen: (previous, current) =>
                          current is StreamingUpdated ||
                          current is StreamingStarted,
                      builder: (context, state) {
                        if (state is StreamingUpdated ||
                            state is StreamingStarted) {
                          return Text(
                            "  ${context.read<StreamingCubit>().livestream.viewCount} Watching",
                            style: const TextStyle(color: Colors.white54),
                          );
                        }

                        return const SpinnerIndicator(
                          size: 30,
                        );
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                AnimatedButton(
                  shadowDegree: ShadowDegree.dark,
                  height: 50,
                  width: MediaQuery.of(context).size.width * 0.3,
                  onPressed: showChatSection,
                  child: Text(
                    "CHAT",
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 20,
                      color: AppColors.mainBlue20Color,
                    ),
                  ),
                  color: AppColors.mainWhiteColor.withOpacity(0.6),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
