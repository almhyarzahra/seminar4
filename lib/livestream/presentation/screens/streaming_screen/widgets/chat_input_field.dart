import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:progress_state_button/iconed_button.dart';
import 'package:progress_state_button/progress_button.dart';
import 'package:school_erp/ui/shared/colors.dart';

import '../../../../logic/cubits/streaming_chat_cubit.dart';

class ChatInputField extends StatefulWidget {
  final FocusNode fieldFocusNode;
  const ChatInputField({super.key, required this.fieldFocusNode});

  @override
  ChatInputFieldState createState() => ChatInputFieldState();
}

class ChatInputFieldState extends State<ChatInputField> {
  late TextEditingController _controller;
  ButtonState _btnState = ButtonState.idle;
  @override
  void initState() {
    _controller = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              style: TextStyle(color: Colors.white),
              controller: _controller,
              focusNode: widget.fieldFocusNode,
              decoration: InputDecoration(
                filled: true,
                fillColor: AppColors.mainBlue20Color,
                hintText: 'Type a message!',
                hintStyle: TextStyle(color: Colors.white),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20.0),
                  borderSide: const BorderSide(
                    width: 0,
                    style: BorderStyle.none,
                  ),
                ),
                contentPadding: const EdgeInsets.all(10),
              ),
            ),
          ),
        ),
        BlocListener<StreamingChatCubit, StreamingChatState>(
          listenWhen: (previous, current) => current is StreamingChatSendState,
          listener: (context, state) {
            if (state is StreamingChatSendSucceed) {
              setState(() {
                _btnState = ButtonState.success;
              });
              Timer(const Duration(milliseconds: 1000), () {
                setState(() {
                  _btnState = ButtonState.idle;
                });
              });
            } else if (state is StreamingChatSendFailed) {
              setState(() {
                _btnState = ButtonState.fail;
              });
              Timer(const Duration(milliseconds: 1000), () {
                setState(() {
                  _btnState = ButtonState.idle;
                });
              });
            } else {
              setState(() {
                _btnState = ButtonState.loading;
              });
            }
          },
          child: SizedBox(width: 50, height: 45, child: sendButton()),
        ),
      ],
    );
  }

  ProgressButton sendButton() {
    return ProgressButton.icon(
        iconedButtons: {
          ButtonState.idle: IconedButton(
              icon: Icon(Icons.send, color: Colors.white),
              color: AppColors.mainBlue20Color),
          ButtonState.loading:
              IconedButton(text: "Loading", color: AppColors.mainBlue20Color),
          ButtonState.fail: IconedButton(
              // text: "Failed",
              icon: const Icon(Icons.cancel, color: Colors.white),
              color: Colors.red.shade300),
          ButtonState.success: IconedButton(
              //text: "Sent",
              icon: const Icon(
                Icons.check_circle,
                color: Colors.white,
              ),
              color: Colors.green.shade400)
        },
        onPressed: () {
          if (_btnState != ButtonState.loading &&
              _controller.text.isNotEmpty &&
              context.read<StreamingChatCubit>().state is! MessagesProcessing) {
            context
                .read<StreamingChatCubit>()
                .sendMessage(message: _controller.text.trim());
            widget.fieldFocusNode.unfocus();
            _controller.clear();
          }
        },
        state: _btnState);
  }
}
