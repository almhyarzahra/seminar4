import 'dart:async';
import 'dart:ui';

import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:timer_builder/timer_builder.dart';

import '../../../../logic/cubits/streaming_cubit.dart';
import '../../../global_widgets/utils/time_formater.dart';

class VideoView extends StatefulWidget {
  final bool isPortrait;

  final Future<bool> Function()? backPressed;
  const VideoView({
    super.key,
    required this.isPortrait,
    this.backPressed,
  });

  @override
  State<VideoView> createState() => _VideoViewState();
}

class _VideoViewState extends State<VideoView> {
  VideoViewController? _videoController;
  bool _showButtons = false;
  Timer? buttonsDisapperTimer;
  @override
  void dispose() {
    buttonsDisapperTimer?.cancel();
    super.dispose();
  }

  void _startButtonsDisapperTimer() {
    buttonsDisapperTimer?.cancel();
    buttonsDisapperTimer = Timer(const Duration(seconds: 3), () {
      setState(() {
        _showButtons = false;
      });
    });
  }

  Widget _agoraVideoPalyer(BuildContext context) {
    return BlocBuilder<StreamingCubit, StreamingState>(
      buildWhen: (previous, current) =>
          current is StreamingStarted || current is StopedPreview,
      builder: (context, state) {
        if (state is StreamingStarted) {
          StreamingCubit sCubit = context.read<StreamingCubit>();
          if (state.remoteUid != null && _videoController == null) {
            _videoController = _videoController = VideoViewController.remote(
                rtcEngine: sCubit.agoraEngine,
                canvas: VideoCanvas(
                  uid: state.remoteUid,
                  renderMode: widget.isPortrait && sCubit.videoHeight == 240
                      ? RenderModeType.renderModeFit
                      : null,
                ),
                connection: RtcConnection(channelId: sCubit.livestream.id));
          } else if (state.remoteUid == null) {
            _videoController = VideoViewController(
              rtcEngine: sCubit.agoraEngine,
              canvas: VideoCanvas(
                sourceType: sCubit.isScreenShared
                    ? VideoSourceType.videoSourceScreen
                    : null,
                uid: 0,
                // renderMode: RenderModeType.renderModeFit,
              ),
            );
          }
          return GestureDetector(
            onTap: () {
              setState(() {
                _showButtons = true;
              });
              _startButtonsDisapperTimer();
            },
            child: AgoraVideoView(
              controller: _videoController!,
            ),
          );
        } else if (state is StopedPreview) {
          return Container(
            alignment: Alignment.center,
            color: const Color.fromARGB(1, 51, 200, 246),
            child: Icon(
              Icons.videocam_off,
              size: 70,
              color: Colors.black45,
            ),
          );
        }
        return Container(
          color: AppColors.mainBlue250Color,
          child: const Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    print("skiman buit video_view");
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Hero(
      tag: 1,
      child: Stack(
        children: [
          Container(
            width: screenWidth,
            //height: screenHeight / 2,
            decoration: const BoxDecoration(
              color: Color.fromARGB(255, 161, 255, 228),
            ),
            child: _agoraVideoPalyer(
              context,
            ),
          ),
          if (_showButtons)
            Positioned(
              top: 50,
              left: 15,
              child: IconButton(
                icon: const Icon(Icons.arrow_back_ios, color: Colors.grey),
                onPressed: () {
                  if (widget.backPressed != null) {
                    widget.backPressed!();
                  }
                },
              ),
            ),
          if (_showButtons)
            Positioned(
              bottom: 30,
              left: 20,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TimerBuilder.periodic(
                    const Duration(seconds: 1),
                    builder: (context) {
                      final duration = DateTime.now().difference(
                          context.read<StreamingCubit>().livestream.startedAt);
                      final formattedDuration =
                          TimeFormater.formatTime(duration);
                      return Text(
                        "• Live :$formattedDuration",
                        maxLines: 1,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                          color: Colors.red,
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                      );
                    },
                  ),
                  //  _networkStatusIndicator(context),
                ],
              ),
            ),
          if (_showButtons)
            Positioned(
              top: 50,
              right: 20,
              child: IconButton(
                icon: const Icon(Icons.settings, color: Colors.grey),
                onPressed: () {},
              ),
            ),
          if (_showButtons)
            Positioned(
              bottom: 17,
              right: 20,
              child: IconButton(
                icon: const Icon(Icons.fullscreen, color: Colors.grey),
                onPressed: () {
                  context
                      .read<StreamingCubit>()
                      .changeScreenOreintation(widget.isPortrait);
                },
              ),
            ),
        ],
      ),
    );
  }
}
