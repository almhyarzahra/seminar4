import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../bloc/auth/auth_bloc.dart';
import '../../../../data/models/message_model.dart';
import '../../../../logic/cubits/streaming_chat_cubit.dart';
import 'message_card.dart';

class ChatList extends StatefulWidget {
  final List<Message> messages;

  const ChatList({
    super.key,
    required this.messages,
  });

  @override
  State<ChatList> createState() => _ChatListState();
}

class _ChatListState extends State<ChatList> {
  late ScrollController _controller;
  @override
  void initState() {
    _controller = ScrollController();
    Timer(const Duration(milliseconds: 10), () {
      _controller.animateTo(
        _controller.position.maxScrollExtent + 100,
        duration: const Duration(milliseconds: 300),
        curve: Curves.elasticInOut,
      );
    });
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<StreamingChatCubit, StreamingChatState>(
      listener: (context, state) {
        if (state is MessagesUpdated) {
          _controller.animateTo(
            _controller.position.maxScrollExtent + 100,
            duration: const Duration(milliseconds: 300),
            curve: Curves.easeInOut,
          );
        }
      },
      child: ListView.builder(
        controller: _controller,
        itemCount: widget.messages.length,
        itemBuilder: (context, index) {
          return MessageCard(
              message: widget.messages[index],
              userUUID: context.read<AuthBloc>().state.user!.id!);
        },
      ),
    );
  }
}
