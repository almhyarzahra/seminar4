import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:school_erp/ui/shared/colors.dart';

import '../../../../data/models/message_model.dart';
import '../../../../logic/cubits/streaming_cubit.dart';
import '../../../global_widgets/utils/names_color_generator.dart';
import '../../../global_widgets/utils/time_formater.dart';

class MessageCard extends StatelessWidget {
  final Message message;
  final int userUUID;

  const MessageCard({
    super.key,
    required this.message,
    required this.userUUID,
  });

  @override
  Widget build(BuildContext context) {
    Duration s = message.sentAt
        .difference(context.read<StreamingCubit>().livestream.startedAt);
    bool localMessage = userUUID == int.parse(message.senderId);
    bool shortMessage =
        message.messageBody.length < 10; // adjust the threshold as needed

    return Align(
      alignment: localMessage ? Alignment.centerRight : Alignment.centerLeft,
      child: Card(
        elevation: 1,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        color: localMessage
            ? AppColors.mainBlue20Color.withAlpha(20)
            : AppColors.mainBlue20Color.withAlpha(100),
        margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        child: Stack(
          children: [
            Padding(
              padding: EdgeInsets.only(
                left: !localMessage ? 10 : 20,
                right: !localMessage ? 20 : 10,
                top: 5,
                bottom: 23,
              ),
              child: !localMessage
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          message.senderName,
                          style: TextStyle(
                              color: NamesColorGenerator.getColor(
                                int.parse(message.senderId),
                              ),
                              fontWeight: FontWeight.w500),
                        ),
                        Text(
                          message.messageBody,
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                          ),
                        ),
                      ],
                    )
                  : Text(
                      message.messageBody,
                      //textAlign: TextAlign.right,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
            ),
            Positioned(
              bottom: 4,
              left: !localMessage ? 12 : null,
              right: !localMessage ? null : 12,
              child: Text(
                TimeFormater.formatTime(s),
                style: const TextStyle(
                  fontSize: 13,
                  color: Colors.white60,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
