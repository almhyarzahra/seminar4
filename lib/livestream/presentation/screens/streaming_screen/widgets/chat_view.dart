import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:school_erp/ui/shared/colors.dart';

import '../../../../logic/cubits/streaming_chat_cubit.dart';
import '../../../global_widgets/spinner_indicator.dart';
import 'chat_input_field.dart';
import 'chat_list.dart';

class ChatView extends StatelessWidget {
  final FocusNode focusNode;
  final VoidCallback close;
  final Function(DragUpdateDetails) handleDragUpdate;
  final Function(DragEndDetails) handleDragEnd;

  const ChatView(
      {super.key,
      required this.focusNode,
      required this.close,
      required this.handleDragUpdate,
      required this.handleDragEnd});
  @override
  Widget build(BuildContext context) {
    print("sliman built chat_view");
    return Stack(children: [
      Padding(
        padding: const EdgeInsets.only(bottom: 50),
        child: Column(
          children: [
            GestureDetector(
              onVerticalDragUpdate: handleDragUpdate,
              onVerticalDragEnd: handleDragEnd,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                width: double.infinity,
                color: AppColors.mainWhiteColor,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Live Chat",
                      style: TextStyle(
                          color: AppColors.mainBlue20Color, fontSize: 17),
                    ),
                    SizedBox(
                      width: 2,
                    ),
                    Icon(
                      Icons.drag_handle,
                      color: AppColors.mainBlue20Color,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    IconButton(
                        onPressed: close,
                        icon: Icon(
                          Icons.cancel_outlined,
                          color: AppColors.mainBlue20Color,
                          size: 27,
                        ))
                  ],
                ),
              ),
            ),
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(bottom: 25),
                color: AppColors.mainBlue20Color,
                child: BlocBuilder<StreamingChatCubit, StreamingChatState>(
                  buildWhen: (previous, current) =>
                      current is StreamingChatMessagesState,
                  builder: (context, state) {
                    if (state is MessagesUpdated) {
                      return ChatList(
                        messages: state.messages,
                      );
                    }
                    return SpinnerIndicator();
                  },
                ),
              ),
            ),
          ],
        ),
      ),
      Positioned(
          bottom: MediaQuery.of(context).viewInsets.bottom,
          left: 0,
          right: 0,
          child: Container(
            color: AppColors.mainWhiteColor.withOpacity(0.8),
            padding: const EdgeInsets.all(4),
            child: ChatInputField(fieldFocusNode: focusNode),
          )) // bottom: MediaQuery.of(context).viewInsets.bottom,
    ]);
  }
}
