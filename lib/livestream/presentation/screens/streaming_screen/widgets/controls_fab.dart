import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_expandable_fab/flutter_expandable_fab.dart';
import 'package:school_erp/ui/shared/colors.dart';

import '../../../../logic/cubits/streaming_cubit.dart';

/// A widget that displays a floating action button with various controls for a streaming video.
class ControlsFab extends StatefulWidget {
  const ControlsFab({Key? key}) : super(key: key);

  @override
  State<ControlsFab> createState() => _ControlsFabState();
}

class _ControlsFabState extends State<ControlsFab> {
  bool _isSharingScreenProcessing = false;
  bool _isCameraButtonsClickable = true;

  @override
  Widget build(BuildContext context) {
    return ExpandableFab(
      closeButtonStyle: ExpandableFabCloseButtonStyle(
          backgroundColor: AppColors.mainWhiteColor,
          child: Icon(
            Icons.close,
            color: AppColors.mainBlue20Color,
          )),
      backgroundColor: AppColors.mainWhiteColor,
      child: Icon(
        Icons.settings,
        color: AppColors.mainBlue20Color,
      ),
      children: [
        // Share screen button
        context.read<StreamingCubit>().isCameraOff
            ? Container()
            : _isSharingScreenProcessing
                ? CircularProgressIndicator(
                    color: AppColors.mainWhiteColor,
                  )
                : FloatingActionButton.extended(
                    backgroundColor: AppColors.mainWhiteColor,
                    onPressed: () async {
                      // Check if streaming is already processing
                      if (context.read<StreamingCubit>().state
                          is StreamingProcessing) {
                        return;
                      }

                      // Start sharing screen
                      setState(() {
                        _isSharingScreenProcessing = true;
                        if (!context.read<StreamingCubit>().isScreenShared) {
                          _isCameraButtonsClickable = false;
                        }
                      });
                      await context.read<StreamingCubit>().shareScreen();

                      // Finish sharing screen
                      setState(() {
                        _isSharingScreenProcessing = false;
                        if (!context.read<StreamingCubit>().isScreenShared) {
                          _isCameraButtonsClickable = true;
                        }
                      });
                    },
                    label: Text(
                      context.read<StreamingCubit>().isScreenShared
                          ? "Finish share"
                          : "Share Screen",
                      style: TextStyle(color: AppColors.mainBlue20Color),
                    ),
                  ),

        // Camera switch button
        _isCameraButtonsClickable
            ? FloatingActionButton.small(
                backgroundColor: AppColors.mainWhiteColor,
                heroTag: null,
                onPressed: () {
                  context.read<StreamingCubit>().switchCamera();
                },
                child: Icon(
                  Icons.switch_camera,
                  color: AppColors.mainBlue20Color,
                ),
              )
            : Container(),

        // Mute button
        FloatingActionButton.small(
          backgroundColor: AppColors.mainWhiteColor,
          heroTag: null,
          child: Icon(
            context.read<StreamingCubit>().isMuted ? Icons.mic_off : Icons.mic,
            color: AppColors.mainBlue20Color,
          ),
          onPressed: () {
            context.read<StreamingCubit>().toggleMute();
            setState(() {});
          },
        ),

        // Video sharing button
        _isCameraButtonsClickable
            ? FloatingActionButton.small(
                backgroundColor: AppColors.mainWhiteColor,
                heroTag: null,
                onPressed: () {
                  context.read<StreamingCubit>().toggleVideoSharring();
                  setState(() {});
                },
                child: Icon(
                  context.read<StreamingCubit>().isCameraOff
                      ? Icons.videocam_off
                      : Icons.video_camera_front,
                  color: AppColors.mainBlue20Color,
                ),
              )
            : Container(),
      ],
    );
  }
}
