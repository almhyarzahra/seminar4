import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:school_erp/livestream/presentation/screens/home_screen/livestream_card_2.dart';
import 'package:school_erp/ui/shared/colors.dart';

import '../../../../bloc/auth/auth_bloc.dart';
import '../../../../ui/shared/custom_widget/custom_text.dart';
import '../../../logic/cubits/sync_livestreams_cubit.dart';
import '../../global_widgets/overlapping_images_row.dart';
import 'livestream_card.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    int id = context.read<AuthBloc>().state.user!.id!;
    BlocProvider.of<SyncLivestreamsCubit>(context).connect(id: id.toString());
    super.initState();
  }

  /* @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.red,
        child: Padding(
          padding: const EdgeInsets.all(50.0),
          child: Container(
              height: 100,
              width: 200,
              color: Colors.green,
              child: Column(
                children: [
                  Text("DSAdasdas"),
                  OverlappingImagesRow(imageUrls: [
                    'https://picsum.photos/seed/picsum/300/300',
                    'https://picsum.photos/seed/picsum/300/300',
                    'https://picsum.photos/seed/picsum/300/300',
                    'https://picsum.photos/seed/picsum/300/300',
                    'https://picsum.photos/seed/picsum/300/300',
                    'https://picsum.photos/seed/picsum/300/300',
                    'https://picsum.photos/seed/picsum/300/300',
                    'https://picsum.photos/seed/picsum/300/300',
                    'https://picsum.photos/seed/picsum/300/300',
                    'https://picsum.photos/seed/picsum/300/300',
                    'https://picsum.photos/seed/picsum/300/300',
                  ]),
                ],
              )),
        ));
  } */

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    print("bi");

    return BlocListener<SyncLivestreamsCubit, LsState>(
      listener: (context, state) {
        if (state is LsConnected) {
          if (state.onAirLivetreams != null &&
              state.onAirLivetreams!.length == 0) {
            Fluttertoast.showToast(msg: "connected");
          }
        }
      },
      child: Container(
          color: AppColors.mainBlue250Color,
          child: Column(
            children: [
              SizedBox(
                height: 200,
                width: 300,
                child: Image.asset('images/go_live_logo.png',
                    fit: BoxFit.fitWidth),
              ),
              Padding(
                padding: EdgeInsetsDirectional.only(
                    start: 20, end: 30, top: 10, bottom: 10),
                child: CustomText(
                  content: "Online lectures",
                  colorText: AppColors.mainBlackColor,
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(
                    top: 20,
                    // right: size.width * 0.05,
                    //left: size.width * 0.05,
                  ),
                  decoration: BoxDecoration(
                      color: AppColors.mainWhiteColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(45),
                        topRight: Radius.circular(45),
                      )),
                  child: BlocBuilder<SyncLivestreamsCubit, LsState>(
                    buildWhen: (previous, current) {
                      if (current == previous && current is LsDisconnected) {
                        return false;
                      }
                      return true;
                    },
                    builder: (context, state) {
                      if (state is LsConnected) {
                        return state.onAirLivetreams == null ||
                                state.onAirLivetreams == null ||
                                state.onAirLivetreams!.isEmpty
                            ? const Center(
                                child: Text(
                                  '',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 40,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              )
                            : ListView.builder(
                                itemBuilder: (context, index) {
                                  return LivestreamCard2(
                                    liveStream: state.onAirLivetreams![index],
                                  );
                                },
                                itemCount: state.onAirLivetreams!.length,
                              );
                      } else if (state is LsDisconnected) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.wifi_off,
                              size: 80,
                              color: AppColors.mainBlue250Color,
                            ),
                            const SizedBox(
                              width: double.infinity,
                              height: 10,
                            ),
                            Text(
                              "Please check your internet connection!",
                              style: TextStyle(
                                color: AppColors.mainBlackColor,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        );
                      }
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    },
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
/* BlocBuilder<SyncLivestreamsCubit, LsState>(
          buildWhen: (previous, current) {
            if (current == previous && current is LsDisconnected) {
              return false;
            }
            return true;
          },
          builder: (context, state) {
            if (state is LsConnected) {
              return state.onAirLivetreams == null ||
                      state.onAirLivetreams == null ||
                      state.onAirLivetreams!.isEmpty
                  ? const Center(
                      child: Text(
                        'No one is live now',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    )
                  : ListView.builder(
                      itemBuilder: (context, index) {
                        return LivestreamCard(
                          livestream: state.onAirLivetreams![index],
                        );
                      },
                      itemCount: state.onAirLivetreams!.length,
                    );
            } else if (state is LsDisconnected) {
              return const Center(
                child: Text("check your internet connection!"),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),*/