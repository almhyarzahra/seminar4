import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:school_erp/ui/shared/colors.dart';

import 'package:timer_builder/timer_builder.dart';

import '../../../../bloc/auth/auth_bloc.dart';
import '../../../../locator.dart';
import '../../../data/models/livestream_model.dart';
import '../../../data/network_services/livestream_ns.dart';
import '../../../data/repositories/livestream_chat_repository.dart';
import '../../../data/repositories/streaning_repository.dart';
import '../../../logic/cubits/streaming_chat_cubit.dart';
import '../../../logic/cubits/streaming_cubit.dart';
import '../../global_widgets/utils/time_formater.dart';
import '../streaming_screen/streaming_screen_wrapper.dart';

class LivestreamCard extends StatelessWidget {
  final Livestream livestream;

  const LivestreamCard({Key? key, required this.livestream}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width * 0.9;
    final double cardHeight = screenWidth / 2.3;
    final double padding = screenWidth / 20;
    final double imageSize = screenWidth / 3.5;
    final double titleFontSize = screenWidth / 20;
    final double broadcasterFontSize = screenWidth / 25;
    final double tapWatchFontSize = screenWidth / 30;
    final TextStyle titleStyle = TextStyle(
      color: AppColors.mainBlackColor,
      fontSize: titleFontSize,
      fontWeight: FontWeight.bold,
      letterSpacing: 1,
      wordSpacing: 1,
    );
    final TextStyle broadcasterStyle = TextStyle(
      color: AppColors.mainBlackColor,
      fontSize: broadcasterFontSize,
      fontWeight: FontWeight.w600,
    );
    final TextStyle timerStyle = TextStyle(
      color: AppColors.mainBlackColor,
      fontSize: broadcasterFontSize,
      fontWeight: FontWeight.w600,
    );
    final TextStyle tapToKnowMoreStyle = TextStyle(
      color: Colors.black,
      fontSize: tapWatchFontSize,
    );

    return Container(
      height: cardHeight,
      width: screenWidth,
      padding: EdgeInsets.fromLTRB(padding, 0, padding, padding),
      child: InkWell(
        onTap: () {
          goToStreamingScreen(livestream, context);
        },
        child: Container(
          decoration: BoxDecoration(
            color: AppColors.mainBlueColor.withOpacity(0.9),
            borderRadius: BorderRadius.all(Radius.circular(30)),
            border: Border.all(
              color: AppColors.mainBlue10Color.withOpacity(.1),
              width: 1,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.all(padding / 2),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: imageSize,
                  width: imageSize,
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(.2),
                    borderRadius: BorderRadius.circular(20),
                    image: DecorationImage(
                      image: NetworkImage(livestream.imageUrl),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(width: padding / 2),
                SizedBox(
                  width: screenWidth / 2.05,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        livestream.title,
                        maxLines: 2,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.start,
                        style: titleStyle,
                      ),
                      Text(
                        livestream.broadcaster,
                        maxLines: 1,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                        style: broadcasterStyle,
                      ),
                      TimerBuilder.periodic(
                        const Duration(seconds: 1),
                        builder: (context) {
                          final duration =
                              DateTime.now().difference(livestream.startedAt);
                          final formattedDuration =
                              TimeFormater.formatTime(duration);
                          return Text(
                            "Live for $formattedDuration",
                            maxLines: 1,
                            softWrap: true,
                            overflow: TextOverflow.ellipsis,
                            style: timerStyle,
                          );
                        },
                      ),
                      Text(
                        '${livestream.viewCount.toString()} watching',
                        style: TextStyle(color: AppColors.mainBlackColor),
                      ),
                      Text(
                        'Tap to Watch',
                        maxLines: 1,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                        style: tapToKnowMoreStyle,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void goToStreamingScreen(Livestream livestream, BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) {
        StreamingCubitArgumnets args = StreamingCubitArgumnets(
            isHost: false,
            livestream: livestream,
            repo: StreamingRepository(
                livestream: livestream, ns: locator<LivestreamNS>()),
            userUUID: context.read<AuthBloc>().state.user!.id!);
        return MultiBlocProvider(providers: [
          BlocProvider<StreamingCubit>(
            create: (context) => StreamingCubit(args: args),
          ),
          BlocProvider<StreamingChatCubit>(
            create: (context) => StreamingChatCubit(
                livestreamId: args.livestream.id,
                repo: LivestreamChatRepository(ns: locator<LivestreamNS>())),
          )
        ], child: StreamingScreenWrapper());
      },
    ));
  }
}
