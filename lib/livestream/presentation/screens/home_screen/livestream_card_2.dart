import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:timer_builder/timer_builder.dart';

import '../../../../bloc/auth/auth_bloc.dart';
import '../../../../locator.dart';
import '../../../constants/networkconfig.dart';
import '../../../data/network_services/livestream_ns.dart';
import '../../../data/repositories/livestream_chat_repository.dart';
import '../../../data/repositories/streaning_repository.dart';
import '../../../logic/cubits/streaming_chat_cubit.dart';
import '../../../logic/cubits/streaming_cubit.dart';
import '../../global_widgets/utils/ls_user_fetcher.dart';
import '../../global_widgets/utils/time_formater.dart';
import '../../global_widgets/utils/user_subjects_fetcher.dart';
import '../../global_widgets/overlapping_images_row.dart';
import '../../../data/models/livestream_model.dart';
import 'package:shimmer/shimmer.dart';

import '../streaming_screen/streaming_screen_wrapper.dart';

class LivestreamCard2 extends StatelessWidget {
  final Livestream liveStream;

  LivestreamCard2({required this.liveStream});

  @override
  Widget build(BuildContext context) {
    print(UserSubjectsFetcher.subjects);
    print(liveStream);
    final Size size = MediaQuery.of(context).size;
    double width = size.width;
    double height = size.height;

    Widget _broadcasterNamefutureBuilder = FutureBuilder<LsUser?>(
      future: LsUserFetcher.fetchLsUser(
        int.parse(liveStream.broadcaster),
      ),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Shimmer.fromColors(
            baseColor: AppColors.mainBlue10Color,
            highlightColor: AppColors.mainBlueColor,
            child: Container(
              width: 50,
              height: 20,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: AppColors.mainWhiteColor),
            ),
          );
        } else if (snapshot.data != null) {
          return FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              snapshot.data!.Name.split(' ').length > 2
                  ? snapshot.data!.Name.split(' ')[0] +
                      ' ' +
                      snapshot.data!.Name.split(' ')[1]
                  : snapshot.data!.Name,
              style: TextStyle(
                  color: AppColors.mainGrey2Color,
                  fontWeight: FontWeight.bold,
                  fontSize: height / 45),
            ),
          );
        } else {
          return Text(
            'Error',
            style: TextStyle(
                color: AppColors.mainRedColor,
                fontWeight: FontWeight.bold,
                fontSize: height / 45),
          );
        }
      },
    );
    Widget _subjectFutureBuilder = FutureBuilder<int>(
      future: UserSubjectsFetcher.fetchSubjects(
          context.read<AuthBloc>().state.user!.id!),
      builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Shimmer.fromColors(
            baseColor: AppColors.mainBlue10Color,
            highlightColor: AppColors.mainBlueColor,
            child: Container(
              width: 60,
              height: 20,
              decoration: BoxDecoration(
                  // borderRadius: BorderRadius.circular(20),
                  color: AppColors.mainWhiteColor),
            ),
          ); // Display a progress indicator while loading
        } else {
          if (snapshot.data == 0) {
            return Text(
              "Error",
              style: TextStyle(
                color: AppColors.mainRedColor,
                fontWeight: FontWeight.bold,
              ),
            );
          }
          return Text(
            UserSubjectsFetcher.subjects
                .firstWhere(
                    (element) => element.Id == int.parse(liveStream.subjectID))
                .Name
                .capitalizeFirst!,
            style: TextStyle(
              color: AppColors.mainBlackColor,
              fontWeight: FontWeight.bold,
            ),
          );
        }
      },
    );
    Widget _broadcasterImageFutureBuilder = FutureBuilder<LsUser?>(
      future: LsUserFetcher.fetchLsUser(
        int.parse(liveStream.broadcaster),
      ),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Shimmer.fromColors(
            baseColor: AppColors.mainBlue10Color,
            highlightColor: AppColors.mainBlueColor,
            child: CircleAvatar(
              backgroundColor: AppColors.mainWhiteColor,
              radius: 30,
            ),
          );
        } else if (snapshot.data != null && snapshot.data!.image != null) {
          return CircleAvatar(
            backgroundImage:
                NetworkImage(LaravelApiEndPoint + snapshot.data!.image!),
            radius: 30,
          );
        } else {
          return CircleAvatar(
            backgroundColor: AppColors.mainGrey2Color,
            child: Icon(
              Icons.person,
              size: 40,
            ),
            radius: 30,
          );
        }
      },
    );
    return Padding(
      padding: const EdgeInsets.only(bottom: 10, right: 10, left: 10),
      child: InkWell(
        onTap: () {
          _goToStreamingScreen(liveStream, context);
        },
        child: Container(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(top: 30),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50)),
                    color: AppColors.mainBlue250Color),
                height: height / 6, // Adjust the height as per your requirement
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 20, right: 10, top: 6),
                      child: _broadcasterImageFutureBuilder,
                    ),
                    SizedBox(
                      height: height / 12,
                      width: width / 4,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          _broadcasterNamefutureBuilder,
                          SizedBox(
                            height: height / 100,
                          ),
                          Container(
                            // width: width / 4,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: AppColors.mainGrey2Color),
                            padding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 3),
                            // Customize the color as per your requirement
                            child: Text(
                              'Host',
                              style: TextStyle(
                                //fontSize: 15,
                                fontWeight: FontWeight.bold,
                                overflow: TextOverflow.ellipsis,
                                color: AppColors.mainBlackColor,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                          margin: EdgeInsets.all(8),
                          child: Text(
                            liveStream.description,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 4,
                            style: TextStyle(
                                fontSize: 15, color: AppColors.mainGrey2Color),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(5),
                color: AppColors.mainBlue250Color,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.remove_red_eye,
                          color: AppColors.mainWhiteColor,
                        ),
                        SizedBox(
                          width: 2,
                        ),
                        Text(
                          '${liveStream.viewCount} watching',
                          style: TextStyle(
                              color: AppColors.mainWhiteColor,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: AppColors.mainGrey2Color),
                      child: _subjectFutureBuilder,
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                height: height / 12,
                decoration: BoxDecoration(
                  border: Border.all(color: AppColors.mainBlue250Color),
                  color: AppColors.mainBlue250Color.withOpacity(0.4),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(50),
                      bottomRight: Radius.circular(50)),
                ),
                child: Row(
                  children: [
                    liveStream.usersWatching.length > 0
                        ? Expanded(
                            flex: 11,
                            child: OverlappingImagesRow(
                              lsParticipants: liveStream.usersWatching,
                            ),
                          )
                        : Expanded(
                            flex: 7,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 5),
                              width: 30,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: AppColors.mainBlue250Color),
                              child: Text(
                                'No one has joined yet',
                                style: TextStyle(
                                  color: AppColors.mainGrey2Color,
                                ),
                              ),
                            ),
                          ),
                    Expanded(
                      flex: 7,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            'LIVE',
                            style: TextStyle(
                                color: Colors.red, fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          TimerBuilder.periodic(
                            const Duration(seconds: 1),
                            builder: (context) {
                              final duration = DateTime.now()
                                  .difference(liveStream.startedAt);
                              final formattedDuration =
                                  TimeFormater.formatTime(duration);
                              return Text(
                                "$formattedDuration",
                                maxLines: 1,
                                softWrap: true,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: AppColors.mainRedColor,
                                    fontWeight: FontWeight.bold),
                              );
                            },
                          ),
                          const SizedBox(
                            width: 4,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _goToStreamingScreen(Livestream livestream, BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) {
        StreamingCubitArgumnets args = StreamingCubitArgumnets(
            isHost: false,
            livestream: livestream,
            repo: StreamingRepository(
                livestream: livestream, ns: locator<LivestreamNS>()),
            userUUID: context.read<AuthBloc>().state.user!.id!);
        return MultiBlocProvider(providers: [
          BlocProvider<StreamingCubit>(
            create: (context) => StreamingCubit(args: args),
          ),
          BlocProvider<StreamingChatCubit>(
            create: (context) => StreamingChatCubit(
                livestreamId: args.livestream.id,
                repo: LivestreamChatRepository(ns: locator<LivestreamNS>())),
          )
        ], child: StreamingScreenWrapper());
      },
    ));
  }
}
