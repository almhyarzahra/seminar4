import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:school_erp/livestream/logic/cubits/create_livestream_cubit.dart';
import 'package:school_erp/livestream/logic/cubits/sync_livestreams_cubit.dart';
import 'package:school_erp/livestream/presentation/screens/go_live_screen/go_live_screen.dart';
import 'package:school_erp/livestream/presentation/screens/home_screen/home_screen.dart';
import 'package:school_erp/locator.dart';

import '../../data/repositories/create_livestream_repository.dart';
import '../../data/repositories/sync_livestream_repository.dart';

Widget getLiveStreamView(bool isDoctor) {
  if (isDoctor) {
    return MultiBlocProvider(providers: [
      BlocProvider(
        create: (context) => SyncLivestreamsCubit(
          repo: locator<LiveStreamRepository>(),
        ),
      ),
      BlocProvider(
        create: (context) =>
            CreateLivestreamCubit(repo: locator<CreateLiveStreamRepository>()),
      )
    ], child: GoLiveScreen());
  }
  return BlocProvider(
    create: (context) => SyncLivestreamsCubit(
      repo: locator<LiveStreamRepository>(),
    ),
    child: HomeScreen(),
  );
}
