import 'package:flutter/material.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:shimmer/shimmer.dart';

import '../../constants/networkconfig.dart';
import 'utils/ls_user_fetcher.dart';

class OverlappingImagesRow extends StatelessWidget {
  final List<String> lsParticipants; // List of image URLs
  final int maxImagesToShow; // Maximum number of images to display

  OverlappingImagesRow({
    required this.lsParticipants,
    this.maxImagesToShow = 5,
  });

  @override
  Widget build(BuildContext context) {
    // Calculate the number of images to show
    final int numImagesToShow = lsParticipants.length > maxImagesToShow
        ? maxImagesToShow - 1
        : lsParticipants.length;

    return Row(
      children: [
        // Build the overlapping images
        for (int i = 0; i < numImagesToShow; i++)
          Transform.translate(
            offset: Offset(-i * 15.0, 0), // Adjust the spacing between images
            child: FutureBuilder<LsUser?>(
              future: LsUserFetcher.fetchLsUser(
                int.parse(lsParticipants[i]),
              ),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Shimmer.fromColors(
                    baseColor: AppColors.mainBlue10Color,
                    highlightColor: AppColors.mainBlueColor,
                    child: ClipOval(
                      child: Container(
                        width: 40,
                        height: 40,
                        color: AppColors.mainWhiteColor,
                      ),
                    ),
                  );
                } else if (snapshot.data != null &&
                    snapshot.data!.image != null) {
                  return ClipOval(
                    child: Image.network(
                      LaravelApiEndPoint + snapshot.data!.image!,
                      width: 40,
                      height: 40,
                      fit: BoxFit.cover,
                    ),
                  );
                } else {
                  return ClipOval(
                    child: Container(
                      width: 40,
                      height: 40,
                      color: AppColors.mainBlue20Color,
                      child: Icon(
                        Icons.person,
                        size: 30,
                        color: AppColors.mainWhiteColor,
                      ),
                    ),
                  );
                }
              },
            ),
          ),

        // Build the remaining count circle
        if (lsParticipants.length > maxImagesToShow)
          Transform.translate(
            offset: Offset(-numImagesToShow * 15.0, 0),
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: AppColors.mainBlue250Color,
              ),
              child: Center(
                child: Text(
                  '+${lsParticipants.length - maxImagesToShow + 1}',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
      ],
    );
  }
}
/*ClipOval(
              child: Image.asset(
                imageUrls[i],
                width: 40,
                height: 40,
                fit: BoxFit.cover,
              ),
            )*/

class OverlappingImagesRow2 extends StatelessWidget {
  final List<String> imageUrls; // List of image URLs
  final int maxImagesToShow; // Maximum number of images to display

  OverlappingImagesRow2({
    required this.imageUrls,
    this.maxImagesToShow = 5,
  });

  @override
  Widget build(BuildContext context) {
    // Calculate the number of images to show
    final int numImagesToShow = imageUrls.length > maxImagesToShow
        ? maxImagesToShow - 1
        : imageUrls.length;

    return Row(
      //scrollDirection: Axis.horizontal,
      children: [
        // Build the overlapping images
        for (int i = 0; i < numImagesToShow; i++)
          Transform.translate(
            offset: Offset(-i * 15.0, 0), // Adjust the spacing between images
            child: ClipOval(
              child: Image.asset(
                imageUrls[i],
                width: 40,
                height: 40,
                fit: BoxFit.cover,
              ),
            ),
          ),

        // Build the remaining count circle
        if (imageUrls.length > maxImagesToShow)
          Transform.translate(
            offset: Offset(-numImagesToShow * 15.0, 0),
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: AppColors.mainBlue250Color,
              ),
              child: Center(
                child: Text(
                  '+${imageUrls.length - maxImagesToShow + 1}',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
      ],
    );
  }
}
