import 'dart:math';

import 'package:flutter/material.dart';

class NamesColorGenerator {
  static final Map<int, Color> _colorMap = {};

  static Color getColor(int input) {
    if (_colorMap.containsKey(input)) {
      return _colorMap[input]!;
    } else {
      final newColor = _generateUniqueColor();
      _colorMap[input] = newColor;
      return newColor;
    }
  }

  static Color _generateUniqueColor() {
    final random = Random();
    final hue = random.nextDouble() * 360;
    final saturation = 0.5 + random.nextDouble() * 0.5;
    final value = 0.7 + random.nextDouble() * 0.3;
    return HSVColor.fromAHSV(1, hue, saturation, value).toColor();
  }
}
