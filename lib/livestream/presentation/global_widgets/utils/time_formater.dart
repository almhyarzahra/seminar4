class TimeFormater {
  static String formatTime(Duration duration) {
    if (duration.inSeconds < 60) {
      return '${duration.inSeconds}s';
    } else if (duration.inSeconds >= 60 && duration.inSeconds < 3600) {
      return '${duration.inMinutes}m ${duration.inSeconds.remainder(60)}s';
    } else if (duration.inSeconds >= 3600 && duration.inSeconds < 86400) {
      return '${duration.inHours}h ${duration.inMinutes.remainder(60)}m';
    } else if (duration.inSeconds >= 86400 && duration.inSeconds < 2592000) {
      return '${duration.inDays}d ${duration.inHours.remainder(24)}h ${duration.inMinutes.remainder(60)}m';
    } else if (duration.inSeconds >= 2592000 && duration.inSeconds < 31536000) {
      return '${duration.inDays ~/ 30}mo ${duration.inDays.remainder(30)}d ${duration.inHours.remainder(24)}h';
    } else {
      return '${duration.inDays ~/ 365}y ${duration.inDays.remainder(365)}d ${duration.inHours.remainder(24)}h';
    }
  }
}
