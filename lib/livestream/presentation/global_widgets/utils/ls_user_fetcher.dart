import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:school_erp/livestream/constants/networkconfig.dart';

class LsUserFetcher {
  static List<LsUser> _lsUsers = [];

  static Future<LsUser?> fetchLsUser(int userId) async {
    int searchIndex = _lsUsers.indexWhere((element) => element.Id == userId);

    if (searchIndex != -1) {
      return _lsUsers.elementAt(searchIndex);
    }
    final String url = "${LaravelApiEndPoint}/api/findUser";
    http.Response response;

    try {
      var headers = {'Content-Type': 'application/json'};
      var body = {'id': userId};
      response = await http.post(Uri.parse(url),
          headers: headers, body: jsonEncode(body));
      if (response.statusCode == 200) {
        var rawUser = jsonDecode(response.body)[0];
        LsUser lsUser = LsUser.fromJson(rawUser);
        if (!_lsUsers.contains(lsUser)) {
          _lsUsers.add(lsUser);
        }

        return lsUser;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }
}

class LsUser {
  final String Name;
  final int Id;
  final String? image;

  LsUser({required this.Name, required this.Id, required this.image});
  factory LsUser.fromJson(Map<String, dynamic> json) {
    return LsUser(
        Name: json['name'] as String,
        Id: json['id'] as int,
        image: json['photo_path'] as String?);
  }
  @override
  String toString() {
    return 'LsUser{ Name: $Name, Id: $Id , image $image }';
  }
}
