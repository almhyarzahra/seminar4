import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:school_erp/livestream/constants/networkconfig.dart';

class UserSubjectsFetcher {
  static List<UserSubject> subjects = [];
  static int _status = 0;
  static late int _userId;

  static Future<int> fetchSubjects(int userId) async {
    if (_status == 1 && userId == _userId) {
      return _status;
    }
    final String url = "${LaravelApiEndPoint}/api/findUser";
    http.Response response;

    try {
      var headers = {'Content-Type': 'application/json'};
      var body = {'id': userId};
      response = await http.post(Uri.parse(url),
          headers: headers, body: jsonEncode(body));
      if (response.statusCode == 200) {
        _status = 1;
        List<dynamic> rawSubjects = jsonDecode(response.body)[0]['subjects'];
        subjects.clear();
        for (var subject in rawSubjects) {
          UserSubject s = UserSubject.fromJson(subject);
          subjects.add(s);
        }
        _userId = userId;
      } else {
        _status = 0;
      }
    } catch (e) {
      print(e);
      _status = 0;
    }

    return _status;
  }

  static List<UserSubject> getSubjects() {
    return subjects;
  }
}

class UserSubject {
  final String Name;
  final int Id;

  UserSubject(this.Name, this.Id);
  factory UserSubject.fromJson(Map<String, dynamic> json) {
    return UserSubject(
      json['name'] as String,
      json['id'] as int,
    );
  }
  @override
  String toString() {
    return 'UserSubject{ Name: $Name, Id: $Id }';
  }
}
