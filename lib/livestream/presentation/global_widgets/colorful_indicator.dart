import 'package:flutter/material.dart';

class ColorfulCircularProgressIndicator extends StatefulWidget {
  const ColorfulCircularProgressIndicator({super.key});

  @override
  _ColorfulCircularProgressIndicatorState createState() =>
      _ColorfulCircularProgressIndicatorState();
}

class _ColorfulCircularProgressIndicatorState
    extends State<ColorfulCircularProgressIndicator>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<Color?> _animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    )..repeat();
    _animation = TweenSequence<Color?>(
      [
        TweenSequenceItem(
          tween: ColorTween(
            begin: Colors.red,
            end: Colors.orange,
          ),
          weight: 1,
        ),
        TweenSequenceItem(
          tween: ColorTween(
            begin: Colors.orange,
            end: Colors.yellow,
          ),
          weight: 1,
        ),
        TweenSequenceItem(
          tween: ColorTween(
            begin: Colors.yellow,
            end: Colors.green,
          ),
          weight: 1,
        ),
        TweenSequenceItem(
          tween: ColorTween(
            begin: Colors.green,
            end: Colors.blue,
          ),
          weight: 1,
        ),
        TweenSequenceItem(
          tween: ColorTween(
            begin: Colors.blue,
            end: Colors.indigo,
          ),
          weight: 1,
        ),
        TweenSequenceItem(
          tween: ColorTween(
            begin: Colors.indigo,
            end: Colors.purple,
          ),
          weight: 1,
        ),
        TweenSequenceItem(
          tween: ColorTween(
            begin: Colors.purple,
            end: Colors.red,
          ),
          weight: 1,
        ),
      ],
    ).animate(_controller);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
        strokeWidth: 10,
        valueColor: _animation,
      ),
    );
  }
}
