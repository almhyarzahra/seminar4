import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SpinnerIndicator extends StatelessWidget {
  final double? size;

  const SpinnerIndicator({super.key, this.size});
  @override
  Widget build(BuildContext context) {
    return Center(
        child: SpinKitCircle(
      size: size ?? 60,
      color: Colors.blue.withOpacity(0.7),
    ));
  }
}
