enum AuthMode { signup, login }

enum LivestreamSocketState {
  connected,
  disconnected,
  addToLiveStreamList,
  deleteFromLivestreamList,
  updateLivestreamList,
  createdLivesteamSuccessfully,
  createLiveStreamFailed,
  joinSuccess,
  joinFail,
  sentMessageSuccess,
  sentMessageFailed,
  recivedMessage
}

enum SyncLivestreamRepositoryState { listUpdated, notConnected }

enum CreateLivestreamReopsitoryState { createSucceed, createFailed }

enum StreamingRepositoryState {
  livestreamEnded,
  livestreamUpdated,
  joinsucceed,
  joinFailed
}

enum LivestreamChatRepositoryState { messagesUpdated, sentSuccess, sentFailed }
