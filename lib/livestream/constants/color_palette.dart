import 'package:flutter/material.dart';

class ColorPalette {
  static Color get purple =>
      const Color.fromARGB(255, 54, 39, 63).withOpacity(0.8);
  static Color get blue => Colors.blue;
}
