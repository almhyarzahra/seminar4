import '../../constants/enums.dart';

abstract class LivestreamNSObserver {
  void update(Map<LivestreamSocketState, dynamic> data);
}

abstract class Observerable {
  void notifyObservers(Map<LivestreamSocketState, dynamic> data);
  void attach(LivestreamNSObserver observer);
  void detach(LivestreamNSObserver observer);
}
