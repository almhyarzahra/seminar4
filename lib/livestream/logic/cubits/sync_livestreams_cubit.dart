import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../constants/enums.dart';
import '../../data/models/livestream_model.dart';
import '../../data/repositories/sync_livestream_repository.dart';

class SyncLivestreamsCubit extends Cubit<LsState> {
  final LiveStreamRepository repo;
  SyncLivestreamsCubit({required this.repo}) : super(LsInit()) {
    _listenToRepository();
  }

  void _listenToRepository() {
    repo.stream.listen((event) {
      switch (event.keys.first) {
        case SyncLivestreamRepositoryState.listUpdated:
          emit(LsConnected(event.values.first));
          break;
        case SyncLivestreamRepositoryState.notConnected:
          emit(LsDisconnected());
          break;
      }
    });
  }

  void connect({required String id}) async {
    emit(LsProcessing());
    final connectivityResult = await (Connectivity().checkConnectivity());

    if (connectivityResult == ConnectivityResult.none) {
      emit(LsDisconnected());
      return;
    }

    repo.connect(id: id);
  }

  @override
  Future<void> close() {
    repo.resetStream();
    return super.close();
  }

  @override
  void onChange(Change<LsState> change) {
    print('changed ${change.toString()}');
    if (change.nextState is LsConnected) {
      print(change.nextState.toString());
    }
    super.onChange(change);
  }
}

abstract class LsState {}

class LsInit extends LsState {}

class LsBuildState extends LsState {}

class LsProcessing extends LsBuildState {}

class LsConnected extends LsBuildState {
  final List<Livestream>? onAirLivetreams;

  LsConnected(this.onAirLivetreams);
}

class LsDisconnected extends LsBuildState {}
