import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../constants/enums.dart';
import '../../data/models/livestream_model.dart';
import '../../data/repositories/create_livestream_repository.dart';

class CreateLivestreamCubit extends Cubit<CreateLivestreamState> {
  final CreateLiveStreamRepository repo;
  late Livestream _createdLivestream;
  late StreamSubscription<Map<CreateLivestreamReopsitoryState, dynamic>> _sub;
  CreateLivestreamCubit({required this.repo}) : super(CreateLivestreamInit()) {
    _observRepoStates();
  }
  @override
  Future<void> close() {
    _sub.cancel();
    repo.resetStream();
    return super.close();
  }

  void _observRepoStates() {
    _sub = repo.stream.listen((event) {
      switch (event.keys.first) {
        case CreateLivestreamReopsitoryState.createSucceed:
          _createdLivestream = event.values.first;
          emit(CreateLiveStreamSuccess(event.values.first));
          break;
        case CreateLivestreamReopsitoryState.createFailed:
          emit(CreateLiveStreamFailed(
              "Cant go live please check you internet connection"));
          break;
      }
    });
  }

  @override
  void onChange(Change<CreateLivestreamState> change) {
    print('flag2 + ${change.toString()}');
    super.onChange(change);
  }

  void connect({required String id}) async {
    repo.connect(userId: id);
  }

  Future<void> createLivestream(
      {required String title,
      required String description,
      required int uuid,
      required int subjectID}) async {
    emit(CreateLivestreamInProgress());
    final connectivityResult = await (Connectivity().checkConnectivity());
    print(connectivityResult.toString());
    if (connectivityResult == ConnectivityResult.none) {
      emit(CreateLiveStreamFailed("Please Check your internet connection!"));
      return;
    }
    repo.createLiveStream(title, description, uuid, subjectID);
  }
}

////////////////////////////////////////////////////////////states
abstract class CreateLivestreamState {}

class CreateLivestreamInit extends CreateLivestreamState {}

class CreateLivestreamNotifterState extends CreateLivestreamState {
  final String message;

  CreateLivestreamNotifterState(this.message);
}

class CreateLivestreamBuilderState extends CreateLivestreamState {}

class CreateLivestreamInProgress extends CreateLivestreamBuilderState {}

class CreateLiveStreamSuccess extends CreateLivestreamBuilderState {
  final Livestream createdLs;

  CreateLiveStreamSuccess(this.createdLs);
}

class CreateLiveStreamFailed extends CreateLivestreamNotifterState {
  CreateLiveStreamFailed(super.message);
}
