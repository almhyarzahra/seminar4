import 'dart:async';

import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../constants/agora.dart';
import '../../constants/enums.dart';
import '../../data/models/livestream_model.dart';
import '../../data/network_services/agora_token_handler.dart';
import '../../data/repositories/streaning_repository.dart';

class StreamingCubit extends Cubit<StreamingState> {
  late RtcEngine agoraEngine;
  late Livestream _livestream;
  late String _agoraToken;
  late bool isHost;
  bool _isJoined = false;
  late int userUUID;
  late StreamingRepository repo;
  bool isMuted = false;
  bool isCameraOff = false;
  bool isScreenShared = false;
  int? _remoteUID;

  late int videoHeight;
  StreamingCubit({required StreamingCubitArgumnets args})
      : super(StreamingProcessing()) {
    _livestream = args.livestream;
    isHost = args.isHost;
    userUUID = args.userUUID;
    repo = args.repo;
    videoHeight = isHost ? 540 : 200;
    /*  String numberAsString = userUUID.toString();
    String firsNineDigits = numberAsString.substring(0, 8);
    userUUID = int.parse(firsNineDigits); */
    listenToRepo();
    joinLivestream();
  }
  Livestream get livestream {
    _livestream = repo.ls;
    return _livestream;
  }

  int? get remoteUid => _remoteUID;

  void listenToRepo() {
    repo.strm.listen((event) async {
      switch (event) {
        case StreamingRepositoryState.livestreamEnded:
          emit(StreamingEnded("Livestream has ended", true));
          break;
        case StreamingRepositoryState.livestreamUpdated:
          emit(StreamingUpdated(livestream: livestream));
          break;
        case StreamingRepositoryState.joinsucceed:
          await setupVideoSDKEngine();
          joinEngine();
          //emit(StreamingStarted(remoteUid: _remoteUID, livestream: livestream));

          break;
        case StreamingRepositoryState.joinFailed:
          emit(StreamingEnded("Cant join livestream ", false));
          break;
      }
    });
  }

  void joinLivestream() async {
    emit(StreamingProcessing());
    String? temptoken = await getAgoraToken();
    if (temptoken == null) {
      emit(StreamingEnded("Failed to join please check you internet", false));
      return;
    }
    _agoraToken = temptoken;
    print('flag22' + _agoraToken);
    if (isHost) {
      await setupVideoSDKEngine();
      joinEngine();
      emit(StreamingStarted(remoteUid: null, livestream: livestream));

      return;
    }
    repo.joinLivestream(livestreamId: _livestream.id);
  }

  Future<String?> getAgoraToken() async {
    return await AgoraTokenHandler.getToken(
        channelId: _livestream.id, uid: userUUID.toString(), isHost: isHost);
  }

  void leaveLivestream() {
    repo.leaveLivstream(livestreamId: _livestream.id);
    print("falg2000 from cubit");

    emit(StreamingEnded("left", false));
  }

  @override
  void onChange(Change<StreamingState> change) {
    print("elie" + change.toString());
    super.onChange(change);
  }

  @override
  Future<void> close() async {
    repo.dispose();
    await agoraEngine.leaveChannel();
    agoraEngine.release();
    return super.close();
  }

  Future<void> setupVideoSDKEngine() async {
    // retrieve or request camera and microphone permissions
    if (isHost) {
      await [Permission.microphone, Permission.camera].request();
    }
    //TODO: handle premisson rejection

    //create an instance of the Agora engine
    agoraEngine = createAgoraRtcEngine();
    await agoraEngine.initialize(
      const RtcEngineContext(
        appId: agoraAPPID,
      ),
    );

    await agoraEngine.setVideoEncoderConfiguration(
      const VideoEncoderConfiguration(
        orientationMode: OrientationMode.orientationModeAdaptive,
        frameRate: 2,
        mirrorMode: VideoMirrorModeType.videoMirrorModeDisabled,
        advanceOptions: AdvanceOptions(
            encodingPreference: EncodingPreference.preferHardware),
        /*   dimensions: VideoDimensions(
          height: 200,
          width: 320,
        ), */
      ),
    );
    await agoraEngine.enableVideo();
    // Register the event handler
    agoraEngine.registerEventHandler(
      RtcEngineEventHandler(
        onTokenPrivilegeWillExpire: (connection, token) async {
          String? newToken = await getAgoraToken();
          if (newToken != null) {
            agoraEngine.renewToken(newToken);
          } else {
            emit(StreamingEnded("Check your internet connection", isHost));
          }
        },
        onJoinChannelSuccess: (RtcConnection connection, int elapsed) {
          //showMessage("Local user uid:${connection.localUid} joined the channel");
          if (!isHost) {
            emit(StreamingProcessing());
          }
          _isJoined =
              true; /* else {
            print("elie from onJoinChannelSuccess");
            emit(StreamingStarted(
                remoteUid: _remoteUID, livestream: livestream));
          } */
        },
        onUserJoined: (RtcConnection connection, int remoteUid, int elapsed) {
          // showMessage("Remote user uid:$remoteUid joined the channel");
          if (!isHost) {
            _remoteUID = remoteUid;
            emit(
                StreamingStarted(remoteUid: remoteUid, livestream: livestream));
          }
        },
        onVideoSizeChanged:
            (connection, sourceType, uid, width, height, rotation) {
          print(
              "mhyar rotation is $rotation width is $width height is $height");
          if (!isHost) {
            if ((rotation == 0 && width > height) || rotation == 180) {
              videoHeight = 250;
            } else {
              videoHeight = 540;
            }

            emit(VideoDimensionsChanged());
          }
        },
      ),
    );
  }

  Future<void> joinEngine() async {
    // Set channel options
    ChannelMediaOptions options;

    // Set channel profile and client role
    if (isHost) {
      options = const ChannelMediaOptions(
        clientRoleType: ClientRoleType.clientRoleBroadcaster,
        channelProfile: ChannelProfileType.channelProfileLiveBroadcasting,
      );
      await agoraEngine.startPreview();
    } else {
      options = const ChannelMediaOptions(
        clientRoleType: ClientRoleType.clientRoleAudience,
        channelProfile: ChannelProfileType.channelProfileLiveBroadcasting,
      );
    }

    await agoraEngine.joinChannel(
      token: _agoraToken,
      channelId: _livestream.id,
      options: options,
      uid: userUUID,
    );
  }

  void toggleMute() async {
    if (isHost && _isJoined) {
      isMuted = !isMuted;
      await agoraEngine.muteLocalAudioStream(isMuted);
    }
  }

  void switchCamera() async {
    if (isHost && _isJoined) {
      await agoraEngine.switchCamera();
    }
  }

  void toggleVideoSharring() async {
    if (!isHost || !_isJoined) {
      return;
    }
    isCameraOff = !isCameraOff;

    await agoraEngine.muteLocalVideoStream(isCameraOff);
    if (isCameraOff) {
      await agoraEngine.stopPreview();
      await agoraEngine.disableVideo();
      emit(StopedPreview());
    } else {
      await agoraEngine.startPreview();
      await agoraEngine.enableVideo();
      emit(StreamingStarted(livestream: livestream, remoteUid: remoteUid));
    }
  }

  void changeScreenOreintation(bool isPortrait) async {
    if (!isPortrait) {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
    } else {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
    }
    emit(StreamingStarted(livestream: livestream, remoteUid: remoteUid));
  }

  Future<void> shareScreen() async {
    if (!isHost || !_isJoined) {
      return;
    }
    isScreenShared = !isScreenShared;

    // Update channel media options to publish camera or screen capture streams
    if (isScreenShared) {
      try {
        await agoraEngine.startScreenCapture(const ScreenCaptureParameters2(
            captureAudio: true,
            audioParams: ScreenAudioParameters(
                sampleRate: 16000, channels: 2, captureSignalVolume: 100),
            captureVideo: true,
            videoParams: ScreenVideoParameters(
                dimensions: VideoDimensions(height: 720, width: 1280),
                frameRate: 15,
                bitrate: 600)));
      } catch (e) {
        print(e.toString());
        isScreenShared = !isScreenShared;
        return;
      }
    } else {
      await agoraEngine.stopScreenCapture();
    }
    ChannelMediaOptions options = ChannelMediaOptions(
      publishCameraTrack: !isScreenShared,
      publishMicrophoneTrack: true,
      publishScreenTrack: isScreenShared,
      publishScreenCaptureAudio: isScreenShared,
      publishScreenCaptureVideo: isScreenShared,
    );

    await agoraEngine.updateChannelMediaOptions(options);
    if (isScreenShared) {
      await agoraEngine.startPreview(
          sourceType: VideoSourceType.videoSourceScreen);
    }
    emit(StreamingStarted(livestream: livestream, remoteUid: remoteUid));

    return;
  }
}

abstract class StreamingState {}

class StreamingProcessing extends StreamingState {}

//1
class StreamingStarted extends StreamingState {
  final Livestream livestream;
  final int? remoteUid;

  StreamingStarted({required this.livestream, required this.remoteUid});
}

class StreamingUpdated extends StreamingState {
  final Livestream livestream;

  StreamingUpdated({required this.livestream});
}

class StopedPreview extends StreamingState {}

class VideoDimensionsChanged extends StreamingState {
  VideoDimensionsChanged();
}

//2
class StreamingEnded extends StreamingState {
  final String errorMessage;
  final bool endedByBroadcaster;

  StreamingEnded(this.errorMessage, this.endedByBroadcaster);
}

//3

class StreamingCubitArgumnets {
  final Livestream livestream;
  final bool isHost;
  final int userUUID;
  final StreamingRepository repo;

  StreamingCubitArgumnets(
      {required this.livestream,
      required this.isHost,
      required this.userUUID,
      required this.repo});
}
