import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';

import '../../constants/enums.dart';
import '../../data/models/message_model.dart';
import '../../data/repositories/livestream_chat_repository.dart';

class StreamingChatCubit extends Cubit<StreamingChatState> {
  final String livestreamId;

  final LivestreamChatRepository repo;
  StreamingChatCubit({required this.livestreamId, required this.repo})
      : super(MessagesProcessing()) {
    listenToRepoStates();
    initialMessagesFetch();
  }

  void listenToRepoStates() {
    repo.strm.listen((event) {
      switch (event) {
        case LivestreamChatRepositoryState.messagesUpdated:
          emit(MessagesUpdated(messages: repo.messages));
          break;
        case LivestreamChatRepositoryState.sentSuccess:
          emit(StreamingChatSendSucceed());
          break;
        case LivestreamChatRepositoryState.sentFailed:
          emit(StreamingChatSendFailed());
          break;
      }
    });
  }

  void sendMessage({required String message}) {
    emit(StreamingChatSending());
    repo.sendMessage(livestreamId: livestreamId, messageBody: message);
  }

  @override
  void onChange(Change<StreamingChatState> change) {
    print("fla11 $change");
    super.onChange(change);
  }

  @override
  Future<void> close() {
    repo.dispose();
    return super.close();
  }

  void initialMessagesFetch() async {
    Timer(const Duration(seconds: 4), () async {
      repo.initFetch(lsID: livestreamId);
    });
  }
}

abstract class StreamingChatState {}

class StreamingChatMessagesState extends StreamingChatState {}

class MessagesProcessing extends StreamingChatMessagesState {}

class MessagesUpdated extends StreamingChatMessagesState {
  final List<Message> messages;

  MessagesUpdated({required this.messages});
}

class StreamingChatSendState extends StreamingChatState {}

class StreamingChatSendSucceed extends StreamingChatSendState {}

class StreamingChatSendFailed extends StreamingChatSendState {}

class StreamingChatSending extends StreamingChatSendState {}
