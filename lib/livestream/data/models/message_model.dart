class Message {
  final String id;
  final String senderId;
  final String messageBody;
  final String liveStream;
  final String senderName;
  final DateTime sentAt;

  Message(
      {required this.senderName,
      required this.id,
      required this.senderId,
      required this.messageBody,
      required this.liveStream,
      required this.sentAt});
  factory Message.fromJson(Map<String, dynamic> json) {
    return Message(
      id: json['_id'],
      senderId: json['senderId'],
      messageBody: json['messageBody'],
      liveStream: json['liveStream'],
      senderName: json['senderName'],
      sentAt: DateTime.parse(json['sentAt']),
    );
  }

  // Define a method that converts the MessageModel instance to a JSON map
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'senderId': senderId,
      'messageBody': messageBody,
      'liveStream': liveStream,
      'sentAt': sentAt.toIso8601String(),
    };
  }

  @override
  String toString() {
    return 'Message(id: $id, senderId: $senderId, messageBody: $messageBody, liveStream: $liveStream, senderName: $senderName, sentAt: $sentAt)';
  }
}
