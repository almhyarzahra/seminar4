class Livestream {
  final String id;
  final String title;
  final String description;
  final String imageUrl;
  final String broadcaster;
  final String subjectID;
  final DateTime startedAt;
  int viewCount;
  final List<String> usersWatching;

  Livestream(
      {required this.subjectID,
      required this.id,
      required this.title,
      required this.description,
      required this.broadcaster,
      required this.imageUrl,
      required this.startedAt,
      required this.usersWatching,
      required this.viewCount});

  factory Livestream.fromJson(Map<String, dynamic> json) {
    final List<dynamic> userListJson = json['viewers']['users'];
    final List<String> userList =
        userListJson.map((userJson) => userJson['user'].toString()).toList();
    return Livestream(
      id: json['id'],
      title: json['title'],
      description: json['description'],
      imageUrl: json['imageUrl'],
      broadcaster: json['broadcaster'],
      startedAt: DateTime.parse(json['startedAt']),
      usersWatching: userList,
      viewCount: json['viewers']['count'],
      subjectID: json['subjectID'],
    );
  }

  void incrementViewCount() {
    viewCount++;
  }

  void decrementViewCount() {
    viewCount--;
  }

  void get() => id;
  @override
  String toString() {
    return 'Livestream {'
        ' id: $id,'
        ' title: $title,'
        ' description: $description,'
        ' imageUrl: $imageUrl,'
        ' broadcaster: $broadcaster,'
        ' startedAt: $startedAt,'
        ' viewCount: $viewCount,'
        ' usersWatching: $usersWatching,'
        'subjectID :$subjectID'
        '}';
  }
}
