import 'dart:async';

import '../../constants/enums.dart';
import '../../logic/patterns/observer.dart';
import '../models/livestream_model.dart';
import '../network_services/livestream_ns.dart';

class CreateLiveStreamRepository implements LivestreamNSObserver {
  late StreamController<Map<CreateLivestreamReopsitoryState, dynamic>>?
      _controller;
  final LivestreamNS _livestreamNS;

  CreateLiveStreamRepository(this._livestreamNS) {
    _livestreamNS.attach(this);
    _controller =
        StreamController<Map<CreateLivestreamReopsitoryState, dynamic>>();
  }
  Stream<Map<CreateLivestreamReopsitoryState, dynamic>> get stream {
    if (_controller == null) {
      _controller =
          StreamController<Map<CreateLivestreamReopsitoryState, dynamic>>();
      return _controller!.stream;
    }
    return _controller!.stream;
  }

  @override
  void update(Map<LivestreamSocketState, dynamic> data) {
    if (_controller != null) {
      if (data.keys.first ==
          LivestreamSocketState.createdLivesteamSuccessfully) {
        _nofifyCubit({
          CreateLivestreamReopsitoryState.createSucceed:
              Livestream.fromJson(data.values.first)
        });
      } else if (data.keys.first ==
          LivestreamSocketState.createLiveStreamFailed) {
        _nofifyCubit({CreateLivestreamReopsitoryState.createFailed: null});
      }
    }
  }

  void _nofifyCubit(Map<CreateLivestreamReopsitoryState, dynamic> data) {
    _controller?.add(data);
  }

  void createLiveStream(
      String title, String description, int userId, int SubjectID) {
    _livestreamNS.createLivestream(title, description, userId, SubjectID);
  }

  void connect({required String userId}) {
    _livestreamNS.connect(id: userId);
  }

  void resetStream() {
    _controller?.close();
    _controller = null;
  }
}
