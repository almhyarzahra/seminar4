import 'dart:async';
import 'dart:convert';

import '../../constants/enums.dart';
import '../../constants/networkconfig.dart';
import '../../logic/patterns/observer.dart';

import 'package:http/http.dart' as http;

import '../models/message_model.dart';
import '../network_services/livestream_ns.dart';

class LivestreamChatRepository extends LivestreamNSObserver {
  final List<Message> _messages = <Message>[];
  late StreamController<LivestreamChatRepositoryState> _controller;
  final LivestreamNS ns;

  LivestreamChatRepository({required this.ns}) {
    _controller = StreamController<LivestreamChatRepositoryState>();
    ns.attach(this);
  }
  Stream<LivestreamChatRepositoryState> get strm => _controller.stream;
  List<Message> get messages => _messages;
  @override
  void update(Map<LivestreamSocketState, dynamic> data) {
    switch (data.keys.first) {
      case LivestreamSocketState.sentMessageSuccess:
        notifyCubit(LivestreamChatRepositoryState.sentSuccess);
        break;
      case LivestreamSocketState.sentMessageFailed:
        notifyCubit(LivestreamChatRepositoryState.sentFailed);
        break;
      case LivestreamSocketState.recivedMessage:
        print("flag-alpha tony" + data.values.first.toString());
        Message message = Message.fromJson(data.values.first);
        print("flag-alpha  message is " + message.toString());

        messages.add(message);
        notifyCubit(LivestreamChatRepositoryState.messagesUpdated);
        break;
    }
  }

  void sendMessage(
      {required String livestreamId, required String messageBody}) {
    ns.sendMessgae(messageBody: messageBody, livestreamId: livestreamId);
  }

  void notifyCubit(LivestreamChatRepositoryState state) {
    _controller.add(state);
  }

  Future<void> initFetch({required String lsID}) async {
    // print("hi");
    http.Response response;
    final Map<String, String> headers = {
      'Content-Type': 'application/json',
    };
    try {
      response = await http.get(Uri.parse('$endPoint/ls/messages/$lsID'),
          headers: headers);

      //print("status is " + response.statusCode.toString());
      if (response.statusCode != 200) {
        notifyCubit(LivestreamChatRepositoryState.messagesUpdated);
        return;
      }
      List<dynamic> decodedBody = jsonDecode(response.body)['messages'];
      List<Message> fetchedMessages =
          decodedBody.map((e) => Message.fromJson(e['message'])).toList();
      _messages.clear();
      _messages.insertAll(0, fetchedMessages);

      notifyCubit(LivestreamChatRepositoryState.messagesUpdated);
    } catch (e) {
      notifyCubit(LivestreamChatRepositoryState.messagesUpdated);

      print(e);
    }
  }

  void dispose() {
    _controller.close();
    ns.detach(this);
  }
}
