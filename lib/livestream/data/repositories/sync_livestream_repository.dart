// This class is responsible for managing livestreams as a repository
// It listens to the events emitted by LivestreamNS and notifies the cubit
// The class implements Observer interface to be able to listen to the events
import 'dart:async';

import 'package:flutter/material.dart';

import '../../constants/enums.dart';
import '../../logic/patterns/observer.dart';
import '../models/livestream_model.dart';
import '../network_services/livestream_ns.dart';

class LiveStreamRepository implements LivestreamNSObserver {
  bool connected = false;
  final LivestreamNS netwrokService; // A reference to LivestreamNS
  late StreamController<Map<SyncLivestreamRepositoryState, dynamic>>?
      _streamControllerToCubit; // A stream controller to emit events to the cubit
  final List<Livestream> _onAirLiveStreams =
      <Livestream>[]; // A list of livestreams

  // Constructor
  LiveStreamRepository(this.netwrokService) {
    _streamControllerToCubit =
        StreamController<Map<SyncLivestreamRepositoryState, dynamic>>();
    netwrokService.attach(this); // Attach itself to the network service
  }
  // A getter to get the stream
  Stream<Map<SyncLivestreamRepositoryState, dynamic>> get stream {
    _streamControllerToCubit ??=
        StreamController<Map<SyncLivestreamRepositoryState, dynamic>>();
    return _streamControllerToCubit!.stream;
  }

  // An overriden method from Observer interface to listen to events from LivestreamNS
  @override
  void update(Map<LivestreamSocketState, dynamic> data) {
    switch (data.keys.first) {
      // When connected, initialize the list of livestreams and notify the cubit
      case LivestreamSocketState.connected:
        _initLivestreamsList(data.values.first);
        notifyCubit(
            {SyncLivestreamRepositoryState.listUpdated: _onAirLiveStreams});
        break;
      // When disconnected, clear the list of livestreams and notify the cubit
      case LivestreamSocketState.disconnected:
        _onAirLiveStreams.clear();
        notifyCubit({SyncLivestreamRepositoryState.notConnected: null});
        break;
      // When a new livestream is added, add it to the list and notify the cubit
      case LivestreamSocketState.addToLiveStreamList:
        _onAirLiveStreams.add(Livestream.fromJson(data.values.first));
        printls();
        notifyCubit(
            {SyncLivestreamRepositoryState.listUpdated: _onAirLiveStreams});
        break;
      // When a livestream is deleted, remove it from the list and notify the cubit
      case LivestreamSocketState.deleteFromLivestreamList:
        _deleteLiveStream(data.values.first as String);
        notifyCubit(
            {SyncLivestreamRepositoryState.listUpdated: _onAirLiveStreams});
        break;
      case LivestreamSocketState.createdLivesteamSuccessfully:
        // nothing to do
        break;
      case LivestreamSocketState.createLiveStreamFailed:
        // nothing to do
        break;
      case LivestreamSocketState.updateLivestreamList:
        int i = _onAirLiveStreams
            .indexWhere((element) => element.id == data.values.first["id"]);
        _onAirLiveStreams[i] = Livestream.fromJson(data.values.first);
        notifyCubit(
            {SyncLivestreamRepositoryState.listUpdated: _onAirLiveStreams});
        break;
      case LivestreamSocketState.joinSuccess:
        // TODO: Handle this case.
        break;
      case LivestreamSocketState.joinFail:
        // TODO: Handle this case.
        break;
    }
  }

  // A method to notify the cubit with the provided data
  void notifyCubit(Map<SyncLivestreamRepositoryState, dynamic> data) {
    if (_streamControllerToCubit != null) {
      _streamControllerToCubit!.add(data);
    }
  }

  void resetStream() {
    _streamControllerToCubit?.close();
    _streamControllerToCubit = null;
  }

  // A method to connect to the network service with the provided token
  void connect({required String id}) {
    if (!connected) {
      connected = true;
      netwrokService.connect(id: id);
      return;
    }
    notifyCubit({SyncLivestreamRepositoryState.listUpdated: _onAirLiveStreams});
  }

  // A method to initialize the list of livestreams
  void _initLivestreamsList(List<dynamic> fetched) {
    _onAirLiveStreams.clear();
    print(fetched);
    List<Livestream> fetchedLs =
        fetched.map((lSJson) => Livestream.fromJson(lSJson)).toList();
    _onAirLiveStreams.addAll(fetchedLs);
    //printls();
  }

  // A method to delete a livestream with the provided id
  void _deleteLiveStream(String id) {
    _onAirLiveStreams.removeWhere(
      (e) {
        return e.id == id;
      },
    );
  }

  void printls() {
    for (Livestream ls in _onAirLiveStreams) {
      print(ls.toString());
    }
  }
}
