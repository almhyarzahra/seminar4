import 'dart:async';

import '../../constants/enums.dart';
import '../../logic/patterns/observer.dart';
import '../models/livestream_model.dart';
import '../network_services/livestream_ns.dart';

class StreamingRepository extends LivestreamNSObserver {
  final LivestreamNS ns;
  Livestream _livestream;
  late StreamController<StreamingRepositoryState> _controller;
  StreamingRepository({required this.ns, required Livestream livestream})
      : _livestream = livestream {
    ns.attach(this);
    _controller = StreamController<StreamingRepositoryState>();
  }
  Stream<StreamingRepositoryState> get strm => _controller.stream;
  Livestream get ls => _livestream;
  @override
  void update(Map<LivestreamSocketState, dynamic> data) {
    switch (data.keys.first) {
      case LivestreamSocketState.disconnected:
        // TODO: Handle this case.
        break;

      case LivestreamSocketState.deleteFromLivestreamList:
        if (data.values.first == _livestream.id) {
          notifyCubit(StreamingRepositoryState.livestreamEnded);
        }
        break;
      case LivestreamSocketState.createdLivesteamSuccessfully:
        // TODO: Handle this case.
        break;
      case LivestreamSocketState.createLiveStreamFailed:
        // TODO: Handle this case.
        break;
      case LivestreamSocketState.updateLivestreamList:
        if (data.values.first['id'] == _livestream.id) {
          _livestream = Livestream.fromJson(data.values.first);
          notifyCubit(StreamingRepositoryState.livestreamUpdated);
        }
        break;
      case LivestreamSocketState.joinSuccess:
        notifyCubit(StreamingRepositoryState.joinsucceed);
        break;
      case LivestreamSocketState.joinFail:
        notifyCubit(StreamingRepositoryState.joinFailed);
        break;
    }
  }

  void notifyCubit(StreamingRepositoryState data) {
    try {
      _controller.add(data);
    } catch (e) {
      print("did closed");
    }
  }

  void joinLivestream({required String livestreamId}) {
    ns.joinLivestream(livestreamId: livestreamId);
  }

  void leaveLivstream({required String livestreamId}) {
    print("falg2000 from repo");
    ns.leaveLivestream(livestreamId: livestreamId);
  }

  void dispose() {
    _controller.close();
    ns.detach(this);
  }
}
