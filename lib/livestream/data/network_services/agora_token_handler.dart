import 'dart:convert';

import 'package:http/http.dart' as http;

class AgoraTokenHandler {
  static const String _endPoint = "https://seminar-token-generator.vercel.app";
  static Future<String?> getToken(
      {required String channelId,
      required String uid,
      required bool isHost}) async {
    final String role = isHost ? "publisher" : "audience";
    String token = "";

    http.Response response;
    try {
      response =
          await http.get(Uri.parse("$_endPoint/rtc/$channelId/$role/uid/$uid"));
      print('flag101 $uid');
      if (response.statusCode == 200) {
        token = jsonDecode(response.body)['rtcToken'];
        print("flag101 from generator" + token);
      }
    } catch (e) {
      print('error http ${e.toString()}');
      return null;
    }
    if (token.isEmpty) {
      return null;
    }
    return token;
  }
}
