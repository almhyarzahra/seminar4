import 'package:socket_io_client/socket_io_client.dart' as IO;

import '../../constants/enums.dart';
import '../../constants/networkconfig.dart';
import '../../logic/patterns/observer.dart';

class LivestreamNS implements Observerable {
  final List<LivestreamNSObserver> _obs = <LivestreamNSObserver>[];
  bool connected = false;
  late IO.Socket socket;
  void connect({required String id}) {
    if (!connected) {
      socket = IO.io('$endPoint/live', <String, dynamic>{
        'transports': ['websocket'],
        'auth': {'socketId': id},
      });

      socket.onConnect((_) {
        connected = true;
      });
      socket.on('init', (lss) {
        notifyObservers({LivestreamSocketState.connected: lss});
      });
      socket.on('fetched', (lss) {
        notifyObservers({LivestreamSocketState.connected: lss});
      });

// Listen for create events
      socket.on('create', (livestream) {
        notifyObservers(
            {LivestreamSocketState.addToLiveStreamList: livestream});
      });
      socket.on(
        'update-livestreams',
        (data) {
          notifyObservers({LivestreamSocketState.updateLivestreamList: data});
        },
      );

      socket.on(
        'recive-message',
        (data) {
          notifyObservers({LivestreamSocketState.recivedMessage: data});
        },
      );
// Listen for end event
      socket.on('end', (livestreamId) {
        notifyObservers(
            {LivestreamSocketState.deleteFromLivestreamList: livestreamId});
      });

// Listen for connect_error event

      //Listen to createSucceed event
      socket.on('createSucceed', (data) {
        print("from ns created successfully");
        notifyObservers(
            {LivestreamSocketState.createdLivesteamSuccessfully: data});
      });
      socket.on('join-success',
          (_) => notifyObservers({LivestreamSocketState.joinSuccess: null}));
      socket.on('join-error',
          (_) => notifyObservers({LivestreamSocketState.joinFail: null}));
      socket.on('connect_error', (err) {
        print(err.toString() + "connect error");
        notifyObservers({LivestreamSocketState.disconnected: err.toString()});
      });

      // Listen for createError event
      socket.on('createError', (errorMessage) {
        print('Error creating live stream: $errorMessage');
        notifyObservers({LivestreamSocketState.createLiveStreamFailed: null});
      });
      socket.on(
        'connect_timeout',
        (_) {
          notifyObservers(
              {LivestreamSocketState.disconnected: 'connection timed out'});
        },
      );
      // Listen for disconnect event
      socket.onDisconnect((_) {
        notifyObservers({LivestreamSocketState.disconnected: 'Disconnected'});
        print('Disconnected from /live');
      });
      //socket.onConnectTimeout((_) => connected = false);
    } else {
      socket.emit('fetch');
    }
  }

  void createLivestream(
      String title, String description, int broadcaster, int subjectId) {
    if (socket.connected) {
      socket.emit('create', {
        'title': title,
        'description': description,
        'broadcaster': broadcaster,
        "subjectID": subjectId
      });
    } else {
      notifyObservers({LivestreamSocketState.createLiveStreamFailed: null});
    }
  }

  void joinLivestream({required String livestreamId}) {
    socket.emit('join-livestream', livestreamId);
  }

  void leaveLivestream({required String livestreamId}) {
    socket.emit('leave-livestraem', livestreamId);
  }

  void sendMessgae(
      {required String messageBody, required String livestreamId}) {
    socket.emitWithAck('send-message', [messageBody, livestreamId],
        ack: (Map<String, dynamic> response) {
      print('flag1000' + response.values.first.toString());
      if (response['sent'] != null && response["sent"] == true) {
        notifyObservers({LivestreamSocketState.sentMessageSuccess: null});
      } else {
        notifyObservers({LivestreamSocketState.sentMessageFailed: null});
      }
    });
  }

  void disconnect() {
    socket.disconnect();
  }

  @override
  void attach(LivestreamNSObserver observer) {
    _obs.add(observer);
  }

  @override
  void detach(LivestreamNSObserver observer) {
    _obs.removeWhere((element) => element == observer);
  }

  @override
  void notifyObservers(Map<LivestreamSocketState, dynamic> data) {
    for (LivestreamNSObserver ob in _obs) {
      ob.update(data);
    }
  }
}
