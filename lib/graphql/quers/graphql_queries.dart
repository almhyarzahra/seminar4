class GraphQlQueries {
  static String checkDoctorValidation(
      {required int USER_ID,
      required String DAY,
      required String BEGIN,
      required String END}) {
    return """
  {  
  check_user_validation(   
    subject_time_schedule:{
    AND:[
          {column:USER_ID,operator:EQ,value:$USER_ID}
          {column:DAY,operator:EQ,value:$DAY}
          {column:BEGIN,operator:EQ,value:$BEGIN}
          {column:END,operator:EQ,value:$END}
  ]
  }
  )
   
   
}
  """;
  }

  static String checkCategoryValidation({
    required String YEAR,
    required String SPECIALTY,
    required String DEPARTMENT,
    required int CATEGORY,
    required String DAY,
    required String BEGIN,
    required String END,
  }) {
    return """
       {check_validation(
    where:{
      AND:[
        {column:YEAR,operator:EQ,value:$YEAR}
{column:SPECIALTY,operator:EQ,value:$SPECIALTY}
{column:DEPARTMENT,operator:EQ,value:$DEPARTMENT}

         {column:CATEGORY,operator:EQ,value:"$CATEGORY"}
    ]}
    
subject_time_schedule:{    AND:[
          
          {column:DAY,operator:EQ,value:$DAY}
          {column:BEGIN,operator:EQ,value:$BEGIN}
          {column:END,operator:EQ,value:$END}
  ]}
    
    )
    
  }
    
    """;
  }

  static String createSubject({
    required String laboratory,
    required int year,
    required int duration,
    required int category,
    required String specialty,
    required String department,
    required int dr,
    required String name,
    required String day,
    required String begin,
    required String end,
    required var ids,
  }) {
    return """
 mutation{
create_subject(subject:{year:$year,duration:$duration,category:$category,specialty:"$specialty"
  ,department:"$department",dr:$dr
 ,laboratory:"$laboratory", 
  name:"$name",subject_time_schedule:{create:
[
  
  {day:"$day",begin:"$begin",end:"$end"}

]
}
doctor:{connect:$ids}
}

){category}
}
    
  
    
    """;
  }
}
