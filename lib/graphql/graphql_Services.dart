import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import '../utils/logger.dart';
import 'graphql_config.dart';

class GraphQlServices {
  late GraphQLClient client;
  late String _query;

  GraphQlServices({required String query}) {
    _query = query;
    final graphqlConfig = GraphQlConfig(
        httpLink1:
            HttpLink("https://0866-37-19-221-240.ngrok-free.app/graphql"));
    client = graphqlConfig.graphQLClient();
  }

  Future<Map<String, dynamic>> get() async {
    QueryResult result = await client.query(QueryOptions(
      document: gql(_query),
    ));
    return result.data!;
  }

  Future<Map<String, dynamic>> post() async {
    var SubjectInput = "math2222";
    iLog("""${SubjectInput}""");
    QueryResult result =
        await client.mutate(MutationOptions(document: gql(_query)));

    return result.data!;
  }
}
