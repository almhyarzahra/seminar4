import 'dart:convert';
import 'package:get/get.dart';
import 'package:school_erp/enums/data_type.dart';
import 'package:school_erp/models/apis/token_info.dart';

import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceRepository {
  SharedPreferences globalSharedPrefs = Get.find();

  String PREF_TOKEN_INFO = 'token_info';
  String ONE_CLICK = 'one_click';
  String DATE_DAY = 'date,day';

  setDay(int value) {
    setPreferance(
      dataType: DataType.INT,
      key: DATE_DAY,
      value: value,
    );
  }

  int getDay() {
    if (globalSharedPrefs.containsKey(DATE_DAY)) {
      return getPrefrance(key: DATE_DAY);
    } else {
      return 0;
    }
  }

  setStateClick(bool value) {
    setPreferance(
      dataType: DataType.BOOL,
      key: ONE_CLICK,
      value: value,
    );
  }

  bool getStateClick() {
    if (globalSharedPrefs.containsKey(ONE_CLICK)) {
      return getPrefrance(key: ONE_CLICK);
    } else {
      return true;
    }
  }

  setTokenInfo(TokenInfo value) {
    setPreferance(
      dataType: DataType.STRING,
      key: PREF_TOKEN_INFO,
      value: jsonEncode(value),
    );
  }

  setTokenInfoString(String value) {
    setPreferance(
      dataType: DataType.STRING,
      key: PREF_TOKEN_INFO,
      value: value,
    );
  }

  String? getTokenInfoString() {
    if (globalSharedPrefs.containsKey(PREF_TOKEN_INFO)) {
      return getPrefrance(key: PREF_TOKEN_INFO);
    } else {
      return null;
    }
  }

  TokenInfo? getTokenInfo() {
    if (globalSharedPrefs.containsKey(PREF_TOKEN_INFO)) {
      return TokenInfo.fromJson(jsonDecode(getPrefrance(key: PREF_TOKEN_INFO)));
    } else {
      return null;
    }
  }

  setPreferance({
    required DataType dataType,
    required String key,
    required dynamic value,
  }) async {
    switch (dataType) {
      case DataType.INT:
        await globalSharedPrefs.setInt(key, value);
        break;
      case DataType.BOOL:
        await globalSharedPrefs.setBool(key, value);
        break;
      case DataType.STRING:
        await globalSharedPrefs.setString(key, value);
        break;
      case DataType.DOUBLE:
        await globalSharedPrefs.setDouble(key, value);
        break;
      case DataType.LISTSTRING:
        await globalSharedPrefs.setStringList(key, value);
        break;
    }
  }

  dynamic getPrefrance({required String key}) {
    return globalSharedPrefs.get(key);
  }
}
