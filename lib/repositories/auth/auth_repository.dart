import 'dart:developer';
import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:dio/dio.dart';
import 'package:school_erp/enums/message_type.dart';
import 'package:school_erp/models/app_response.dart';
import 'package:school_erp/models/requests/login_request.dart';
import 'package:school_erp/models/requests/register_request.dart';
import 'package:school_erp/models/user_model.dart';
import 'package:school_erp/repositories/auth/base_auth_repository.dart';
import 'package:school_erp/repositories/core/endpoints.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_toast.dart';
import 'package:school_erp/utils/dio_client/dio_client.dart';
import 'package:school_erp/utils/logger.dart';

import '../../enums/request_status.dart';
import '../../services/base_controller.dart';

class AuthRepository extends BaseAuthRepository {
  AuthRepository({Dio? dioClient})
      : _dioClient = dioClient ?? DioClient().instance;
  final Dio _dioClient;

  @override
  Future<AppResponse<AuthUser?>> login(LoginRequest request) async {
    try {
      final respone = await _dioClient.post("api/sanctum/token",
          queryParameters: request.toJson());
      wLog(respone.data);
      return AppResponse<AuthUser?>.fromJson(
          respone.data,
          (dynamic json) => respone.data['success'] && json != null
              ? AuthUser.fromJson(json)
              : null);
    } on DioError catch (e) {
      BotToast.closeAllLoading();
      return AppResponse(message: "No Internet Connection", success: false);
    }
  }

  @override
  Future<AppResponse<UserModel?>> loginWithToken() async {
    // TODO: implement loginWithToken

    final respone = await _dioClient.post(Endpoints.loginWithToken);
    return AppResponse<UserModel?>.fromJson(
        respone.data,
        (dynamic json) => respone.data['success'] && json != null
            ? UserModel.fromJson(json)
            : null);
  }

  @override
  Future<AppResponse> logout({required String token}) async {
    // TODO: implement logout
    try {
      _dioClient.options.headers[HttpHeaders.authorizationHeader] =
          'Bearer $token';
      final respone = await _dioClient.get("/api/user/revoke");

      return AppResponse.fromJson({
        "statusMessage": respone.data.toString(),
        "statusCode": 200,
        "success": true,
        "message": "message"
      }, (dynamic json) => null);
    } on DioError catch (e) {
      return AppResponse(message: "No Internet Connection", success: false);
    }
  }
//
// @override
// Future<AppResponse<AuthUser?>> register(RegisterRequest request) async {
//
// }
}
