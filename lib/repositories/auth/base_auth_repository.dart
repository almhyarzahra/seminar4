import 'package:school_erp/models/models.dart';
import 'package:school_erp/models/requests/login_request.dart';

import '../../models/app_response.dart';
import '../../models/requests/register_request.dart';

abstract class BaseAuthRepository
{

  // Future<AppResponse<AuthUser?>> register(RegisterRequest request);
  // Future<AppResponse<AuthUser?>>
  login(LoginRequest request);
  Future<AppResponse<UserModel?>> loginWithToken();
Future<AppResponse> logout({required String token});


}