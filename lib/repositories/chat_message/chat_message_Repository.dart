import 'package:dio/dio.dart';
import 'package:school_erp/models/app_response.dart';
import 'package:school_erp/models/chat_message_model.dart';
import 'package:school_erp/models/requests/create_chat_message_request.dart';
import 'package:school_erp/repositories/chat_message/base_chat_message_repository.dart';
import 'package:school_erp/repositories/core/endpoints.dart';
import 'package:school_erp/utils/logger.dart';

import '../../utils/dio_client/dio_client.dart';

class ChatMessageRepository extends BaseChatMessageRepository {
  final Dio _dioClient;

  ChatMessageRepository({Dio? dioClient})
      : _dioClient = dioClient ?? DioClient().instance;

  @override
  Future<AppResponse<ChatMessageEntity?>>   createChatMessage(
      String socketId, CreateChaMessageRequest request) async {

    final response = await _dioClient.post(Endpoints.createChatMessage,
        queryParameters: request.toJson(),
        options: Options(headers: {'X-Socket-ID':socketId})
    );

    return AppResponse<ChatMessageEntity?>.fromJson(
        response.data,
        (dynamic json) => (response.data['success'] && json != null)
            ? ChatMessageEntity.fromJson(json)
            : null);
  }

  @override
  Future<AppResponse<List<ChatMessageEntity>>> getChatMessages(
      {required int chatId, required int page}) async {
    final response = await _dioClient.get(Endpoints.getChatMessage,
        queryParameters: {"chat_id": chatId, "page": page});

    return AppResponse<List<ChatMessageEntity>>.fromJson(
        response.data,
        (dynamic json) => (response.data['success'] && json != null)
            ? (json as List<dynamic>)
                .map((e) => ChatMessageEntity.fromJson(e))
                .toList()
            : []);
  }
}
