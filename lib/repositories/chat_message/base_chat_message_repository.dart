import 'package:school_erp/models/chat_message_model.dart';
import 'package:school_erp/models/requests/create_chat_message_request.dart';

import '../../models/app_response.dart';

abstract class BaseChatMessageRepository
{
  Future<AppResponse<List<ChatMessageEntity>>> getChatMessages({required int chatId,required int page});
  Future<AppResponse<ChatMessageEntity?>> createChatMessage(String socketId,CreateChaMessageRequest request);
}