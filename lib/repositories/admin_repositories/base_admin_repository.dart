abstract class BaseAdminRepository {
  Future<bool?> checkDoctorValidation(
      {required int USER_ID,
      required String DAY,
      required String BEGIN,
      required String END});
  Future<bool?> checkCategoryValidation({
    required String YEAR,
    required String SPECIALTY,
    required String DEPARTMENT,
    required int CATEGORY,
    required String DAY,
    required String BEGIN,
    required String END,
  });
  Future<int> createSubject(
      {required int year,
      required int duration,
      required var ids,
      required int category,
      required String specialty,
      required String department,
      required int dr,
      required String name,
      required String day,
      required String begin,
      required String end,
      required String laboratory});
}
