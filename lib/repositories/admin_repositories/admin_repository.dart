import 'package:dartz/dartz.dart';
import 'package:school_erp/graphql/quers/graphql_queries.dart';
import 'package:school_erp/models/app_response.dart';
import 'package:school_erp/repositories/admin_repositories/base_admin_repository.dart';
import 'package:school_erp/utils/logger.dart';

import '../../graphql/graphql_Services.dart';

class AdminRepository extends BaseAdminRepository {
  @override
  Future<bool?> checkDoctorValidation(
      {required int USER_ID,
      required String DAY,
      required String BEGIN,
      required String END}) async {
    var graphqlServices = GraphQlServices(
        query: GraphQlQueries.checkDoctorValidation(
            USER_ID: USER_ID, DAY: DAY, BEGIN: BEGIN, END: END));
    Map<String, dynamic> result = await graphqlServices.get();
    // wLog(result);
    return result['check_user_validation'];
  }

  @override
  Future<bool?> checkCategoryValidation({
    required String YEAR,
    required String SPECIALTY,
    required String DEPARTMENT,
    required int CATEGORY,
    required String DAY,
    required String BEGIN,
    required String END,
  }) async {
    var graphqlServices = GraphQlServices(
        query: GraphQlQueries.checkCategoryValidation(
      YEAR: YEAR,
      SPECIALTY: SPECIALTY,
      DEPARTMENT: DEPARTMENT,
      CATEGORY: CATEGORY,
      DAY: DAY,
      BEGIN: BEGIN,
      END: END,
    ));
    Map<String, dynamic> result = await graphqlServices.get();
    iLog(result);
    return result['check_validation'];
  }

  @override
  Future<int> createSubject({
    required int year,
    required int duration,
    required int category,
    required String specialty,
    required String department,
    required int dr,
    required String name,
    required String day,
    required String begin,
    required String end,
    required String laboratory,
    required var ids,
  }) async {
    var graphqlServices = GraphQlServices(
        query: GraphQlQueries.createSubject(
            year: year,
            duration: duration,
            category: category,
            specialty: specialty,
            department: department,
            dr: dr,
            name: name,
            day: day,
            begin: begin,
            end: end,
            laboratory: laboratory,
            ids: ids));
    Map<String, dynamic> result = await graphqlServices.post();
    return result["create_subject"]["category"];
    // return  result['category'];
  }
}
