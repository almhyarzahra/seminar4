import 'dart:io';

import 'package:dio/dio.dart';
import 'package:school_erp/models/app_response.dart';
import 'package:school_erp/models/chat_model.dart';
import 'package:school_erp/models/requests/create_chat_request.dart';
import 'package:school_erp/repositories/chat/base_chat_repository.dart';
import 'package:school_erp/repositories/core/endpoints.dart';
import 'package:school_erp/utils/dio_client/dio_client.dart';

import '../../bloc/blocs.dart';
import '../../utils/logger.dart';

class ChatRepository extends BaseChatRepository {
  final Dio _dioClient;

  ChatRepository({Dio? dioClient})
      : _dioClient = dioClient ?? DioClient().instance;

  @override
  Future<AppResponse<ChatEntity?>> createChat(CreateChatRequest request) async {
    _dioClient.options.headers[HttpHeaders.authorizationHeader] =
        'Bearer ${AuthBloc().state.token}';

    final response = await _dioClient.post(Endpoints.createChat,
        queryParameters: request.toJson());
    return AppResponse<ChatEntity?>.fromJson(
        response.data,
        (dynamic json) => response.data['success'] && json != null
            ? ChatEntity.fromJson(json)
            : null);
  }

  @override
  Future<AppResponse<List<ChatEntity>>> getChats() async {

    _dioClient.options.headers[HttpHeaders.authorizationHeader] =
        'Bearer ${AuthBloc().state.token}';

    final response = await _dioClient.get(Endpoints.getChats);

    return AppResponse<List<ChatEntity>>.fromJson(response.data,
        (dynamic json) {

      return response.data['success'] && json != null
          ? (json['chat'] as List<dynamic>)
              .map((e) => ChatEntity.fromJson(e))
              .toList()
          : [];
    });
  }

  @override
  Future<AppResponse<ChatEntity?>> getSingleChat(int chatId) async {
    final response = await _dioClient.get("${Endpoints.getSingleChat}$chatId");
    return AppResponse<ChatEntity?>.fromJson(
        response.data,
        (dynamic json) => response.data['success'] && json != null
            ? ChatEntity.fromJson(json)
            : null);
  }
}
