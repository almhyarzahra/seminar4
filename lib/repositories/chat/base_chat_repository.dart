import 'package:school_erp/models/requests/requests.dart';

import '../../models/app_response.dart';
import '../../models/chat_model.dart';

abstract class BaseChatRepository
{
Future<AppResponse<List<ChatEntity>>> getChats
    ();
Future<AppResponse<ChatEntity?>> createChat  (CreateChatRequest request);
Future<AppResponse<ChatEntity?>> getSingleChat
    (int chatId);


}