import 'package:school_erp/models/chat_message_model.dart';
import 'package:school_erp/models/models.dart';
import 'package:school_erp/models/notification.dart';
import 'package:school_erp/models/requests/create_chat_message_request.dart';
import 'package:school_erp/models/requests/get_user_notifications_request.dart';

import '../../models/app_response.dart';

abstract class BaseNotificationRepository {
  Future<AppResponse<List<notification>>> getNotification(
      {required GetUserNotificationRequest request});
}
