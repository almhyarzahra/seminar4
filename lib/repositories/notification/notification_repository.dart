import 'package:dio/dio.dart';
import 'package:school_erp/models/app_response.dart';
import 'package:school_erp/models/notification.dart';
import 'package:school_erp/models/requests/get_user_notifications_request.dart';
import 'package:school_erp/repositories/notification/base_notification_repository.dart';
import 'package:school_erp/utils/utils.dart';

import '../../models/models.dart';
import '../core/endpoints.dart';

class NotificationRepository extends BaseNotificationRepository {
  final Dio _dioClient;

  NotificationRepository({Dio? dioClient})
      : _dioClient = dioClient ?? DioClient().instance;

  @override
  Future<AppResponse<List<notification>>> getNotification(
      {required GetUserNotificationRequest request}) async {
    var response = await _dioClient.post(Endpoints.getUserNotifications,
        queryParameters: request.toJson());
    return AppResponse<List<notification>>.fromJson(response.data,
        (dynamic json) {
      if ((response.data['success'] && json != null)) {
        eLog(json);
        return (json as List<dynamic>)
            .map((e) => notification.fromJson(e))
            .toList();
      } else
        return [];
    });
  }
}
