import 'package:school_erp/models/apis/all_user_model.dart';
import 'package:school_erp/models/apis/category_model.dart';
import 'package:school_erp/models/apis/category_user_model.dart';
import 'package:school_erp/models/apis/chat_model.dart';
import 'package:school_erp/models/apis/doctor_model.dart';
import 'package:school_erp/models/apis/forget_password_model.dart';
import 'package:school_erp/models/apis/laboratory_model.dart';
import 'package:school_erp/models/apis/subject_properties_model.dart';
import 'package:school_erp/models/apis/subject_model.dart';
import 'package:school_erp/models/apis/shared_model.dart';
import 'package:school_erp/models/apis/token_info.dart';
import 'package:school_erp/models/models.dart';
import 'package:dartz/dartz.dart';

abstract class BaseUserRepository {
  Future<AppResponse<List<UserModel>>> getUsers();
  Future<Either<String?, ForgetPasswordModel>> verificationEmail(
      {required String email});
  Future<Either<String?, TokenInfo>> resetPassword({
    required String email,
    required String password,
    required String token,
  });
  Future<Either<String?, SubjectModel>> getSubjectUser();
  Future<Either<String?, CategoryUserModel>> getAllCategoryUser({
    required int category,
    required int year,
  });
  Future<Either<String, SharedModel>> takePresence({
    required int subjectId,
    required List<int> userId,
    required List<int> userIdAbsence,
  });
  Future<Either<String, SharedModel>> register({
    required String name,
    required String email,
    required String password,
    required int age,
    required int? category,
    required int? year,
    required int isDoctor,
  });
  Future<Either<String?, SharedModel>> UploadFile({
    required String? image,
    required String title,
  });
  Future<Either<String?, SharedModel>> storeLecture({
    required int subjectId,
    required String name,
    required String path,
  });
  Future<Either<String?, SubjectPropertiesModel>> getAllLectureSubject({
    required int subjectId,
  });
  Future<Either<String?, SharedModel>> storeCourses({
    required int subjectId,
    required String name,
    required String path,
  });
  Future<Either<String?, SubjectPropertiesModel>> getAllCoursesSubject({
    required int subjectId,
  });
  Future<Either<String?, SharedModel>> updateProfileImage({
    required String? image,
    required String title,
    required int userId,
  });
  Future<Either<String?, AllUserModel>> getAllUser();
  Future<Either<String?, ChatModel>> storChat({
    required int userId,
  });
  Future<Either<String?, String>> storeMessage({
    required int chatId,
    required String message,
  });
  Future<Either<String?, String>> broadCast({
    int chatId = 1,
    required String message,
    int broadcast = 1,
  });
  Future<Either<String?, LaboratoryModel>> getLaboratory({
    required String begin,
    required String end,
    required String day,
  });
  Future<Either<String?, DoctorModel>> getDoctor();
  Future<Either<String?, CategoryModel>> getIdFrowCategory({
    required int category,
  });
}
