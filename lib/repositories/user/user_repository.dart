import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:school_erp/bloc/auth/auth_bloc.dart';
import 'package:school_erp/enums/request_status.dart';
import 'package:school_erp/enums/request_type.dart';
import 'package:school_erp/models/apis/all_user_model.dart';
import 'package:school_erp/models/apis/category_model.dart';
import 'package:school_erp/models/apis/category_user_model.dart';
import 'package:school_erp/models/apis/chat_model.dart';
import 'package:school_erp/models/apis/doctor_model.dart';
import 'package:school_erp/models/apis/forget_password_model.dart';
import 'package:school_erp/models/apis/laboratory_model.dart';
import 'package:school_erp/models/apis/subject_properties_model.dart';
import 'package:school_erp/models/apis/subject_model.dart';
import 'package:school_erp/models/apis/shared_model.dart';
import 'package:school_erp/models/apis/token_info.dart';
import 'package:school_erp/models/app_response.dart';
import 'package:school_erp/models/common_response.dart';
import 'package:school_erp/models/user_model.dart';
import 'package:school_erp/repositories/core/network_config.dart';
import 'package:school_erp/repositories/user/base_user_repository.dart';
import 'package:school_erp/services/base_controller.dart';
import 'package:school_erp/utils/logger.dart';
import 'package:school_erp/utils/network_util.dart';

import '../../utils/dio_client/dio_client.dart';
import '../core/endpoints.dart';

class UserRepository extends BaseUserRepository {
  final Dio _dioClient;

  UserRepository({Dio? dioClient})
      : _dioClient = dioClient ?? DioClient().instance;

  @override
  Future<AppResponse<List<UserModel>>> getUsers() async {
    _dioClient.options.headers[HttpHeaders.authorizationHeader] =
        'Bearer ${AuthBloc().state.token}';

    var response = await _dioClient.get(Endpoints.allUser);
    return AppResponse<List<UserModel>>.fromJson(response.data, (dynamic json) {
      if (response.data['success'] && json != null) {
        return (json['user'] as List<dynamic>)
            .map((e) => UserModel.fromJson(e))
            .toList();
      }
      return [];
    });
  }

  @override
  Future<Either<String?, ForgetPasswordModel>> verificationEmail(
      {required String email}) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.POST,
        url: Endpoints.forget_password,
        params: {'email': email},
        headers: NetworkConfig.getHeaders(
          needAuth: false,
        ),
      ).then((response) {
        if (response == null) {
          return Left(null);
        }
        eLog(response);
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(ForgetPasswordModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, TokenInfo>> resetPassword({
    required String email,
    required String password,
    required String token,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: Endpoints.reset_password,
          headers: NetworkConfig.getHeaders(
            needAuth: false,
          ),
          body: {
            "email": email,
            "password": password,
            "token": token,
          }).then((response) {
        eLog(response);
        if (response == null) {
          return Left("Wrong acsess internet");
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(TokenInfo.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, SubjectModel>> getSubjectUser() async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: Endpoints.subject,
        headers: NetworkConfig.getHeaders(
          needAuth: true,
        ),
      ).then((response) {
        eLog(response);
        if (response == null) {
          return Left(null);
          // BaseController.requestStatus.value = RequestStatus.DEFUALT;
        }

        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(SubjectModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, CategoryUserModel>> getAllCategoryUser({
    required int category,
    required int year,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: Endpoints.user_category,
          headers: NetworkConfig.getHeaders(
            needAuth: true,
          ),
          body: {
            "category": category,
            "year": year,
          }).then((response) {
        eLog(response);
        if (response == null) {
          return Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(CategoryUserModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String, SharedModel>> takePresence({
    required int subjectId,
    required List<int> userId,
    required List<int> userIdAbsence,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: Endpoints.presence,
          headers: NetworkConfig.getHeaders(
            needAuth: true,
          ),
          body: {
            "subject_id": subjectId,
            "user_id": userId,
            "user_id_absence": userIdAbsence
          }).then((response) {
        eLog(response);
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(SharedModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String, SharedModel>> register({
    required String name,
    required String email,
    required String password,
    required int age,
    int? category,
    int? year,
    required int isDoctor,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: Endpoints.register,
          headers: NetworkConfig.getHeaders(
            needAuth: false,
          ),
          body: {
            "name": name,
            "email": email,
            "password": password,
            "age": age,
            "category": category,
            "year": year,
            "isDoctor": isDoctor,
          }).then((response) {
        if (response == null) {
          return left("No Internet Connection");
        }
        eLog(response);
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(SharedModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, SharedModel>> UploadFile({
    required String? image,
    required String title,
  }) async {
    try {
      return NetworkUtil.sendMultipartRequest(
        type: RequestType.POST,
        url: Endpoints.upload_file,
        fields: {'title': title},
        files: {"image": image},
        headers:
            NetworkConfig.getHeaders(needAuth: false, isMultipartRequest: true),
      ).then((response) {
        if (response == null) {
          return Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(SharedModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, SharedModel>> storeLecture({
    required int subjectId,
    required String name,
    required String path,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: Endpoints.store_lecture,
          headers: NetworkConfig.getHeaders(
            needAuth: false,
          ),
          body: {
            "subject_id": subjectId,
            "name": name,
            "path": path,
          }).then((response) {
        eLog(response);
        if (response == null) {
          return Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(SharedModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, SubjectPropertiesModel>> getAllLectureSubject({
    required int subjectId,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: Endpoints.show_lecture,
          headers: NetworkConfig.getHeaders(
            needAuth: false,
          ),
          body: {
            "subject_id": subjectId,
          }).then((response) {
        eLog(response);
        if (response == null) {
          return Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(
              SubjectPropertiesModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, SharedModel>> storeCourses({
    required int subjectId,
    required String name,
    required String path,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: Endpoints.store_course,
          headers: NetworkConfig.getHeaders(
            needAuth: false,
          ),
          body: {
            "subject_id": subjectId,
            "name": name,
            "path": path,
          }).then((response) {
        eLog(response);
        if (response == null) {
          return Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(SharedModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, SubjectPropertiesModel>> getAllCoursesSubject(
      {required int subjectId}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: Endpoints.show_course,
          headers: NetworkConfig.getHeaders(
            needAuth: false,
          ),
          body: {
            "subject_id": subjectId,
          }).then((response) {
        eLog(response);
        if (response == null) {
          return Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(
              SubjectPropertiesModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, SharedModel>> updateProfileImage({
    required String? image,
    required String title,
    required int userId,
  }) async {
    try {
      return NetworkUtil.sendMultipartRequest(
        type: RequestType.POST,
        url: Endpoints.update_profile_image,
        fields: {
          'title': title,
          'user_id': userId.toString(),
        },
        files: {"image": image},
        headers:
            NetworkConfig.getHeaders(needAuth: false, isMultipartRequest: true),
      ).then((response) {
        if (response == null) {
          return Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(SharedModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, AllUserModel>> getAllUser() async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: Endpoints.user,
        headers: NetworkConfig.getHeaders(
          needAuth: true,
        ),
      ).then((response) {
        eLog(response);
        if (response == null) {
          return Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(AllUserModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, ChatModel>> storChat({required int userId}) async {
    try {
      return NetworkUtil.sendMultipartRequest(
        type: RequestType.POST,
        url: Endpoints.chat,
        fields: {
          'user_id': userId.toString(),
        },
        headers:
            NetworkConfig.getHeaders(needAuth: true, isMultipartRequest: true),
      ).then((response) {
        if (response == null) {
          return Left("Please Try Agin");
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(ChatModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, String>> storeMessage({
    required int chatId,
    required String message,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: Endpoints.message,
          headers: NetworkConfig.getHeaders(
            needAuth: true,
          ),
          body: {
            "chat_id": chatId,
            "message": message,
          }).then((response) {
        eLog(response);
        if (response == null) {
          return Left("Please try Agin");
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right("Succsess");
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, String>> broadCast({
    int chatId = 21,
    required String message,
    int broadcast = 1,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: Endpoints.message,
          headers: NetworkConfig.getHeaders(
            needAuth: true,
          ),
          body: {
            "chat_id": chatId,
            "message": message,
            "broadcast": broadcast,
          }).then((response) {
        eLog(response);
        if (response == null) {
          return Left("Please try Agin");
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right("Succsess");
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, LaboratoryModel>> getLaboratory(
      {required String begin, required String end, required String day}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: Endpoints.laboratory,
          headers: NetworkConfig.getHeaders(
            needAuth: false,
          ),
          body: {
            "begin": begin,
            "end": end,
            "day": day,
          }).then((response) {
        eLog(response);
        if (response == null) {
          return Left("Please try Agin");
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(LaboratoryModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, DoctorModel>> getDoctor() async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: Endpoints.doctor,
        headers: NetworkConfig.getHeaders(
          needAuth: false,
        ),
      ).then((response) {
        eLog(response);
        if (response == null) {
          return Left("Please refresh");
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(DoctorModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String?, CategoryModel>> getIdFrowCategory(
      {required int category}) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: Endpoints.category,
          headers: NetworkConfig.getHeaders(
            needAuth: false,
          ),
          body: {"category": category}).then((response) {
        eLog(response);
        if (response == null) {
          return Left("Please try agin");
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(CategoryModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }
}
