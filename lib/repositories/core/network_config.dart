import 'package:school_erp/bloc/auth/auth_bloc.dart';
import 'package:school_erp/enums/request_type.dart';
import 'package:school_erp/ui/shared/utils.dart';

class NetworkConfig {
  static String BASE_API = 'api/';

  static String getFulApiUrl(String api) {
    return BASE_API + api;
  }

  static Map<String, String> getHeaders(
      {bool isMultipartRequest = false,
      bool? needAuth = true,
      RequestType? type = RequestType.POST,
      Map<String, String>? extraHeaders = const {}}) {
    return {
      if (needAuth!)
        'Authorization': 'Bearer ${store.getTokenInfoString() ?? ''}',
      if (type != RequestType.GET && isMultipartRequest == false)
        'Content-Type': 'application/json',
      'Accept': 'application/json',
      ...extraHeaders!
    };
  }
}
