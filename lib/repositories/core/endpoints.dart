import 'package:school_erp/repositories/core/network_config.dart';

class Endpoints {
  static const _apiVersion = 'api';

  //Auth
  static const _baseAuth = "$_apiVersion/auth";

  static const login = "$_apiVersion/sanctum/token";
// static const login="$_apiVersion/login";
  static const loginWithToken = "$_apiVersion/login_with_token";
  static const logout = "$_apiVersion/user/revoke";
  static const allUser = "$_apiVersion/allUser";

  //Chat
  static const _baseChat = "$_apiVersion/chat";

  static const getChats = "$_apiVersion/chat";
  static const getSingleChat = "$_baseChat/chat";
  static const createChat = "$_apiVersion/chat";

  //Chat Message
  static const _baseChatMessage = "$_apiVersion/chat_message";
  static const getChatMessage = "$_apiVersion/message";
  static const createChatMessage = "$_apiVersion/message";

//User
  static const getUsers = "$_apiVersion/user";
  static const getUserNotifications = "$_apiVersion/find_user_notifications";

  static String forget_password = NetworkConfig.getFulApiUrl('forgot-password');
  static String reset_password = NetworkConfig.getFulApiUrl('reset-Password');
  static String subject = NetworkConfig.getFulApiUrl('subject');
  static String user_category =
      NetworkConfig.getFulApiUrl('get_users_category');
  static String presence = NetworkConfig.getFulApiUrl('presence');
  static String register = NetworkConfig.getFulApiUrl('register');
  static String upload_file = NetworkConfig.getFulApiUrl('image');
  static String store_lecture = NetworkConfig.getFulApiUrl('doctor/lecture');
  static String show_lecture =
      NetworkConfig.getFulApiUrl('find_subject_lectures');
  static String store_course = NetworkConfig.getFulApiUrl('courses');
  static String show_course =
      NetworkConfig.getFulApiUrl('find_subject_courses');
  static String update_profile_image =
      NetworkConfig.getFulApiUrl('update_profile_picture');
  static String user = NetworkConfig.getFulApiUrl('allUser_broadcast');
  static String chat = NetworkConfig.getFulApiUrl('chat');
  static String message = NetworkConfig.getFulApiUrl('message');
  static String laboratory =
      NetworkConfig.getFulApiUrl('find_empty_laboratory');
  static String doctor = NetworkConfig.getFulApiUrl('doctor/find_all_doctors');
  static String category = NetworkConfig.getFulApiUrl('user_category');
}
