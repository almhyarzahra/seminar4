import 'package:get_it/get_it.dart';

import 'livestream/data/network_services/livestream_ns.dart';
import 'livestream/data/repositories/create_livestream_repository.dart';
import 'livestream/data/repositories/sync_livestream_repository.dart';

final locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => LivestreamNS());
  locator.registerLazySingleton(
      () => LiveStreamRepository(locator<LivestreamNS>()));
  locator.registerLazySingleton(
      () => CreateLiveStreamRepository(locator<LivestreamNS>()));
}
