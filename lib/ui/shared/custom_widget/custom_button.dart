import 'package:flutter/material.dart';

import 'package:school_erp/ui/shared/colors.dart';

class CustomButton extends StatefulWidget {
  const CustomButton({
    super.key,
    required this.onPressed,
    required this.Text,
    this.backgroundColor,
    this.TextColor,
    this.suffixIcon,
    this.fontWeight,
    this.width,
    this.height,
  });
  final VoidCallback onPressed;
  final String Text;
  final Color? backgroundColor;
  final IconData? suffixIcon;
  final Color? TextColor;
  final FontWeight? fontWeight;
  final double? width;
  final double? height;

  @override
  State<CustomButton> createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return ElevatedButton(
        onPressed: () {
          widget.onPressed();
        },
        style: ElevatedButton.styleFrom(
          shape: StadiumBorder(),
          backgroundColor: widget.backgroundColor ?? AppColors.mainBlueColor,
          fixedSize: Size(widget.width ?? size.width * 0.85,
              widget.height ?? size.height * 0.06),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (widget.suffixIcon != null) ...[
              Expanded(
                child: Center(
                  child: Text(
                    widget.Text,
                    style: TextStyle(
                        color: widget.TextColor ?? AppColors.mainWhiteColor,
                        fontWeight: widget.fontWeight ?? FontWeight.normal),
                  ),
                ),
              ),
              Icon(
                widget.suffixIcon,
                color: AppColors.mainBlackColor,
              ),
            ] else
              Text(
                widget.Text,
                style: TextStyle(
                    color: widget.TextColor ?? AppColors.mainWhiteColor,
                    fontWeight: widget.fontWeight ?? FontWeight.normal),
              ),
          ],
        ));
  }
}
