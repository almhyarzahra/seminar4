import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';

class CustomButtonVertical extends StatefulWidget {
  const CustomButtonVertical(
      {super.key,
      required this.onPressed,
      required this.svgName,
      required this.Text});

  final VoidCallback onPressed;
  final String svgName;
  final String Text;

  @override
  State<CustomButtonVertical> createState() => _CustomButtonVerticalState();
}

class _CustomButtonVerticalState extends State<CustomButtonVertical> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return ElevatedButton(
      onPressed: () {
        widget.onPressed();
      },
      child: Padding(
        padding:
            EdgeInsets.only(top: size.width * 0.05, right: size.width * 0.1),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SvgPicture.asset("images/${widget.svgName}.svg"),
            (size.width * 0.04).ph,
            Text(
              widget.Text,
              style: TextStyle(color: AppColors.mainBlackColor),
            )
          ],
        ),
      ),
      style: ElevatedButton.styleFrom(
        primary: AppColors.mainGrey2Color,
        fixedSize: Size(size.width * 0.45, size.width * 0.3),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(size.width * 0.05),
        ),
      ),
    );
  }
}
