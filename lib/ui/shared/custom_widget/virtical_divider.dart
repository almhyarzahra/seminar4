import 'package:flutter/material.dart';
import 'package:school_erp/ui/shared/colors.dart';

class VirticalDivider extends StatelessWidget {
  const VirticalDivider({super.key});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Center(
      child: Container(
        width: size.width * 0.4,
        height: size.width * 0.01,
        color: AppColors.mainGreyColor,
      ),
    );
  }
}
