import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:school_erp/repositories/shared_preference_repository.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_button.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';

bool isEmail(String value) {
  RegExp regExp = new RegExp(
      (r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+"));
  return regExp.hasMatch(value);
}

bool isPassword(String value) {
  RegExp regExp = new RegExp(r'^.{8,}$');

  return regExp.hasMatch(value);
}

bool isTowNumber(String value) {
  RegExp regExp = new RegExp(r'^\d{2}$');

  return regExp.hasMatch(value);
}

bool isCategory(String value) {
  RegExp regExp = new RegExp(r'^\d{1,2}$');

  return regExp.hasMatch(value);
}

bool isCorrectYear(String value) {
  RegExp regExp = new RegExp(r'^[1-5]$');

  return regExp.hasMatch(value);
}

bool isName(String value) {
  RegExp regExp = new RegExp(r'^[a-zA-Z]+$');

  return regExp.hasMatch(value);
}

bool isIDUniversity(String value) {
  RegExp regExp = new RegExp(r'^\d{5}$');

  return regExp.hasMatch(value);
}

bool isTwoYear(String value) {
  RegExp regExp = new RegExp(r'^\d{4}\/\d{4}$');

  return regExp.hasMatch(value);
}

double screenWidth(double perecent) {
  return Get.size.width / perecent;
}

double screenHeight(double perecent) {
  return Get.size.height / perecent;
}

void customLoader() => BotToast.showCustomLoading(toastBuilder: (builder) {
      return Container(
        width: screenWidth(5),
        height: screenWidth(5),
        decoration: BoxDecoration(
            color: AppColors.mainBlackColor.withOpacity(0.5),
            borderRadius: BorderRadius.circular(15)),
        child: SpinKitCircle(color: AppColors.mainBlueColor),
      );
    });

SharedPreferenceRepository get store => Get.find<SharedPreferenceRepository>();

extension StringCasingExtension on String {
  String toCapitalized() =>
      '${this[0].toUpperCase()}${substring(1).toLowerCase()}';
  String toTitleCase() => replaceAll(RegExp(' +'), ' ')
      .split(' ')
      .map((str) => str.toCapitalized())
      .join(' ');
}

Widget customFaildConnection({required VoidCallback onPressed}) => Column(
      children: [
        Image.asset("images/no_internet.png"),
        CustomText(content: "No Internet Connnection, Please Try Agin"),
        screenHeight(20).ph,
        Padding(
          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(3)),
          child: CustomButton(
            onPressed: onPressed,
            Text: "Try Agin",
            backgroundColor: AppColors.mainBlue250Color,
          ),
        )
      ],
    );
