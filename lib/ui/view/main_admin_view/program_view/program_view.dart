import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:school_erp/enums/request_status.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/ui.dart';
import 'package:school_erp/ui/view/main_admin_view/program_view/program_controller.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';

class ProgramView extends StatefulWidget {
  const ProgramView({super.key});

  @override
  State<ProgramView> createState() => _ProgramViewState();
}

class _ProgramViewState extends State<ProgramView> {
  late ProgramController controller;
  @override
  void initState() {
    controller = Get.put(ProgramController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: CustomText(
          content: "Program",
          fontWeight: FontWeight.bold,
          fontSize: screenWidth(20),
          colorText: AppColors.mainWhiteColor,
        ),
        backgroundColor: AppColors.mainBlue250Color,
      ),
      body: RefreshIndicator(
          onRefresh: () async {
            controller.onInit();
          },
          child: ListView(
            children: [
              Obx(() {
                return controller.requestStatus == RequestStatus.LOADING
                    ? SpinKitCircle(
                        color: AppColors.mainBlue250Color,
                      )
                    : controller.counter.value == false
                        ? Padding(
                            padding: EdgeInsetsDirectional.symmetric(
                                horizontal: screenWidth(10)),
                            child: Card(
                              elevation: 10,
                              shadowColor: AppColors.mainBlueColor,
                              margin: EdgeInsetsDirectional.symmetric(
                                  horizontal: screenWidth(100),
                                  vertical: screenWidth(30)),
                              child: Padding(
                                padding: EdgeInsetsDirectional.symmetric(
                                    horizontal: screenWidth(30),
                                    vertical: screenWidth(20)),
                                child: Container(
                                  width: screenWidth(1.2),
                                  child: Column(
                                    children: [
                                      CustomTextField(
                                          controller:
                                              controller.nameSubjectController,
                                          labelText: "Name Subject"),
                                      screenWidth(10).ph,
                                      DropdownButton<int>(
                                        hint: Padding(
                                          padding: EdgeInsetsDirectional.only(
                                              start: screenWidth(15)),
                                          child: CustomText(
                                              content: controller
                                                      .doctorName.value.isEmpty
                                                  ? "Select Doctor"
                                                  : controller.doctorName.value
                                                      .toCapitalized()),
                                        ),
                                        //value: controller.idUser.value,
                                        items: controller.doctor.value.data!
                                            .map((e) {
                                          return DropdownMenuItem<int>(
                                            value: e.id,
                                            child:
                                                Text(e.name!.toCapitalized()),
                                          );
                                        }).toList(),
                                        onChanged: (value) {
                                          controller.idDoctor.value = value!;
                                          controller.doctor.value.data!.map(
                                            (e) {
                                              if (e.id ==
                                                  controller.idDoctor.value)
                                                controller.doctorName.value =
                                                    e.name!;
                                            },
                                          ).toSet();
                                        },
                                      ),
                                      DropdownButton<String>(
                                        hint: Padding(
                                          padding: EdgeInsetsDirectional.only(
                                              start: screenWidth(15)),
                                          child: CustomText(
                                              content: controller
                                                      .dayName.value.isEmpty
                                                  ? "Select Day"
                                                  : controller.dayName.value
                                                      .toCapitalized()),
                                        ),
                                        items: controller.week.map((e) {
                                          return DropdownMenuItem<String>(
                                            value: e,
                                            child: Text(e.toCapitalized()),
                                          );
                                        }).toList(),
                                        onChanged: (value) {
                                          controller.dayName.value = value!;
                                        },
                                      ),
                                      screenWidth(10).ph,
                                      CustomTextField(
                                          typeInput: TextInputType.number,
                                          controller:
                                              controller.beginController,
                                          labelText: "Begin"),
                                      CustomTextField(
                                          typeInput: TextInputType.number,
                                          controller: controller.endController,
                                          labelText: "End"),
                                      screenWidth(5).ph,
                                      CustomButton(
                                          backgroundColor:
                                              AppColors.mainBlue250Color,
                                          onPressed: () {
                                            controller.firstCheck();
                                          },
                                          Text: "Check")
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Padding(
                            padding: EdgeInsetsDirectional.symmetric(
                                horizontal: screenWidth(10)),
                            child: Card(
                              elevation: 10,
                              shadowColor: AppColors.mainBlueColor,
                              margin: EdgeInsetsDirectional.symmetric(
                                  horizontal: screenWidth(100),
                                  vertical: screenWidth(30)),
                              child: Padding(
                                padding: EdgeInsetsDirectional.symmetric(
                                    horizontal: screenWidth(30),
                                    vertical: screenWidth(20)),
                                child: Container(
                                  width: screenWidth(1.2),
                                  child: Column(
                                    children: [
                                      DropdownButton<String>(
                                        hint: Padding(
                                          padding: EdgeInsetsDirectional.only(
                                              start: screenWidth(10)),
                                          child: CustomText(
                                              content: controller.laboratoryName
                                                      .value.isEmpty
                                                  ? "Select Laboratory"
                                                  : controller
                                                      .laboratoryName.value
                                                      .toCapitalized()),
                                        ),
                                        items: controller.laboratory.map((e) {
                                          return DropdownMenuItem<String>(
                                            value: e,
                                            child: Text(e.toCapitalized()),
                                          );
                                        }).toList(),
                                        onChanged: (value) {
                                          controller.laboratoryName.value =
                                              value!;
                                        },
                                      ),
                                      screenWidth(8).ph,
                                      CustomTextField(
                                          typeInput: TextInputType.number,
                                          controller:
                                              controller.categoryController,
                                          labelText: "Category"),
                                      CustomTextField(
                                          typeInput: TextInputType.number,
                                          controller: controller.yearController,
                                          labelText: "Year"),
                                      screenWidth(8).ph,
                                      DropdownButton<String>(
                                        hint: Padding(
                                          padding: EdgeInsetsDirectional.only(
                                              start: screenWidth(10)),
                                          child: CustomText(
                                              content: controller.departmentName
                                                      .value.isEmpty
                                                  ? "Select Department"
                                                  : controller
                                                      .departmentName.value
                                                      .toCapitalized()),
                                        ),
                                        items: controller.department.map((e) {
                                          return DropdownMenuItem<String>(
                                            value: e,
                                            child: Text(e.toCapitalized()),
                                          );
                                        }).toList(),
                                        onChanged: (value) {
                                          controller.departmentName.value =
                                              value!;
                                        },
                                      ),
                                      DropdownButton<String>(
                                        hint: Padding(
                                          padding: EdgeInsetsDirectional.only(
                                              start: screenWidth(10)),
                                          child: CustomText(
                                              content: controller.specialtyName
                                                      .value.isEmpty
                                                  ? "Select Specialty"
                                                  : controller
                                                      .specialtyName.value
                                                      .toCapitalized()),
                                        ),
                                        items: controller.specialty.map((e) {
                                          return DropdownMenuItem<String>(
                                            value: e,
                                            child: Text(e.toCapitalized()),
                                          );
                                        }).toList(),
                                        onChanged: (value) {
                                          controller.specialtyName.value =
                                              value!;
                                        },
                                      ),
                                      screenWidth(10).ph,
                                      CustomButton(
                                          backgroundColor:
                                              AppColors.mainBlue250Color,
                                          onPressed: () {
                                            controller.finish();
                                          },
                                          Text: "Finish")
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
              }),
            ],
          )),
    ));
  }
}
