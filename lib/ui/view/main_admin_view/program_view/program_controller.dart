import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:school_erp/enums/message_type.dart';
import 'package:school_erp/models/apis/doctor_model.dart';
import 'package:school_erp/models/apis/laboratory_model.dart';
import 'package:school_erp/repositories/admin_repositories/admin_repository.dart';
import 'package:school_erp/repositories/repositories.dart';
import 'package:school_erp/services/base_controller.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_toast.dart';

class ProgramController extends BaseController {
  Rx<DoctorModel> doctor = DoctorModel().obs;
  RxList<String> laboratory = <String>[
    "l1",
    "l2",
    "l3",
    "l4",
    "l5",
    "l6",
    "l7",
    "l8",
    "c1",
    "c2",
    "c3",
    "c4",
    "c5",
    "c6",
    "c7",
    "c8",
    "c9",
    "c10",
    "c11",
    "c12"
  ].obs;
  TextEditingController nameSubjectController = TextEditingController();
  RxString doctorName = ''.obs;
  RxInt idDoctor = 0.obs;

  TextEditingController beginController = TextEditingController();
  TextEditingController endController = TextEditingController();
  TextEditingController categoryController = TextEditingController();
  TextEditingController yearController = TextEditingController();
  RxString departmentName = ''.obs;
  RxString specialtyName = ''.obs;
  Rx<LaboratoryModel> laboratoryNotAvailable = LaboratoryModel().obs;
  RxList<String> laboratoryAvailable = <String>[].obs;
  RxString dayName = ''.obs;
  RxList<String> week =
      ["sunday", "monday", "tuesday", "wedensday", "thursday"].obs;
  RxList<String> department = ["practical", "theoretical"].obs;
  RxList<String> specialty = ["programming", "networks"].obs;
  RxBool counter = false.obs;
  RxString laboratoryName = ''.obs;
  RxList<int> userId = <int>[].obs;

  @override
  void onInit() {
    getDoctor();
    super.onInit();
  }

  void getDoctor() {
    runLoadingFutureFunction(
        function: UserRepository().getDoctor().then((value) {
      value.fold((l) {
        CustomToast.showMessage(message: l!, messageType: MessageType.REJECTED);
      }, (r) {
        doctor.value = r;
      });
    }));
  }

  void firstCheck() {
    runFullLoadingFutureFunction(
        function: AdminRepository()
            .checkDoctorValidation(
                USER_ID: idDoctor.value,
                DAY: dayName.value,
                BEGIN: beginController.text.toString(),
                END: endController.text.toString())
            .then((value) {
      if (value == true) {
        CustomToast.showMessage(
            message: "Doctor has lacture in this time",
            messageType: MessageType.REJECTED);
      } else if (value == null) {
        CustomToast.showMessage(
            message: "Avaliable", messageType: MessageType.SUCCSESS);
        runLoadingFutureFunction(
            function: UserRepository()
                .getLaboratory(
                    begin: beginController.text.toString(),
                    end: endController.text.toString(),
                    day: dayName.value)
                .then((value) {
          value.fold((l) {
            CustomToast.showMessage(
                message: l!, messageType: MessageType.REJECTED);
          }, (r) {
            laboratoryNotAvailable.value = r;

            r.data!.map((e) {
              laboratory.remove(e);
            });
            counter.value = true;
          });
        }));
      }
    }));
  }

  void finish() {
    userId.clear();
    runFullLoadingFutureFunction(
        function: AdminRepository()
            .checkCategoryValidation(
                CATEGORY: int.parse(categoryController.text.toString()),
                DAY: dayName.value,
                BEGIN: beginController.text.toString(),
                END: endController.text.toString(),
                DEPARTMENT: departmentName.value,
                SPECIALTY: specialtyName.value,
                YEAR: yearController.text.toString())
            .then((value) {
      if (value == true) {
        CustomToast.showMessage(
            message: "invalid category", messageType: MessageType.REJECTED);
      } else if (value == null) {
        runFullLoadingFutureFunction(
            function: UserRepository()
                .getIdFrowCategory(
                    category: int.parse(categoryController.text.toString()))
                .then((value) {
          value.fold((l) {
            CustomToast.showMessage(
                message: l!, messageType: MessageType.REJECTED);
          }, (r) {
            userId.add(idDoctor.value);
            r.data!.map((e) {
              userId.add(e.id!);
            }).toSet();
            print(userId);
            AdminRepository()
                .createSubject(
                    laboratory: laboratoryName.value,
                    name: nameSubjectController.text.toString(),
                    duration: 12,
                    category: int.parse(categoryController.text.toString()),
                    department: departmentName.value,
                    dr: idDoctor.value,
                    specialty: specialtyName.value,
                    day: dayName.value,
                    year: int.parse(yearController.text.toString()),
                    end: endController.text.toString(),
                    begin: beginController.text.toString(),
                    ids: userId)
                .then((value) {
              CustomToast.showMessage(
                  message: "Succsess", messageType: MessageType.SUCCSESS);
              counter.value = false;
            });
          });
        }));
      }
    }));
  }

  // void getLaboratory(){
  //   UserRepository().getLaboratory(begin: begin, end: end, day: day)
  // }
}
