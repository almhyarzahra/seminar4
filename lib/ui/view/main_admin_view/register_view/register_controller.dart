import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:school_erp/enums/message_type.dart';
import 'package:school_erp/repositories/repositories.dart';
import 'package:school_erp/services/base_controller.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_toast.dart';

class RegisterController extends BaseController {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passController = TextEditingController();
  TextEditingController ageController = TextEditingController();
  TextEditingController yearController = TextEditingController();
  TextEditingController categoryController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  RxInt groupValue = 1.obs;

  void register() async {
    if (formKey.currentState!.validate()) {
      runFullLoadingFutureFunction(
          function: UserRepository().register(
              name: nameController.text.toString(),
              email: emailController.text.toString(),
              password: passController.text.toString(),
              age: int.parse(ageController.text.toString()),
              category: categoryController.text.isEmpty
                  ? null
                  : int.parse(categoryController.text.toString()),
              year: yearController.text.isEmpty
                  ? null
                  : int.parse(yearController.text.toString()),
              isDoctor: groupValue.value)
            ..then((value) {
              value.fold((l) {
                CustomToast.showMessage(
                    message: l, messageType: MessageType.REJECTED);
              }, (r) {
                CustomToast.showMessage(
                    message: "Succsess Register",
                    messageType: MessageType.SUCCSESS);
              });
            }));
    }
  }
}
