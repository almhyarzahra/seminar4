import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:school_erp/bloc/auth/auth_bloc.dart';
import 'package:school_erp/cubit/guest_cubit.dart';
import 'package:school_erp/enums/message_type.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_toast.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/ui.dart';
import 'package:school_erp/ui/view/login_view/login_view.dart';
import 'package:school_erp/ui/view/main_admin_view/register_view/register_controller.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';

class RegisterView extends StatefulWidget {
  const RegisterView({super.key});

  @override
  State<RegisterView> createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  RegisterController controller = RegisterController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.mainGrey2Color,
      appBar: AppBar(
        title: CustomText(
          content: "Sign up Doctor or Student",
          fontWeight: FontWeight.bold,
          fontSize: screenWidth(20),
          colorText: AppColors.mainWhiteColor,
        ),
        backgroundColor: AppColors.mainBlue250Color,
        leading: IconButton(
            onPressed: () {
              Get.defaultDialog(
                  title: "Are you sure you want to leave?",
                  titleStyle: TextStyle(color: AppColors.mainBlue250Color),
                  content: Column(
                    children: [
                      BlocConsumer<AuthBloc, AuthState>(
                          builder: (context, state) => Padding(
                                padding: EdgeInsetsDirectional.symmetric(
                                    horizontal: screenWidth(5)),
                                child: CustomButton(
                                  backgroundColor: AppColors.mainBlue250Color,
                                  onPressed: () {
                                    // vLog();
                                    context
                                        .read<GuestCubit>()
                                        .signOut(context)
                                        .then((value) {
                                      if (value == "No Internet Connection") {
                                        CustomToast.showMessage(
                                            message: value,
                                            messageType: MessageType.REJECTED);
                                      }
                                    });
                                  },
                                  Text: "Yes",
                                ),
                              ),
                          listener: (context, state) {
                            if (!state.isAuthenticated) {
                              Get.back();
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (_) => LoginView()));
                            }
                          }),
                      Padding(
                        padding: EdgeInsetsDirectional.symmetric(
                            horizontal: screenWidth(5)),
                        child: CustomButton(
                            backgroundColor: AppColors.mainBlue250Color,
                            onPressed: () {
                              Get.back();
                            },
                            Text: "No"),
                      )
                    ],
                  ));
            },
            icon: Icon(Icons.logout_outlined)),
      ),
      body: Form(
        key: controller.formKey,
        child: ListView(
          children: [
            Card(
              elevation: 10,
              shadowColor: AppColors.mainBlue250Color,
              margin: EdgeInsetsDirectional.symmetric(
                  horizontal: screenWidth(30), vertical: screenWidth(30)),
              child: Padding(
                padding: EdgeInsetsDirectional.symmetric(
                    horizontal: screenWidth(30)),
                child: Column(
                  children: [
                    CustomTextField(
                      controller: controller.nameController,
                      fontWeight: FontWeight.bold,
                      labelText: "Name",
                      validator: (value) {
                        return value!.isEmpty
                            // || !isName(value)
                            ? "Please Check Name"
                            : null;
                      },
                    ),
                    (screenWidth(30)).ph,
                    CustomTextField(
                      controller: controller.emailController,
                      fontWeight: FontWeight.bold,
                      labelText: "Email",
                      validator: (value) {
                        return value!.isEmpty || !GetUtils.isEmail(value)
                            ? "Please Check Email"
                            : null;
                      },
                    ),
                    (screenWidth(30)).ph,
                    CustomTextField(
                      controller: controller.passController,
                      fontWeight: FontWeight.bold,
                      labelText: "Password",
                      validator: (value) {
                        return value!.isEmpty || !isPassword(value)
                            ? "Please Check Password(length =8)"
                            : null;
                      },
                    ),
                    (screenWidth(30)).ph,
                    CustomTextField(
                      controller: controller.ageController,
                      typeInput: TextInputType.number,
                      fontWeight: FontWeight.bold,
                      labelText: "Age",
                      validator: (value) {
                        return value!.isEmpty || !isTowNumber(value)
                            ? "Please Check Age"
                            : null;
                      },
                    ),
                    (screenWidth(30)).ph,
                  ],
                ),
              ),
            ),
            (screenWidth(50)).ph,
            Card(
              elevation: 20,
              shadowColor: AppColors.mainBlue250Color,
              margin: EdgeInsetsDirectional.symmetric(
                  horizontal: screenWidth(30), vertical: screenWidth(30)),
              child: Padding(
                padding: EdgeInsetsDirectional.symmetric(
                    horizontal: screenWidth(30), vertical: screenWidth(20)),
                child: Column(
                  children: [
                    CustomText(
                      content: "Who do you sign up for?",
                      fontWeight: FontWeight.bold,
                    ),
                    Obx(() {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Row(
                            children: [
                              Radio(
                                  value: 0,
                                  groupValue: controller.groupValue.value,
                                  onChanged: (newValue) => controller.groupValue
                                      .value = int.parse(newValue.toString())),
                              CustomText(content: "Student")
                            ],
                          ),
                          Row(
                            children: [
                              Radio(
                                  value: 1,
                                  groupValue: controller.groupValue.value,
                                  onChanged: (newValue) {
                                    controller.groupValue.value =
                                        int.parse(newValue.toString());
                                    controller.yearController.text = "";
                                    controller.categoryController.text = "";
                                  }),
                              CustomText(content: "Doctor")
                            ],
                          ),
                        ],
                      );
                    }),
                    Obx(() {
                      return controller.groupValue.value == 0
                          ? Column(
                              children: [
                                CustomTextField(
                                  controller: controller.yearController,
                                  typeInput: TextInputType.number,
                                  fontWeight: FontWeight.bold,
                                  labelText: "Year",
                                  validator: (value) {
                                    return value!.isEmpty ||
                                            !isCorrectYear(value)
                                        ? "Please Check Year"
                                        : null;
                                  },
                                ),
                                CustomTextField(
                                  controller: controller.categoryController,
                                  typeInput: TextInputType.number,
                                  fontWeight: FontWeight.bold,
                                  labelText: "Category",
                                  validator: (value) {
                                    return value!.isEmpty || !isCategory(value)
                                        ? "Please Check Category"
                                        : null;
                                  },
                                ),
                              ],
                            )
                          : SizedBox.shrink();
                    }),
                    Padding(
                      padding: EdgeInsetsDirectional.only(top: screenWidth(20)),
                      child: CustomButton(
                        backgroundColor: AppColors.mainBlue250Color,
                        onPressed: () {
                          controller.register();
                        },
                        Text: "Sign up",
                        fontWeight: FontWeight.bold,
                        TextColor: AppColors.mainWhiteColor,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    ));
  }
}
