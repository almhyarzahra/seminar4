import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:school_erp/enums/message_type.dart';
import 'package:school_erp/models/apis/all_user_model.dart';
import 'package:school_erp/repositories/user/user_repository.dart';
import 'package:school_erp/services/base_controller.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_toast.dart';

class HomeAdminController extends BaseController {
  TextEditingController advertisementController = TextEditingController();
  RxInt groupValue = 1.obs;
  Rx<AllUserModel> allUser = AllUserModel().obs;
  RxInt idUser = 0.obs;
  RxString userName = ''.obs;
  @override
  void onInit() {
    getAllUser();
    super.onInit();
  }

  void getAllUser() {
    runLoadingFutureFunction(
        function: UserRepository().getAllUser().then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: "Please Refresh", messageType: MessageType.REJECTED);
      }, (r) {
        allUser.value = r;
      });
    }));
  }

  void send() {
    if (groupValue.value == 1) {
      runFullLoadingFutureFunction(
          function: UserRepository()
              .broadCast(message: advertisementController.text.toString())
              .then((value) {
        value.fold((l) {
          CustomToast.showMessage(
              message: l!, messageType: MessageType.REJECTED);
        }, (r) {
          CustomToast.showMessage(
              message: "Succsess", messageType: MessageType.SUCCSESS);
          Get.back();
        });
      }));
    } else {
      runFullLoadingFutureFunction(
          function:
              UserRepository().storChat(userId: idUser.value).then((value) {
        value.fold((l) {
          CustomToast.showMessage(
              message: "Please Try Agin", messageType: MessageType.REJECTED);
        }, (r) {
          runFullLoadingFutureFunction(
              function: UserRepository()
                  .storeMessage(
                      chatId: r.data!.id!,
                      message: advertisementController.text.toString())
                  .then((value) {
            value.fold((l) {
              CustomToast.showMessage(
                  message: "Please Try Agin",
                  messageType: MessageType.REJECTED);
            }, (r) {
              CustomToast.showMessage(
                  message: "Succsess", messageType: MessageType.SUCCSESS);
              Get.back();
            });
          }));
        });
      }));
    }
  }
}
