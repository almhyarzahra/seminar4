import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:school_erp/enums/request_status.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/ui.dart';
import 'package:school_erp/ui/view/main_admin_view/home_admin_view/home_admin_controller.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';

class HomeAdminView extends StatefulWidget {
  const HomeAdminView({super.key});

  @override
  State<HomeAdminView> createState() => _HomeAdminViewState();
}

class _HomeAdminViewState extends State<HomeAdminView> {
  late HomeAdminController controller;
  @override
  void initState() {
    controller = Get.put(HomeAdminController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          content: "Advertisements",
          fontWeight: FontWeight.bold,
          fontSize: screenWidth(20),
          colorText: AppColors.mainWhiteColor,
        ),
        backgroundColor: AppColors.mainBlue250Color,
      ),
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsetsDirectional.symmetric(
                horizontal: screenWidth(15), vertical: screenWidth(15)),
            child: CustomTextField(
              controller: controller.advertisementController,
              labelText: "Add Advertisement",
              maxLines: 10,
              fillColor: AppColors.mainGreyColor.withOpacity(0.5),
              filled: true,
              labelColor: AppColors.mainBlue250Color,
            ),
          ),
          Obx(() {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  children: [
                    Radio(
                        value: 0,
                        groupValue: controller.groupValue.value,
                        onChanged: (newValue) {
                          controller.groupValue.value =
                              int.parse(newValue.toString());
                        }),
                    CustomText(content: "Private")
                  ],
                ),
                Row(
                  children: [
                    Radio(
                        value: 1,
                        groupValue: controller.groupValue.value,
                        onChanged: (newValue) {
                          controller.groupValue.value =
                              int.parse(newValue.toString());
                        }),
                    CustomText(content: "Public Announcement")
                  ],
                ),
              ],
            );
          }),
          Obx(() {
            return controller.groupValue.value == 0
                ? controller.requestStatus == RequestStatus.LOADING
                    ? SpinKitCircle(
                        color: AppColors.mainBlue250Color,
                      )
                    : Padding(
                        padding: EdgeInsetsDirectional.symmetric(
                            horizontal: screenWidth(10)),
                        child: DropdownButton<int>(
                          hint: Padding(
                            padding: EdgeInsetsDirectional.only(
                                start: screenWidth(4)),
                            child: CustomText(
                                content: controller.userName.value.isEmpty
                                    ? "Select User"
                                    : controller.userName.value
                                        .toCapitalized()),
                          ),
                          //value: controller.idUser.value,
                          items: controller.allUser.value.data!.user!.map((e) {
                            return DropdownMenuItem<int>(
                              value: e.id,
                              child: Text(e.name ?? ""),
                            );
                          }).toList(),
                          onChanged: (value) {
                            controller.idUser.value = value!;
                            controller.allUser.value.data!.user!.map(
                              (e) {
                                if (e.id == controller.idUser.value)
                                  controller.userName.value = e.name!;
                              },
                            ).toSet();
                          },
                        ),
                      )
                : SizedBox.shrink();
          }),
          Padding(
            padding: EdgeInsetsDirectional.symmetric(
                horizontal: screenWidth(10), vertical: screenWidth(10)),
            child: CustomButton(
                backgroundColor: AppColors.mainBlue250Color,
                onPressed: () {
                  Get.defaultDialog(
                      title: "Are You Done?",
                      titleStyle: TextStyle(color: AppColors.mainBlue250Color),
                      content: Column(
                        children: [
                          Padding(
                            padding: EdgeInsetsDirectional.symmetric(
                                horizontal: screenWidth(10)),
                            child: CustomButton(
                              onPressed: () {
                                controller.send();
                              },
                              Text: "Yes",
                              backgroundColor: AppColors.mainBlue250Color,
                            ),
                          ),
                          (screenHeight(45)).ph,
                          Padding(
                            padding: EdgeInsetsDirectional.symmetric(
                                horizontal: screenWidth(10)),
                            child: CustomButton(
                              onPressed: () {
                                Get.back();
                              },
                              Text: "No",
                              backgroundColor: AppColors.mainBlue250Color,
                            ),
                          ),
                        ],
                      ));
                },
                Text: "Send"),
          )
        ],
      ),
    );
  }
}
