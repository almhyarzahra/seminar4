import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/view/main_admin_view/home_admin_view/home_admin_view.dart';
import 'package:school_erp/ui/view/main_admin_view/main_admin_controller.dart';
import 'package:school_erp/ui/view/main_admin_view/main_admin_widgets/bottom_navigation_widget.dart';
import 'package:school_erp/ui/view/main_admin_view/program_view/program_view.dart';
import 'package:school_erp/ui/view/main_admin_view/register_view/register_view.dart';

class MainAdminView extends StatefulWidget {
  const MainAdminView({super.key});

  @override
  State<MainAdminView> createState() => _MainAdminViewState();
}

class _MainAdminViewState extends State<MainAdminView> {
  MainAdminController controller = MainAdminController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.mainWhiteColor,
      bottomNavigationBar: Obx(() {
        return BottomNavigationWidget(
          bottomNavigation: controller.selected.value,
          onTap: (select, pageNumber) {
            controller.selected.value = select;
            controller.pageController.jumpToPage(pageNumber);
          },
        );
      }),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: controller.pageController,
        children: [
          RegisterView(),
          HomeAdminView(),
          ProgramView(),
        ],
      ),
    ));
  }
}
