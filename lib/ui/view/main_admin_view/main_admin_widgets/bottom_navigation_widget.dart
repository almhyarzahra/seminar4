import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:school_erp/enums/bottom_navigation_admin_enum.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/main_admin_view/main_admin_widgets/bottom_navigation_controller.dart';

class BottomNavigationWidget extends StatefulWidget {
  final BottomNavigationAdminEnum bottomNavigation;
  final Function(BottomNavigationAdminEnum, int) onTap;
  const BottomNavigationWidget(
      {super.key, required this.bottomNavigation, required this.onTap});

  @override
  State<BottomNavigationWidget> createState() => _BottomNavigationWidgetState();
}

class _BottomNavigationWidgetState extends State<BottomNavigationWidget> {
  late BottomNavigationController controller;
  @override
  void initState() {
    controller = BottomNavigationController(widget.bottomNavigation);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: AppColors.mainBlue250Color.withOpacity(0.4),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(screenWidth(10)),
            topRight: Radius.circular(screenWidth(10)),
          )),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Obx(() {
            return IconButton(
              onPressed: () {
                widget.onTap(BottomNavigationAdminEnum.REGISTER, 0);
                controller.type1.value = BottomNavigationAdminEnum.REGISTER;
              },
              icon: Icon(Icons.app_registration_rounded,
                  size: screenWidth(12),
                  color: controller.type1.value ==
                          BottomNavigationAdminEnum.REGISTER
                      ? AppColors.mainBlue250Color
                      : AppColors.mainBlackColor.withOpacity(0.3)),
            );
          }),
          Obx(() {
            return IconButton(
                onPressed: () {
                  widget.onTap(BottomNavigationAdminEnum.HOME, 1);
                  controller.type1.value = BottomNavigationAdminEnum.HOME;
                },
                icon: SvgPicture.asset("images/ic_home.svg",
                    width: screenWidth(12),
                    color:
                        controller.type1.value == BottomNavigationAdminEnum.HOME
                            ? AppColors.mainBlue250Color
                            : AppColors.mainBlackColor.withOpacity(0.3)));
          }),
          Obx(() {
            return IconButton(
                onPressed: () {
                  widget.onTap(BottomNavigationAdminEnum.PROGRAM, 2);
                  controller.type1.value = BottomNavigationAdminEnum.PROGRAM;
                },
                icon: Icon(Icons.menu_open,
                    size: screenWidth(12),
                    color: controller.type1.value ==
                            BottomNavigationAdminEnum.PROGRAM
                        ? AppColors.mainBlue250Color
                        : AppColors.mainBlackColor.withOpacity(0.3)));
          })
        ],
      ),
    );
  }
}
