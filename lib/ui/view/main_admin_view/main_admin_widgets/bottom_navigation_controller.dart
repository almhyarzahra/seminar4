import 'package:get/get.dart';
import 'package:school_erp/enums/bottom_navigation_admin_enum.dart';
import 'package:school_erp/services/base_controller.dart';

class BottomNavigationController extends BaseController {
  BottomNavigationController(BottomNavigationAdminEnum type) {
    type1.value = type;
  }
  Rx<BottomNavigationAdminEnum> type1 = BottomNavigationAdminEnum.HOME.obs;
}
