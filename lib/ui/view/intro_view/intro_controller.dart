import 'package:get/get.dart';
import 'package:school_erp/services/base_controller.dart';
import 'package:school_erp/ui/view/login_view/login_view.dart';

class IntroController extends BaseController {
  RxList<String> title = [
    "Organize your time with Seminar",
    "Get the most important news from your college",
    "Get the most important courses\n supporting your materials",
  ].obs;
  RxInt currentIndex = 0.obs;

  void changeCurrentIndex() {
    if (currentIndex != 2)
      currentIndex++;
    else
      Get.off(LoginView());
  }
}
