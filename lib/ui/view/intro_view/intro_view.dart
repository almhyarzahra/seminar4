import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_button.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text_button.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/intro_view/intro_controller.dart';
import 'package:school_erp/ui/view/login_view/login_view.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';

class IntroView extends StatefulWidget {
  const IntroView({super.key});

  @override
  State<IntroView> createState() => _IntroViewState();
}

class _IntroViewState extends State<IntroView> {
  IntroController controller = IntroController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      body: Obx(() {
        return Column(children: [
          Stack(
            children: [
              Container(
                height: screenHeight(2),
                child: Image.asset(
                  "images/intro${controller.currentIndex}.jpg",
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                height: screenHeight(2),
                decoration: BoxDecoration(
                    color: Colors.white,
                    gradient: LinearGradient(
                        begin: FractionalOffset.topCenter,
                        end: FractionalOffset.bottomCenter,
                        colors: [
                          Colors.grey.withOpacity(0.0),
                          Colors.blue.withOpacity(0.8),
                        ],
                        stops: [
                          0.0,
                          1.0
                        ])),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsetsDirectional.only(
                top: screenWidth(5), bottom: screenWidth(5)),
            child: CustomText(
              content: controller.title[controller.currentIndex.value],
              fontWeight: FontWeight.bold,
              fontSize: screenWidth(20),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: EdgeInsetsDirectional.only(bottom: screenWidth(10)),
            child: DotsIndicator(
              dotsCount: 3,
              position: controller.currentIndex.value.toDouble(),
              decorator: DotsDecorator(
                size: Size(screenWidth(35), screenWidth(60)),
                color: AppColors.mainGreyColor,
                activeColor: AppColors.mainBlue250Color,
                activeSize: Size(screenWidth(30), screenWidth(30)),
              ),
            ),
          ),
          CustomButton(
              Text: controller.currentIndex != 2 ? "Next" : "Finish",
              backgroundColor: AppColors.mainBlue250Color,
              onPressed: () {
                controller.changeCurrentIndex();
              }),
          Spacer(),
          Align(
            alignment: Alignment.centerRight,
            child: CustomTextButton(
              onPressed: () {
                Get.off(LoginView());
              },
              decoration: TextDecoration.underline,
              decorationThickness: 2,
              colorText: AppColors.mainBlue250Color,
              text: "Skip",
            ),
          )
        ]);
      }),
    ));
  }
}
