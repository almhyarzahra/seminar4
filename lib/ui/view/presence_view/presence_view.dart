import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:school_erp/models/apis/subject_model.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/take_presence_view/take_presence_view.dart';

class PresenceView extends StatefulWidget {
  const PresenceView({super.key, required this.subject});
  final List<Subjects> subject;

  @override
  State<PresenceView> createState() => _PresenceViewState();
}

class _PresenceViewState extends State<PresenceView> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: CustomText(
          content: "Presence",
          fontWeight: FontWeight.bold,
          fontSize: screenWidth(20),
        ),
        backgroundColor: AppColors.mainGreenColor,
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: AppColors.mainBlackColor,
            )),
      ),
      body: ListView(children: [
        Column(
          children: widget.subject
              .map((e) => Padding(
                    padding: EdgeInsetsDirectional.symmetric(
                        vertical: screenWidth(30), horizontal: screenWidth(30)),
                    child: InkWell(
                      onTap: () {
                        Get.to(TakePresenceView(
                          subjectId: e.id!,
                          category: e.category!,
                          year: e.year!,
                          subjectTimeSchedule: e.subjectTimeSchedule!,
                        ));
                      },
                      child: Container(
                        height: screenHeight(17),
                        decoration: BoxDecoration(
                            color: AppColors.mainGreenColor.withOpacity(0.7),
                            borderRadius: BorderRadius.all(
                                Radius.circular(screenWidth(8)))),
                        child: Padding(
                          padding: EdgeInsetsDirectional.symmetric(
                              horizontal: screenWidth(20)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CustomText(content: e.name!.toCapitalized()),
                              CustomText(
                                  content:
                                      "Year: ${e.year}  Category: ${e.category}")
                            ],
                          ),
                        ),
                      ),
                    ),
                  ))
              .toList(),
        ),
        Image.asset("images/take_presence.png")
      ]),
    ));
  }
}
