import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:school_erp/models/apis/subject_model.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';

class ShowPresenceView extends StatefulWidget {
  const ShowPresenceView({super.key, required this.sbjects});
  final List<Subjects> sbjects;

  @override
  State<ShowPresenceView> createState() => _ShowPresenceViewState();
}

class _ShowPresenceViewState extends State<ShowPresenceView> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: CustomText(
          content: "Presence",
          fontWeight: FontWeight.bold,
          fontSize: screenWidth(20),
        ),
        backgroundColor: AppColors.mainGreenColor,
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: AppColors.mainBlackColor,
            )),
      ),
      body: ListView(
        children: [
          Column(
            children: widget.sbjects.map((e) {
              return Card(
                  elevation: 10,
                  shadowColor: AppColors.mainBlueColor,
                  margin: EdgeInsetsDirectional.symmetric(
                      horizontal: screenWidth(30), vertical: screenWidth(30)),
                  child: Padding(
                    padding: EdgeInsetsDirectional.symmetric(
                        horizontal: screenWidth(30), vertical: screenWidth(20)),
                    child: Container(
                      width: screenWidth(1.2),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText(
                            content: e.name!.toCapitalized(),
                            colorText: AppColors.mainBlue250Color,
                          ),
                          (screenWidth(25)).ph,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              CustomText(
                                content: "Presence: ",
                                colorText: AppColors.mainGreenColor,
                              ),
                              CustomText(
                                  content: e.pivot!.presence!.toString()),
                              CustomText(
                                content: "Absence: ",
                                colorText: AppColors.mainRedColor,
                              ),
                              CustomText(content: e.pivot!.absence!.toString())
                            ],
                          ),
                        ],
                      ),
                    ),
                  ));
            }).toList(),
          ),
          Image.asset("images/Checklist-pana.png")
        ],
      ),
    ));
  }
}
