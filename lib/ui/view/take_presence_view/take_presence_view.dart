import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:school_erp/enums/request_status.dart';
import 'package:school_erp/models/apis/subject_model.dart';
import 'package:school_erp/services/base_controller.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text_button.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';
import 'package:school_erp/ui/view/take_presence_view/take_presence_controller.dart';

import 'take_presence_widgets/take_presence_shimmer.dart';

class TakePresenceView extends StatefulWidget {
  const TakePresenceView(
      {super.key,
      required this.year,
      required this.category,
      required this.subjectId,
      required this.subjectTimeSchedule});
  final int year;
  final int category;
  final int subjectId;
  final List<SubjectTimeSchedule> subjectTimeSchedule;

  @override
  State<TakePresenceView> createState() => _TakePresenceViewState();
}

class _TakePresenceViewState extends State<TakePresenceView> {
  late TakePresenceController controller;
  @override
  initState() {
    controller = Get.put(TakePresenceController(
        year: widget.year,
        category: widget.category,
        subjectId: widget.subjectId,
        subjectTimeSchedule: widget.subjectTimeSchedule));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: AppColors.mainGreenColor,
        onPressed: () {
          Get.bottomSheet(Container(
            decoration: BoxDecoration(
                color: AppColors.mainWhiteColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(screenWidth(8)),
                  topRight: Radius.circular(screenWidth(8)),
                )),
            child: Padding(
                padding: EdgeInsetsDirectional.only(top: screenHeight(30)),
                child: Obx(() {
                  return controller.requestStatus.value == RequestStatus.LOADING
                      ? SizedBox.shrink()
                      : ListView(
                          children: controller.categoryUser.value.data!
                              .map((element) {
                            return Card(
                                elevation: 10,
                                shadowColor: AppColors.mainGreenColor,
                                margin: EdgeInsetsDirectional.symmetric(
                                    horizontal: screenWidth(30),
                                    vertical: screenWidth(30)),
                                child: Padding(
                                  padding: EdgeInsetsDirectional.symmetric(
                                      horizontal: screenWidth(30),
                                      vertical: screenWidth(20)),
                                  child: Container(
                                    width: screenWidth(1.2),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        CustomText(
                                          content:
                                              element.name!.toCapitalized(),
                                          colorText: AppColors.mainBlue250Color,
                                        ),
                                        (screenWidth(25)).ph,
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            CustomText(
                                              content: "Presence: ",
                                              colorText:
                                                  AppColors.mainGreenColor,
                                            ),
                                            CustomText(
                                                content: element.presence
                                                    .toString()),
                                            CustomText(
                                              content: "Absence: ",
                                              colorText: AppColors.mainRedColor,
                                            ),
                                            CustomText(
                                                content:
                                                    element.absence!.toString())
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ));
                          }).toList(),
                        );
                })),
          ));
        },
        tooltip: "View Presence",
        child: Icon(Icons.view_agenda),
      ),
      appBar: AppBar(
        title: CustomText(
          content: "Take Presence",
          fontWeight: FontWeight.bold,
          fontSize: screenWidth(20),
        ),
        backgroundColor: AppColors.mainGreenColor,
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: AppColors.mainBlackColor,
            )),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          controller.onInit();
        },
        child: ListView(children: [
          Obx(() {
            print(controller.openButton);
            return controller.requestStatus.value == RequestStatus.LOADING
                ? TakePresenceShimmer()
                : controller.requestStatus.value == RequestStatus.DEFUALT &&
                        controller.categoryUser.value.message == null
                    ? customFaildConnection(onPressed: () {
                        controller.onInit();
                      })
                    : Column(
                        children: [
                          Column(
                              children: controller
                                  .getAllData()
                                  .map((e) => InkWell(
                                        onTap: () {
                                          controller.checked(
                                              controller.data.indexOf(e));
                                          controller.addUserPresence(
                                              index: controller.data.indexOf(e),
                                              studentId: e.id!);
                                        },
                                        child: Padding(
                                          padding:
                                              EdgeInsetsDirectional.symmetric(
                                                  horizontal: screenWidth(30),
                                                  vertical: screenWidth(50)),
                                          child: Container(
                                            height: screenHeight(17),
                                            decoration: BoxDecoration(
                                                color: AppColors.mainGreenColor
                                                    .withOpacity(0.5),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(
                                                        screenWidth(8)))),
                                            child: Padding(
                                              padding: EdgeInsetsDirectional
                                                  .symmetric(
                                                      horizontal:
                                                          screenWidth(20)),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  CustomText(
                                                      content: e.name
                                                              ?.toCapitalized() ??
                                                          ""),
                                                  Padding(
                                                      padding:
                                                          EdgeInsetsDirectional
                                                              .symmetric(
                                                                  horizontal:
                                                                      screenWidth(
                                                                          20)),
                                                      child: controller.isCheck[
                                                              controller.data
                                                                  .indexOf(e)]
                                                          ? Icon(
                                                              Icons.check_box,
                                                              color: AppColors
                                                                  .mainBlue250Color,
                                                            )
                                                          : Icon(Icons
                                                              .check_box_outline_blank))
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ))
                                  .toList()),
                          Padding(
                              padding: EdgeInsetsDirectional.symmetric(
                                  horizontal: screenWidth(20)),
                              child: ElevatedButton(
                                  onPressed: controller.openButton == false ||
                                          controller.clickState.value == false
                                      ? null
                                      : () {
                                          controller.complete();
                                        },
                                  style: ElevatedButton.styleFrom(
                                    shape: StadiumBorder(),
                                    backgroundColor: AppColors.mainBlueColor,
                                    fixedSize:
                                        Size(screenWidth(1.2), screenWidth(8)),
                                  ),
                                  child: Text(
                                    "Complete",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  )))
                        ],
                      );
          }),
        ]),
      ),
    ));
  }
}
