import 'package:flutter/material.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:shimmer/shimmer.dart';

class TakePresenceShimmer extends StatefulWidget {
  const TakePresenceShimmer({super.key});

  @override
  State<TakePresenceShimmer> createState() => _TakePresenceShimmerState();
}

class _TakePresenceShimmerState extends State<TakePresenceShimmer> {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColors.mainGrey2Color,
      highlightColor: AppColors.mainGreyColor,
      child: Column(
        children: [
          ListView.builder(
            itemCount: 7,
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: EdgeInsetsDirectional.symmetric(
                    horizontal: screenWidth(30), vertical: screenWidth(50)),
                child: Container(
                  height: screenHeight(17),
                  decoration: BoxDecoration(
                      color: AppColors.mainBlackColor,
                      borderRadius:
                          BorderRadius.all(Radius.circular(screenWidth(8)))),
                ),
              );
            },
          ),
          Container(
              height: screenHeight(17),
              width: screenWidth(2),
              decoration: BoxDecoration(
                  color: AppColors.mainBlackColor,
                  borderRadius:
                      BorderRadius.all(Radius.circular(screenWidth(8)))))
        ],
      ),
    );
  }
}
