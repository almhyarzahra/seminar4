import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:school_erp/enums/message_type.dart';
import 'package:school_erp/models/apis/category_user_model.dart'
    as categoryUserModel;
import 'package:school_erp/models/apis/subject_model.dart';
import 'package:school_erp/repositories/user/user_repository.dart';
import 'package:school_erp/services/base_controller.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_button.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_toast.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';

class TakePresenceController extends BaseController {
  TakePresenceController(
      {required int category,
      required int year,
      required int subjectId,
      required List<SubjectTimeSchedule> subjectTimeSchedule}) {
    this.category.value = category;
    this.year.value = year;
    this.subjectId.value = subjectId;
    this.subjectTimeSchedule.value = subjectTimeSchedule;
  }
  Rx<categoryUserModel.CategoryUserModel> categoryUser =
      categoryUserModel.CategoryUserModel().obs;
  RxList<categoryUserModel.Data> data = <categoryUserModel.Data>[].obs;
  RxList<SubjectTimeSchedule> subjectTimeSchedule = <SubjectTimeSchedule>[].obs;
  RxInt category = 0.obs;
  RxInt year = 0.obs;
  RxInt subjectId = 0.obs;
  RxList<int> userId = <int>[].obs;
  RxList<int> userIdAbsence = <int>[].obs;
  RxList<bool> isCheck = <bool>[].obs;
  RxBool openButton = false.obs;
  Rx<DateTime> now = DateTime.now().obs;
  RxBool clickState = store.getStateClick().obs;

  @override
  Future<void> onInit() async {
    await getAllCategoryUser();

    Timer.periodic(Duration(), (timer) {
      checkTimeSubject();
      if (store.getDay() != now.value.day) {
        store.setStateClick(true);
        clickState.value = store.getStateClick();
      }
    });

    super.onInit();
  }

  List<categoryUserModel.Data> getAllData() {
    data.value =
        categoryUser.value.data ?? RxList<categoryUserModel.Data>()().obs;
    return data;
  }

  void checked(int index) {
    isCheck[index] ? isCheck[index] = false : isCheck[index] = true;
  }

  void addUserPresence({required int index, required int studentId}) {
    if (isCheck[index] == true && !userId.contains(studentId)) {
      userId.add(studentId);
      userIdAbsence.remove(studentId);
    } else {
      userId.remove(studentId);
      userIdAbsence.add(studentId);
    }
    // print("p${userId}");
    // print("a${userIdAbsence}");
  }

  void checkTimeSubject() {
    Rx<DateTime> now = DateTime.now().obs;
    Rx<String> dateFormat =
        DateFormat('EEEE').format(now.value).toLowerCase().obs;
    subjectTimeSchedule.forEach((element) {
      if (element.day == dateFormat.value) {
        if (now.value.hour >= int.parse(element.begin!) &&
            now.value.hour <= int.parse(element.end!) - 1) {
          openButton.value = true;
        } else {
          openButton.value = false;
        }
      }
    });
  }

  void complete() async {
    Get.defaultDialog(
        title: "Are you sure you're done?",
        titleStyle: TextStyle(
            color: AppColors.mainBlueColor, fontWeight: FontWeight.bold),
        content: Column(
          children: [
            Padding(
              padding:
                  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(5)),
              child: CustomButton(
                  onPressed: () {
                    runFullLoadingFutureFunction(
                        function: UserRepository()
                            .takePresence(
                                subjectId: subjectId.value,
                                userId: userId,
                                userIdAbsence: userIdAbsence)
                            .then((value) {
                      value.fold((l) {
                        CustomToast.showMessage(
                            message: l, messageType: MessageType.REJECTED);
                        Get.back();
                      }, (r) {
                        CustomToast.showMessage(
                            message: "Finish Take Presence",
                            messageType: MessageType.SUCCSESS);
                        store.setDay(now.value.day);
                        store.setStateClick(false);
                        clickState.value = store.getStateClick();
                        Get.back();
                      });
                    }));
                  },
                  Text: "Yes"),
            ),
            (screenWidth(25)).ph,
            Padding(
              padding:
                  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(5)),
              child: CustomButton(
                  onPressed: () {
                    Get.back();
                  },
                  Text: "No"),
            )
          ],
        ));
  }

  Future<void> getAllCategoryUser() async {
    runLoadingFutureFunction(
        function: UserRepository()
            .getAllCategoryUser(category: category.value, year: year.value)
            .then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: "Wrong Acsses internet",
            messageType: MessageType.REJECTED);
      }, (r) {
        categoryUser.value = r;
        for (int i = 0; i < r.data!.length; i++) {
          isCheck.insert(i, false);
        }
        r.data!.forEach((element) {
          userIdAbsence.add(element.id!);
        });
      });
    }));
  }
}
