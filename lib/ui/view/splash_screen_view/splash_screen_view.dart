import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:school_erp/bloc/auth/auth_bloc.dart';

import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/intro_view/intro_view.dart';
import 'package:school_erp/ui/view/main_admin_view/main_admin_view.dart';
import 'package:school_erp/ui/view/main_view/main_view.dart';
import 'package:school_erp/ui/view/main_admin_view/register_view/register_view.dart';

class SplashScreenView extends StatefulWidget {
  const SplashScreenView({super.key});

  @override
  State<SplashScreenView> createState() => _SplashScreenViewState();
}

class _SplashScreenViewState extends State<SplashScreenView> {
  @override
  void initState() {
    Future.delayed(Duration(seconds: 3)).then((value) {
      context.read<AuthBloc>().state.isAuthenticated
          ? context.read<AuthBloc>().state.user!.isAdmin == 1
              ? Get.off(MainAdminView())
              : Get.off(MainView())
          : Get.off(IntroView());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.mainBlue250Color,
      body: ListView(
        children: [
          screenWidth(2).ph,
          Center(child: Image.asset("images/logo.png")),
          screenWidth(5).ph,
          SpinKitThreeBounce(
            color: AppColors.mainWhiteColor,
          ),
        ],
      ),
    ));
  }
}

extension EmptyPadding on num {
  SizedBox get ph => SizedBox(height: toDouble());
  SizedBox get pw => SizedBox(width: toDouble());
}
