import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:school_erp/models/apis/subject_model.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/shared/utils.dart';

import 'package:school_erp/ui/view/upload_lecture_view/upload_lecture_view.dart';

class SubjectsView extends StatefulWidget {
  const SubjectsView({super.key, required this.subjects, required this.type});
  final List<Subjects> subjects;
  final String type;
  @override
  State<SubjectsView> createState() => _SubjectsViewState();
}

class _SubjectsViewState extends State<SubjectsView> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: CustomText(
          content: "Your Subjects",
          fontWeight: FontWeight.bold,
          fontSize: screenWidth(20),
          colorText: AppColors.mainWhiteColor,
        ),
        backgroundColor: AppColors.mainBlue250Color,
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: AppColors.mainWhiteColor,
            )),
      ),
      body: ListView(
        children: [
          GridView.count(
            crossAxisCount: 3,
            crossAxisSpacing: 5,
            scrollDirection: Axis.vertical,
            physics: ScrollPhysics(),
            shrinkWrap: true,
            children: widget.subjects.map((e) {
              return Align(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        width: screenWidth(6),
                        height: screenHeight(13),
                        decoration: ShapeDecoration(
                            shape: StadiumBorder(),
                            color: AppColors.mainBlue250Color),
                        child: IconButton(
                          onPressed: () {
                            Get.to(UploadLectureView(
                              type: widget.type,
                              subjectId: e.id!,
                              subjectName: e.name!,
                            ));
                          },
                          icon: SvgPicture.asset(
                            "images/homework.svg",
                            color: AppColors.mainWhiteColor,
                            width: screenWidth(18),
                          ),
                          color: AppColors.mainWhiteColor,
                        )),
                    CustomText(
                      content: e.name!.toCapitalized(),
                    )
                  ],
                ),
              );
            }).toList(),
          ),
          Padding(
            padding: EdgeInsetsDirectional.only(top: 20),
            child: widget.type == "Subject"
                ? Image.asset(
                    "images/exam1.png",
                    height: screenWidth(1.5),
                  )
                : Image.asset(
                    "images/course.png",
                    height: screenWidth(1.5),
                  ),
          )
        ],
      ),
    ));
  }
}
