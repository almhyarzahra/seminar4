import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/main_view/home_doctor_view/home_doctor_controller.dart';
import 'package:school_erp/ui/view/profile_view/profile_controller.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';

class ProfileView extends StatefulWidget {
  const ProfileView(
      {super.key,
      required this.image,
      required this.name,
      required this.email,
      this.category,
      this.year,
      this.specialty,
      required this.id,
      required this.isDoctor});
  final String? image;
  final String name;
  final String email;
  final int? category;
  final int? year;
  final String? specialty;
  final int id;
  final bool isDoctor;

  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  late ProfileController controller;
  @override
  void initState() {
    controller =
        Get.put(ProfileController(id: widget.id, isDoctor: widget.isDoctor));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: CustomText(
          content: "My Profile",
          fontWeight: FontWeight.bold,
          fontSize: screenWidth(20),
          colorText: AppColors.mainWhiteColor,
        ),
        backgroundColor: AppColors.mainBlue250Color,
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: AppColors.mainWhiteColor,
            )),
      ),
      body: ListView(
        children: [
          (screenWidth(20)).ph,
          GestureDetector(
            onTap: () {
              controller.updatePicture();
            },
            child: Center(child: Obx(() {
              print(controller.imagePath.value);
              return Container(
                width: screenWidth(2.5),
                height: screenHeight(5),
                clipBehavior: Clip.hardEdge,
                decoration: BoxDecoration(
                    border: Border.all(color: AppColors.mainBlue250Color),
                    shape: BoxShape.circle),
                child: widget.image != null &&
                        controller.imagePath.value.length == 0
                    ? CachedNetworkImage(
                        imageUrl: controller.baseUrl + widget.image!,
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                        // width: screenWidth(4.5),
                        // height: screenWidth(4.5),
                        fit: BoxFit.fill,
                      )
                    : controller.imagePath.value.length == 0
                        ? Image.asset("images/profile.jpg")
                        : CachedNetworkImage(
                            imageUrl: controller.imagePath.value,
                            placeholder: (context, url) =>
                                CircularProgressIndicator(),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                            width: screenWidth(4.5),
                            height: screenWidth(4.5),
                            fit: BoxFit.fill,
                          ),
              );
            })),
          ),
          Center(child: CustomText(content: "Edit image")),
          Card(
              elevation: 10,
              shadowColor: AppColors.mainBlue250Color,
              margin: EdgeInsetsDirectional.symmetric(
                  horizontal: screenWidth(30), vertical: screenWidth(30)),
              child: Padding(
                padding: EdgeInsetsDirectional.symmetric(
                    horizontal: screenWidth(30), vertical: screenWidth(20)),
                child: Container(
                  width: screenWidth(1.2),
                  child: Row(
                    children: [
                      CustomText(
                        content: "Name:  ",
                        colorText: AppColors.mainBlue250Color,
                      ),
                      CustomText(content: widget.name.toCapitalized())
                    ],
                  ),
                ),
              )),
          Card(
              elevation: 10,
              shadowColor: AppColors.mainBlue250Color,
              margin: EdgeInsetsDirectional.symmetric(
                  horizontal: screenWidth(30), vertical: screenWidth(30)),
              child: Padding(
                padding: EdgeInsetsDirectional.symmetric(
                    horizontal: screenWidth(30), vertical: screenWidth(20)),
                child: Container(
                  width: screenWidth(1.2),
                  child: Row(
                    children: [
                      CustomText(
                        content: "Email:  ",
                        colorText: AppColors.mainBlue250Color,
                      ),
                      CustomText(content: widget.email)
                    ],
                  ),
                ),
              )),
          widget.isDoctor
              ? SizedBox.shrink()
              : Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Card(
                            elevation: 10,
                            shadowColor: AppColors.mainBlue250Color,
                            margin: EdgeInsetsDirectional.symmetric(
                                horizontal: screenWidth(30),
                                vertical: screenWidth(30)),
                            child: Padding(
                              padding: EdgeInsetsDirectional.symmetric(
                                  horizontal: screenWidth(30),
                                  vertical: screenWidth(20)),
                              child: Container(
                                width: screenWidth(3),
                                child: Row(
                                  children: [
                                    CustomText(
                                      content: "Year:  ",
                                      colorText: AppColors.mainBlue250Color,
                                    ),
                                    CustomText(content: widget.year.toString())
                                  ],
                                ),
                              ),
                            )),
                        Card(
                            elevation: 10,
                            shadowColor: AppColors.mainBlue250Color,
                            margin: EdgeInsetsDirectional.symmetric(
                                horizontal: screenWidth(30),
                                vertical: screenWidth(30)),
                            child: Padding(
                              padding: EdgeInsetsDirectional.symmetric(
                                  horizontal: screenWidth(30),
                                  vertical: screenWidth(20)),
                              child: Container(
                                width: screenWidth(3),
                                child: Row(
                                  children: [
                                    CustomText(
                                      content: "Category:  ",
                                      colorText: AppColors.mainBlue250Color,
                                    ),
                                    CustomText(
                                        content: widget.category.toString())
                                  ],
                                ),
                              ),
                            )),
                      ],
                    ),
                    Card(
                        elevation: 10,
                        shadowColor: AppColors.mainBlue250Color,
                        margin: EdgeInsetsDirectional.symmetric(
                            horizontal: screenWidth(30),
                            vertical: screenWidth(30)),
                        child: Padding(
                          padding: EdgeInsetsDirectional.symmetric(
                              horizontal: screenWidth(30),
                              vertical: screenWidth(20)),
                          child: Container(
                            width: screenWidth(1.2),
                            child: Row(
                              children: [
                                CustomText(
                                  content: "Specialty:  ",
                                  colorText: AppColors.mainBlue250Color,
                                ),
                                CustomText(
                                    content: widget.specialty!.toCapitalized())
                              ],
                            ),
                          ),
                        )),
                  ],
                )
        ],
      ),
    ));
  }
}
