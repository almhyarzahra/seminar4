import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:school_erp/enums/message_type.dart';
import 'package:school_erp/repositories/repositories.dart';
import 'package:school_erp/services/base_controller.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_button.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_toast.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/main_view/home_doctor_view/home_doctor_controller.dart';
import 'package:school_erp/ui/view/main_view/home_student_view/home_student_controller.dart';
import 'package:school_erp/utils/laravel_echo/laravel_echo.dart';

class ProfileController extends BaseController {
  ProfileController({required int id, required bool isDoctor}) {
    this.id.value = id;
    this.isDoctor.value = isDoctor;
  }

  final ImagePicker picker = ImagePicker();
  XFile? image;
  CroppedFile? croppedImage = null;
  RxInt id = 0.obs;
  RxBool isDoctor = false.obs;
  final String baseUrl = PusherConfig.hostEndPoint;
  RxString imagePath = ''.obs;
  @override
  void onClose() {
    isDoctor.value
        ? Get.find<HomeDoctorController>().onInit()
        : Get.find<HomeStudentController>().onInit();
    super.onClose();
  }

  void updatePicture() async {
    Get.defaultDialog(
        title: "Select Picture",
        titleStyle: TextStyle(color: AppColors.mainBlue250Color),
        content: Padding(
          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(10)),
          child: Column(
            children: [
              CustomButton(
                onPressed: () async {
                  image = await picker.pickImage(source: ImageSource.camera);
                  croppedImage = await ImageCropper().cropImage(
                    sourcePath: image!.path,
                    cropStyle: CropStyle.rectangle,
                    compressFormat: ImageCompressFormat.jpg,
                    compressQuality: 100,
                    uiSettings: [
                      AndroidUiSettings(
                          toolbarTitle: 'Cropper',
                          toolbarColor: AppColors.mainBlue250Color,
                          toolbarWidgetColor: Colors.white,
                          initAspectRatio: CropAspectRatioPreset.original,
                          lockAspectRatio: false),
                    ],
                  );
                  if (image != null || croppedImage != null) completeUpdate();
                },
                Text: "Camera",
                backgroundColor: AppColors.mainBlue250Color,
              ),
              CustomButton(
                onPressed: () async {
                  image = await picker.pickImage(source: ImageSource.gallery);
                  croppedImage = await ImageCropper().cropImage(
                    sourcePath: image!.path,
                    cropStyle: CropStyle.rectangle,
                    compressFormat: ImageCompressFormat.jpg,
                    compressQuality: 100,
                    uiSettings: [
                      AndroidUiSettings(
                          toolbarTitle: 'Cropper',
                          toolbarColor: AppColors.mainBlue250Color,
                          toolbarWidgetColor: Colors.white,
                          initAspectRatio: CropAspectRatioPreset.original,
                          lockAspectRatio: false),
                    ],
                  );
                  if (image != null || croppedImage != null) completeUpdate();
                },
                Text: "Gallery",
                backgroundColor: AppColors.mainBlue250Color,
              )
            ],
          ),
        ));
  }

  void completeUpdate() {
    if (image != null) {
      runFullLoadingFutureFunction(
          function: UserRepository()
              .updateProfileImage(
                  image:
                      croppedImage != null ? croppedImage!.path : image!.path,
                  title: "profile",
                  userId: id.value)
              .then((value) {
        value.fold((l) {
          completeUpdate();
        }, (r) {
          imagePath.value = baseUrl + r.data!;
          CustomToast.showMessage(
              message: "Succsess", messageType: MessageType.SUCCSESS);
          Get.back();
        });
      }));
    }
  }
}
