import 'package:flutter/material.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';
import 'package:shimmer/shimmer.dart';

class HomeDoctorShimmer extends StatefulWidget {
  const HomeDoctorShimmer({super.key});

  @override
  State<HomeDoctorShimmer> createState() => _HomeDoctorShimmerState();
}

class _HomeDoctorShimmerState extends State<HomeDoctorShimmer> {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        child: Padding(
          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(17)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              (screenWidth(15)).ph,
              Center(
                child: Container(
                  width: screenWidth(1.5),
                  height: screenWidth(12),
                  color: AppColors.mainBlackColor,
                ),
              ),
              (screenWidth(25)).ph,
              Center(
                child: Container(
                  width: screenWidth(2),
                  height: screenWidth(20),
                  color: AppColors.mainBlackColor,
                ),
              ),
              Padding(
                padding: EdgeInsetsDirectional.only(
                    top: screenWidth(20), bottom: screenWidth(20)),
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                        color: AppColors.mainBlackColor,
                        borderRadius:
                            BorderRadius.all(Radius.circular(screenWidth(8)))),
                    width: screenWidth(2),
                    height: screenWidth(11),
                  ),
                ),
              ),
              Center(
                child: Container(
                  decoration: BoxDecoration(
                      color: AppColors.mainBlackColor,
                      borderRadius:
                          BorderRadius.all(Radius.circular(screenWidth(8)))),
                  width: screenWidth(2),
                  height: screenWidth(2),
                ),
              ),
              Padding(
                padding: EdgeInsetsDirectional.only(top: screenWidth(10)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    CircleAvatar(
                      radius: screenWidth(12),
                    ),
                    CircleAvatar(
                      radius: screenWidth(12),
                    ),
                    CircleAvatar(
                      radius: screenWidth(12),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsetsDirectional.only(top: screenWidth(10)),
                child: Container(
                  width: screenWidth(1),
                  height: screenHeight(4),
                  decoration: BoxDecoration(
                      color: AppColors.mainBlackColor,
                      borderRadius:
                          BorderRadius.all(Radius.circular(screenWidth(30)))),
                ),
              ),
            ],
          ),
        ),
        baseColor: AppColors.mainGrey2Color,
        highlightColor: AppColors.mainGreyColor);
  }
}
