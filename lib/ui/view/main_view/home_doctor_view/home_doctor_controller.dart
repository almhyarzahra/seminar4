import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:school_erp/enums/message_type.dart';
import 'package:school_erp/enums/request_status.dart';
import 'package:school_erp/models/apis/subject_model.dart';
import 'package:school_erp/repositories/user/user_repository.dart';
import 'package:school_erp/services/base_controller.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_toast.dart';
import 'package:school_erp/ui/shared/utils.dart';

class HomeDoctorController extends BaseController {
  Rx<SubjectModel> subjectModel = SubjectModel().obs;
  RxList<SubjectTimeSchedule> subjecTimeSchedule = <SubjectTimeSchedule>[].obs;
  RxMap<int, String> substance = <int, String>{}.obs;
  RxMap<int, String> laboratory = <int, String>{}.obs;
  RxMap<int, String> year = <int, String>{}.obs;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();

  RxList<Subjects> subjects = <Subjects>[].obs;
  Rx<DateTime> date = DateTime.now().obs;

  @override
  void onInit() async {
    await getAllSubject();

    super.onInit();
  }

  List<String> getSubjects() {
    subjects =
        subjectModel.value.data?.subjects?.obs ?? RxList<Subjects>()().obs;
    RxList<String> sub = <String>[].obs;
    subjects.map((element) {
      sub.add(element.name?.toCapitalized() ?? "");
    }).toList();
    return sub;
  }

  Map getSubjectToday() {
    Rx<String> dateFormat =
        DateFormat('EEEE').format(date.value).toLowerCase().obs;
    subjects =
        subjectModel.value.data?.subjects?.obs ?? RxList<Subjects>()().obs;

    for (int i = 0; i < subjects.length; i++) {
      subjecTimeSchedule.addAll(subjects[i].subjectTimeSchedule!.map((e) {
        if (e.day == dateFormat.value) {
          return e;
        } else
          return SubjectTimeSchedule();
      }).toList());
    }
    subjecTimeSchedule.removeWhere((element) => element.day == null);
    subjecTimeSchedule.sort((a, b) => b.begin!.compareTo(a.begin!));
    for (int i = 0; i < subjects.length; i++)
      for (int j = 0; j < subjecTimeSchedule.length; j++)
        if (subjects[i].id == subjecTimeSchedule[j].subjectId) {
          if (subjecTimeSchedule[j].begin == "8") {
            substance.addAll({8: subjects[i].name!.toCapitalized()});
            year.addAll({8: subjects[i].year!.toString()});
            laboratory.addAll({8: subjects[i].laboratory!});
          } else if (subjecTimeSchedule[j].begin == "10") {
            substance.addAll({10: subjects[i].name!.toCapitalized()});
            year.addAll({10: subjects[i].year!.toString()});
            laboratory.addAll({10: subjects[i].laboratory!});
          } else if (subjecTimeSchedule[j].begin == "12") {
            substance.addAll({12: subjects[i].name!.toCapitalized()});
            year.addAll({12: subjects[i].year!.toString()});
            laboratory.addAll({12: subjects[i].laboratory!});
          }
        }

    return substance;
  }

  Map getLaboratory() {
    return laboratory;
  }

  Map getYear() {
    return year;
  }

  String getState() {
    Rx<String> dateFormat =
        DateFormat('EEEE').format(date.value).toLowerCase().obs;
    if (dateFormat.value == "friday" || dateFormat.value == "saturday")
      return "Today is your holiday,";
    else
      return "Here is your activity today,";
  }

  Future<void> getAllSubject() async {
    runLoadingFutureFunction(
        function: UserRepository().getSubjectUser().then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: "Wrong acsess internet",
            messageType: MessageType.REJECTED);
      }, (r) {
        subjectModel.value = r;
      });
    }));
  }
}
