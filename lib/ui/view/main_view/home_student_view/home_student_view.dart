import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:school_erp/bloc/auth/auth_bloc.dart';
import 'package:school_erp/bloc/chat/chat_bloc.dart';
import 'package:school_erp/bloc/notification/notification_bloc.dart';
import 'package:school_erp/bloc/user/user_bloc.dart';
import 'package:school_erp/enums/request_status.dart';
import 'package:school_erp/graphql/quers/graphql_queries.dart';
import 'package:school_erp/models/notification.dart';
import 'package:school_erp/repositories/admin_repositories/admin_repository.dart';
import 'package:school_erp/services/base_controller.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/ui.dart';
import 'package:school_erp/ui/view/main_view/drawer.dart';
import 'package:school_erp/ui/view/main_view/home_student_view/home_student_controller.dart';
import 'package:school_erp/ui/view/main_view/home_student_view/home_student_view_widgets/home_student_view_shimmer.dart';
import 'package:school_erp/ui/view/main_view/main_controller.dart';
import 'package:school_erp/ui/view/show_presence_view/show_presence_view.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:school_erp/ui/view/subjects_view/subjects_view.dart';
import 'package:school_erp/utils/laravel_echo/laravel_echo.dart';
import 'package:school_erp/utils/logger.dart';
import 'package:school_erp/utils/onesignal/onesignal.dart';

class HomeStudentView extends StatefulWidget {
  const HomeStudentView({super.key});

  @override
  State<HomeStudentView> createState() => _HomeStudentViewState();
}

class _HomeStudentViewState extends State<HomeStudentView> {
  HomeStudentController controller = Get.put(HomeStudentController());

  @override
  Widget build(BuildContext context) {
    List<notification> a = [];
    var _chatBloc = context.read<ChatBloc>();
    var _notificationBloc = context.read<NotificationBloc>();
    var _userBloc = context.read<UserBloc>();
    void onOpened(OSNotificationOpenedResult result) {
      eLog(result.notification.jsonRepresentation().replaceAll("\\n", "\n"));
    }

    void onReceivedInForeground(OSNotificationReceivedEvent event) {
      final data = event.notification.additionalData;
      wLog(data);
      if (data!['data']['senderName'] == 'admin2') {
        var s = notification.fromJson({
          "senderName": data['data']["senderName"],
          "message": data['data']['message']
        });

        a.add(s);
        BlocProvider.of<NotificationBloc>(context)
            .add(AddNotification(notifications: a));
      }

      try {
        final chatBloc = context.read<ChatBloc>();
      } catch (_) {
        event.complete(null);
      }
    }

    Future<void> promptPolicyPrivacy(int userId, String tagName) async {
      final oneSignalShared = OneSignal.shared;
      bool userProviderPrivacyConsent =
          await oneSignalShared.userProvidedPrivacyConsent();
      if (userProviderPrivacyConsent)
        sendUserTag(userId, tagName);
      else {
        bool requireConsent =
            await oneSignalShared.requiresUserPrivacyConsent();
        if (requireConsent) {
          final accepted =
              await oneSignalShared.promptUserForPushNotificationPermission();

          if (accepted) {
            await oneSignalShared.consentGranted(true);
            sendUserTag(userId, tagName);
          }
        } else {
          sendUserTag(userId, tagName);
        }
      }
    }

    Future<void> setupOneSignal(int userId) async {
      await initOneSignal();
      registerOneSignalEventListener(
          onOpened: onOpened, onReceivedInForeground: onReceivedInForeground);
      promptPolicyPrivacy(userId, "userId");
    }

    var _authBloc = context.read<AuthBloc>();
    return SafeArea(
        child: Scaffold(
      key: controller.key,
      drawer: Obx(() {
        return DrawerScreen(
          email: controller.subjectModel.value.data?.email ?? "",
          name: controller.subjectModel.value.data?.name ?? "",
          image: controller.subjectModel.value.data?.photoPath ?? null,
          category: controller.subjectModel.value.data?.category ?? null,
          year: controller.subjectModel.value.data?.year ?? null,
          specialty:
              controller.subjectModel.value.data?.subjects?[0].specialty ??
                  null,
          id: controller.subjectModel.value.data?.id ?? 0,
          isDoctor: false,
        );
      }),
      body: StartUpContainer(
        onInit: () async {
          // eLog(await AdminRepository().createSubject(
          //     laboratory: "c10",
          //     name: "uuuuuu",
          //     duration: 12,
          //     category: 5,
          //     department: "practical",
          //     dr: 6,
          //     specialty: "programming",
          //     day: "thursday",
          //     year: 5,
          //     end: "2",
          //     begin: "2",
          //     ids: [1, 5]));
          // eLog(await AdminRepository().checkDoctorValidation(
          //     USER_ID: 2, DAY: "asdasd", BEGIN: "8", END: "10"));
          // eLog(await AdminRepository().checkCategoryValidation(
          //     CATEGORY: 5,
          //     DAY: "thursday",
          //     BEGIN: "8",
          //     END: "10",
          //     DEPARTMENT: "practical",
          //     SPECIALTY: "programming",
          //     YEAR: "5"));
          LaravelEcho.init(token: _authBloc.state.token!);
          setupOneSignal(_authBloc.state.user!.id!);
          _chatBloc.add(ChatStarted());
          _userBloc.add(UserStarted());
          _notificationBloc.add(Started(id: _authBloc.state.user!.id!));
        },
        child: RefreshIndicator(
          color: AppColors.mainBlue250Color,
          onRefresh: () async {
            controller.onInit();
          },
          child: ListView(
            children: [
              Obx(() {
                return controller.requestStatus == RequestStatus.LOADING
                    ? HomeStudentViewShimmer()
                    : controller.requestStatus.value == RequestStatus.DEFUALT &&
                            controller.subjectModel.value.message == null
                        ? customFaildConnection(onPressed: () {
                            controller.onInit();
                          })
                        : Padding(
                            padding: EdgeInsetsDirectional.symmetric(
                                horizontal: screenWidth(17)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: EdgeInsetsDirectional.only(
                                      top: screenWidth(40)),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      IconButton(
                                          onPressed: () {
                                            controller.key.currentState!
                                                .openDrawer();
                                          },
                                          icon: Icon(Icons.menu)),
                                      Obx(() {
                                        return CustomText(
                                          content:
                                              "Hi, ${controller.subjectModel.value.data?.name?.split(' ')[0].toCapitalized() ?? ''}",
                                          fontWeight: FontWeight.bold,
                                          fontSize: screenWidth(13),
                                          colorText: AppColors.mainBlackColor,
                                        );
                                      }),
                                      BlocBuilder<NotificationBloc,
                                              NotificationState>(
                                          builder: (context1, state) {
                                        // var aa=state.notifications[state.notifications.length-1].message;
                                        // setState(() {aa=state.notifications[state.notifications.length-1].message;
                                        // });

                                        return Stack(
                                          children: [
                                            BlocBuilder<NotificationBloc,
                                                NotificationState>(
                                              builder: (context, state) {
                                                return PopupMenuButton(
                                                  // offset: Offset(2,2),

                                                  color: Colors.white,
                                                  onCanceled: () {
                                                    BlocProvider.of<
                                                                NotificationBloc>(
                                                            context)
                                                        .add(ClearNotRead());
                                                  },

                                                  // shape: ShapeBorder.lerp(S, b, t),
                                                  position:
                                                      PopupMenuPosition.under,
                                                  constraints: BoxConstraints(),
                                                  itemBuilder:
                                                      (context1) =>
                                                          state.notifications
                                                                      .length ==
                                                                  0
                                                              ? [
                                                                  PopupMenuItem(
                                                                      child:
                                                                          Container(
                                                                    width: 150,
                                                                    height: 150,
                                                                    child:
                                                                        Padding(
                                                                      padding: const EdgeInsetsDirectional
                                                                              .only(
                                                                          start:
                                                                              30.0),
                                                                      child:
                                                                          Column(
                                                                        children: [
                                                                          Lottie.asset(
                                                                              "images/animation_ll4ds07c.json"),
                                                                          Text(
                                                                              'Empty')
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ))
                                                                ]
                                                              : [
                                                                  PopupMenuItem(child:
                                                                      BlocBuilder<
                                                                          NotificationBloc,
                                                                          NotificationState>(
                                                                    builder:
                                                                        (context,
                                                                            state) {
                                                                      return Container(
                                                                        width:
                                                                            300,
                                                                        height:
                                                                            90,
                                                                        child:
                                                                            Card(
                                                                          child:
                                                                              Column(
                                                                            children: [
                                                                              Row(
                                                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                children: [
                                                                                  CustomText(
                                                                                    content: ' ${state.notifications[state.notifications.length - 1].message}',
                                                                                    colorText: AppColors.mainBlue250Color,
                                                                                  ),
                                                                                  Icon(
                                                                                    Icons.notifications,
                                                                                    color: AppColors.mainBlue250Color,
                                                                                  )
                                                                                ],
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      );
                                                                    },
                                                                  )),
                                                                  state.notifications
                                                                              .length >
                                                                          1
                                                                      ? PopupMenuItem(child: BlocBuilder<
                                                                          NotificationBloc,
                                                                          NotificationState>(
                                                                          builder:
                                                                              (context, state) {
                                                                            return Container(
                                                                              width: 300,
                                                                              height: 90,
                                                                              child: Card(
                                                                                child: Column(
                                                                                  children: [
                                                                                    Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                      children: [
                                                                                        CustomText(
                                                                                          content: ' ${state.notifications[state.notifications.length - 2].message}',
                                                                                          colorText: AppColors.mainBlue250Color,
                                                                                        ),
                                                                                        Icon(
                                                                                          Icons.notifications,
                                                                                          color: AppColors.mainBlue250Color,
                                                                                        )
                                                                                      ],
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                            );
                                                                          },
                                                                        ))
                                                                      : PopupMenuItem(
                                                                          child:
                                                                              SizedBox(
                                                                          width:
                                                                              10,
                                                                          height:
                                                                              10,
                                                                        )),
                                                                  state.notifications
                                                                              .length >
                                                                          2
                                                                      ? PopupMenuItem(child: BlocBuilder<
                                                                          NotificationBloc,
                                                                          NotificationState>(
                                                                          builder:
                                                                              (context, state) {
                                                                            return Container(
                                                                              width: 300,
                                                                              height: 90,
                                                                              child: Card(
                                                                                child: Column(
                                                                                  children: [
                                                                                    Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                      children: [
                                                                                        CustomText(
                                                                                          content: ' ${state.notifications[state.notifications.length - 3].message}',
                                                                                          colorText: AppColors.mainBlue250Color,
                                                                                        ),
                                                                                        Icon(
                                                                                          Icons.notifications,
                                                                                          color: AppColors.mainBlue250Color,
                                                                                        )
                                                                                      ],
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                            );
                                                                          },
                                                                        ))
                                                                      : PopupMenuItem(
                                                                          child:
                                                                              SizedBox(
                                                                          width:
                                                                              10,
                                                                          height:
                                                                              10,
                                                                        )),
                                                                  state.notifications
                                                                              .length >
                                                                          1
                                                                      ? PopupMenuItem(child: BlocBuilder<
                                                                          NotificationBloc,
                                                                          NotificationState>(
                                                                          builder:
                                                                              (context, state) {
                                                                            return Center(
                                                                              child: Container(
                                                                                width: 300,
                                                                                height: 40,
                                                                                child: TextButton(
                                                                                  child: Text(
                                                                                    'Show More',
                                                                                    style: TextStyle(color: Colors.white),
                                                                                  ),
                                                                                  onPressed: () {},
                                                                                ),
                                                                              ),
                                                                            );
                                                                          },
                                                                        ))
                                                                      : PopupMenuItem(
                                                                          child:
                                                                              SizedBox(
                                                                          width:
                                                                              10,
                                                                          height:
                                                                              10,
                                                                        )),
                                                                ],
                                                  child: Container(
                                                      width: 50,
                                                      height: 30,
                                                      child: SvgPicture.asset(
                                                        "images/bell.svg",
                                                      )),
                                                );
                                              },
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsetsDirectional
                                                      .only(top: 15.0),
                                              child: CircleAvatar(
                                                child: Text('${state.notRead}',
                                                    style: TextStyle(
                                                        color: Colors.white)),
                                                backgroundColor: Colors.red,
                                                radius: 10,
                                              ),
                                            )
                                          ],
                                        );
                                      }),
                                    ],
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    CustomText(
                                      content: controller.getState(),
                                      colorText: AppColors.mainGreyColor,
                                    ),
                                    Lottie.asset("images/arrow.json",
                                        height: screenHeight(30))
                                  ],
                                ),
                                Padding(
                                  padding: EdgeInsetsDirectional.only(
                                      top: screenWidth(20),
                                      bottom: screenWidth(20)),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      ConItem(
                                          numberPer:
                                              "${controller.getPresence()}%",
                                          text: "Presence",
                                          colorNumber:
                                              controller.getPresence() >= 50
                                                  ? AppColors.mainGreenColor
                                                  : AppColors.mainRedColor),
                                      ConItem(
                                          numberPer:
                                              "${controller.getTotalSubjects()}",
                                          text: "Total Subject",
                                          colorNumber:
                                              AppColors.mainGreenColor),
                                    ],
                                  ),
                                ),
                                Center(
                                  child: Lottie.asset(
                                      "images/student_page.json",
                                      height: screenHeight(6.5),
                                      width: screenWidth(1)),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Column(
                                      children: [
                                        IconButton(
                                            iconSize: screenWidth(7),
                                            onPressed: () {
                                              Get.to(SubjectsView(
                                                type: "Course",
                                                subjects: controller.subjects,
                                              ));
                                            },
                                            icon: SvgPicture.asset(
                                                "images/course.svg")),
                                        CustomText(content: "Course"),
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        IconButton(
                                            iconSize: screenWidth(7),
                                            onPressed: () {
                                              Get.to(SubjectsView(
                                                type: "Subject",
                                                subjects: controller.subjects,
                                              ));
                                            },
                                            icon: SvgPicture.asset(
                                                "images/subjects.svg")),
                                        CustomText(content: "Subjects"),
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        IconButton(
                                            iconSize: screenWidth(7),
                                            onPressed: () {
                                              Get.to(ShowPresenceView(
                                                sbjects: controller.subjects,
                                              ));
                                            },
                                            icon: SvgPicture.asset(
                                                "images/presence.svg")),
                                        CustomText(content: "Presence"),
                                      ],
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: EdgeInsetsDirectional.only(
                                      top: screenWidth(30),
                                      bottom: screenWidth(50)),
                                  child: CustomText(
                                    content: "Schedule",
                                    fontWeight: FontWeight.bold,
                                    fontSize: screenWidth(15),
                                  ),
                                ),
                                Container(
                                  height: screenHeight(3.5),
                                  child: ListView.builder(
                                    itemCount: 15,
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return index >= 8 &&
                                              index % 2 == 0 &&
                                              index != 14
                                          ? Column(
                                              children: [
                                                Stack(
                                                  children: [
                                                    Row(
                                                      children: [
                                                        index == 10 ||
                                                                index == 12 ||
                                                                index == 14
                                                            ? SizedBox.shrink()
                                                            : Column(
                                                                children: [
                                                                  CustomText(
                                                                      content:
                                                                          "$index"),
                                                                  Container(
                                                                    height:
                                                                        screenWidth(
                                                                            2),
                                                                    child:
                                                                        VerticalDivider(
                                                                      color: AppColors
                                                                          .mainGreyColor,
                                                                      thickness:
                                                                          1,
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                        (screenWidth(6)).pw,
                                                        index == 14
                                                            ? SizedBox.shrink()
                                                            : Column(
                                                                children: [
                                                                  CustomText(
                                                                      content:
                                                                          "${index + 1}"),
                                                                  Container(
                                                                    height:
                                                                        screenWidth(
                                                                            2),
                                                                    child:
                                                                        VerticalDivider(
                                                                      color: AppColors
                                                                          .mainGreyColor,
                                                                      thickness:
                                                                          1,
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                        (screenWidth(6)).pw,
                                                        index == 14
                                                            ? SizedBox.shrink()
                                                            : Column(
                                                                children: [
                                                                  CustomText(
                                                                      content:
                                                                          "${index + 2}"),
                                                                  Container(
                                                                    height:
                                                                        screenWidth(
                                                                            2),
                                                                    child:
                                                                        VerticalDivider(
                                                                      color: AppColors
                                                                          .mainGreyColor,
                                                                      thickness:
                                                                          1,
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                      ],
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsetsDirectional
                                                          .only(
                                                              start: index == 10
                                                                  ? 0
                                                                  : screenWidth(
                                                                      35),
                                                              top: index == 10
                                                                  ? screenWidth(
                                                                      5)
                                                                  : index == 12
                                                                      ? screenWidth(
                                                                          3.3)
                                                                      : screenWidth(
                                                                          10)),
                                                      child:
                                                          controller.getSubjectToday()[
                                                                      index] ==
                                                                  null
                                                              ? SizedBox
                                                                  .shrink()
                                                              : Container(
                                                                  width:
                                                                      screenWidth(
                                                                          2.5),
                                                                  height:
                                                                      screenWidth(
                                                                          10),
                                                                  color: index ==
                                                                          8
                                                                      ? AppColors
                                                                          .mainPurpleBackGroundColor
                                                                          .withOpacity(
                                                                              0.6)
                                                                      : index ==
                                                                              10
                                                                          ? AppColors.mainBlueBackGroundColor.withOpacity(
                                                                              0.9)
                                                                          : AppColors
                                                                              .mainBlue100BackGroundColor
                                                                              .withOpacity(0.9),
                                                                  child: Center(
                                                                      child:
                                                                          Column(
                                                                    children: [
                                                                      CustomText(
                                                                        content:
                                                                            "${controller.getSubjectToday()[index]}",
                                                                        colorText: index ==
                                                                                8
                                                                            ? AppColors.mainYellowColor
                                                                            : index == 10
                                                                                ? AppColors.mainBlue10Color
                                                                                : AppColors.mainBlue20Color,
                                                                      ),
                                                                      Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.center,
                                                                        children: [
                                                                          CustomText(
                                                                            content:
                                                                                "${controller.getDepartment()[index]}",
                                                                            fontSize:
                                                                                screenWidth(30),
                                                                            colorText:
                                                                                AppColors.mainGreyColor,
                                                                          ),
                                                                          (screenWidth(20))
                                                                              .pw,
                                                                          CustomText(
                                                                            content:
                                                                                "${controller.getLaboratory()[index]}",
                                                                            fontSize:
                                                                                screenWidth(35),
                                                                            colorText:
                                                                                AppColors.mainGreyColor,
                                                                          )
                                                                        ],
                                                                      )
                                                                    ],
                                                                  )),
                                                                ),
                                                    )
                                                  ],
                                                ),
                                              ],
                                            )
                                          : SizedBox.shrink();
                                    },
                                  ),
                                ),
                              ],
                            ),
                          );
              }),
            ],
          ),
        ),
      ),
    ));
  }

  Widget ConItem({
    required String numberPer,
    required String text,
    required Color colorNumber,
  }) {
    return Container(
      height: screenWidth(4.5),
      width: screenWidth(2.5),
      decoration: BoxDecoration(
          color: AppColors.mainBlueColor.withOpacity(0.5),
          borderRadius: BorderRadius.all(Radius.circular(screenWidth(30)))),
      child: Padding(
        padding: EdgeInsetsDirectional.only(start: screenWidth(15)),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(
                content: numberPer,
                fontWeight: FontWeight.bold,
                fontSize: screenWidth(12),
                colorText: colorNumber,
              ),
              CustomText(
                content: text,
                colorText: AppColors.mainBlackColor,
              ),
            ]),
      ),
    );
  }
}
