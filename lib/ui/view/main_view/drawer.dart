import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:school_erp/bloc/blocs.dart';
import 'package:school_erp/cubit/guest_cubit.dart';
import 'package:school_erp/enums/message_type.dart';

import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_button.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_toast.dart';

import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/login_view/login_view.dart';
import 'package:school_erp/ui/view/main_view/home_doctor_view/home_doctor_controller.dart';
import 'package:school_erp/ui/view/main_view/home_student_view/home_student_controller.dart';
import 'package:school_erp/ui/view/profile_view/profile_view.dart';
import 'package:school_erp/ui/view/reset_password_view/reset_password_view.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({
    super.key,
    required this.name,
    required this.email,
    required this.image,
    this.category,
    this.year,
    this.specialty,
    required this.id,
    required this.isDoctor,
  });
  final String name;
  final String email;
  final String? image;
  final int? category;
  final int? year;
  final String? specialty;
  final int id;
  final bool isDoctor;

  @override
  State<DrawerScreen> createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(1.2),
      height: double.infinity,
      decoration: BoxDecoration(
          color: AppColors.mainBlue250Color.withOpacity(0.9),
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(screenWidth(8)),
              bottomRight: Radius.circular(screenWidth(8)))),
      child: Padding(
        padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            screenWidth(10).ph,
            Image.asset(
              "images/drawer.png",
              width: screenWidth(1),
            ),
            CustomText(
              content:
                  widget.name == "" ? widget.name : widget.name.toCapitalized(),
              colorText: AppColors.mainWhiteColor,
            ),
            CustomText(
              content: widget.email,
              colorText: AppColors.mainWhiteColor,
            ),
            screenWidth(10).ph,
            ListTile(
              title: CustomText(
                content: "Profile",
                fontSize: screenWidth(20),
                colorText: AppColors.mainWhiteColor,
              ),
              trailing: Icon(
                Icons.arrow_forward,
                color: AppColors.mainWhiteColor,
              ),
              onTap: () async {
                Get.back();
                await Get.to(ProfileView(
                  image: widget.image,
                  name: widget.name,
                  email: widget.email,
                  id: widget.id,
                  year: widget.year,
                  category: widget.category,
                  specialty: widget.specialty,
                  isDoctor: widget.isDoctor,
                ));
              },
            ),
            screenWidth(10).ph,
            ListTile(
              title: CustomText(
                content: "Change Password",
                fontSize: screenWidth(20),
                colorText: AppColors.mainWhiteColor,
              ),
              trailing: Icon(
                Icons.arrow_forward,
                color: AppColors.mainWhiteColor,
              ),
              onTap: () {
                Get.back();
                Get.to(ResetPasswordView());
              },
            ),
            screenWidth(10).ph,
            ListTile(
              title: CustomText(
                content: "LogOut",
                fontSize: screenWidth(20),
                colorText: AppColors.mainWhiteColor,
              ),
              trailing: Icon(
                Icons.arrow_forward,
                color: AppColors.mainWhiteColor,
              ),
              onTap: () {
                Get.defaultDialog(
                    title: "Are you sure you want to leave?",
                    titleStyle: TextStyle(color: AppColors.mainBlue250Color),
                    content: Column(
                      children: [
                        BlocConsumer<AuthBloc, AuthState>(
                            builder: (context, state) => Padding(
                                  padding: EdgeInsetsDirectional.symmetric(
                                      horizontal: screenWidth(5)),
                                  child: CustomButton(
                                    backgroundColor: AppColors.mainBlue250Color,
                                    onPressed: () {
                                      // vLog();
                                      context
                                          .read<GuestCubit>()
                                          .signOut(context)
                                          .then((value) {
                                        if (value == "No Internet Connection") {
                                          CustomToast.showMessage(
                                              message: value,
                                              messageType:
                                                  MessageType.REJECTED);
                                        }
                                      });
                                    },
                                    Text: "Yes",
                                  ),
                                ),
                            listener: (context, state) {
                              if (!state.isAuthenticated) {
                                Get.back();
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (_) => LoginView()));
                              }
                            }),
                        Padding(
                          padding: EdgeInsetsDirectional.symmetric(
                              horizontal: screenWidth(5)),
                          child: CustomButton(
                              backgroundColor: AppColors.mainBlue250Color,
                              onPressed: () {
                                Get.back();
                              },
                              Text: "No"),
                        )
                      ],
                    ));
              },
            )
          ],
        ),
      ),
    );
  }
}
