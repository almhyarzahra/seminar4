import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:dash_chat_2/dash_chat_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pusher_client/pusher_client.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/chat_widget/startup_cotainer.dart';
import 'package:school_erp/utils/logger.dart';

import '../../../../bloc/blocs.dart';
import '../../../../models/models.dart';
import '../../../../utils/utils.dart';

class Chat extends StatefulWidget {
  const Chat({Key? key}) : super(key: key);

  @override
  State<Chat> createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  List<ChatMessage> samples = <ChatMessage>[
    ChatMessage(
        user: ChatUser(id: '1', firstName: 'ibrahem', lastName: 'ali'),
        createdAt: DateTime(2021, 01, 09, 12),
        text: 'hello'),
    ChatMessage(
        user: ChatUser(id: '1', firstName: 'ibrahem', lastName: 'ali'),
        createdAt: DateTime(2021, 01, 09, 12),
        text: 'mhyar'),
    ChatMessage(
        user: ChatUser(id: '2', firstName: 'mhyar', lastName: 'zahra'),
        createdAt: DateTime(2021, 01, 09, 12, 2),
        text: 'hello'),
    ChatMessage(
        user: ChatUser(id: '2', firstName: 'mhyar', lastName: 'zahra'),
        createdAt: DateTime(2021, 01, 09, 12, 2),
        text: 'ibrahem'),
  ];

  @override
  Widget build(BuildContext context) {
    ChatBloc chatBloc = context.read<ChatBloc>();
    AuthBloc authBloc = context.read<AuthBloc>();
    void _handleNewMessage(Map<String, dynamic> data) {
      iLog("handle");
      final chatBloc = context.read<ChatBloc>();
      final selectedChat = chatBloc.state.selectedChat!;
      if (selectedChat.id == data['chat_id']) {
        final chatMessage = ChatMessageEntity.fromJson(data['message']);

        chatBloc.add(AddNewMessage(message: chatMessage));
      }
    }

    void listenChatChannel(ChatEntity chat) {
      iLog("listen");
      try {
        LaravelEcho.instance.private('chat.${chat.id}').listen('.message.sent',
            (e) {
          eLog("message.sent");
          if (e is PusherEvent) {
            _handleNewMessage(jsonDecode(e.data!));
          }
        });
      } catch (e) {
        iLog(e);
      }
    }

    void leaveChatChannel(ChatEntity chat) {
      try {
        LaravelEcho.instance.leave('chat.${chat.id}');
      } catch (err) {}
    }

    return StartUpContainer(
      child: BlocConsumer<ChatBloc, ChatState>(
          listener: (context, state) {},
          builder: (context, state) {
            return Scaffold(
                appBar: AppBar(
                    backgroundColor: AppColors.mainBlue250Color,
                    title: BlocConsumer<ChatBloc, ChatState>(
                        listenWhen: (previous, current) => true,
                        builder: (context, state) {
                          final chat = state.selectedChat;

                          return Text((chat == null)
                              ? 'NA'
                              : getChatName(
                                  chat.participants, authBloc.state.user!));
                        },
                        listener: (context, state) {
                          if (state.selectedChat != null) {
                            iLog("listen to the caht");
                            listenChatChannel(state.selectedChat!);
                          }
                        })),
                body: state.status.isFetching
                    ? Center(child: CircularProgressIndicator())
                    : DashChat(
                        currentUser: ChatUser(
                            id: '${authBloc.state.user!.id}',
                            firstName: 'ali',
                            lastName: 'ali'),
                        onSend: (ChatMessage chatMessage) {
                          vLog(chatMessage.text);
                          // wLog(state.chatMessages[state.chatMessages.length-1].id);
                          chatBloc.add(SendMessage(
                            user: authBloc.state.user!,
                            message_id: state.chatMessages.length == 0
                                ? 1
                                : state
                                    .chatMessages[state.chatMessages.length - 1]
                                    .id,
                            socketId: LaravelEcho.socketId,
                            message: chatMessage,
                            chatId: state.selectedChat!.id,
                          ));
                        },
                        messages: state.getChatMessage,
                        messageListOptions:
                            MessageListOptions(onLoadEarlier: () async {
                          chatBloc.add(LoadMoreChatMessage());
                        }),
                        messageOptions: MessageOptions(
                          containerColor: AppColors.mainBlueBackGroundColor,
                          currentUserContainerColor: AppColors.mainBlue250Color,
                          showOtherUsersAvatar: true,
                        ),
                      ));
          }),
      onInit: () {
        BlocProvider.of<ChatBloc>(context).add(GetChateMessage());

        if (chatBloc.state.selectedChat != null) {
          // wLog(chatBloc.state.selectedChat!);
          // listenChatChannel(chatBloc.state.selectedChat!);
        }
      },
      onDispose: () {
        leaveChatChannel(chatBloc.state.selectedChat!);

        chatBloc.add(ChatReset());
        chatBloc.add(ChatStarted());
      },
    );
  }
}
