import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_erp/bloc/auth/auth_bloc.dart';
import 'package:school_erp/utils/chat.dart';
import 'package:school_erp/utils/formatting.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../../../../models/models.dart';

class ChatListItem extends StatelessWidget {
  final UserModel user;

  final ChatEntity chat_item;
  final Function(ChatEntity) onPressed;

  ChatListItem(
      {Key? key,
      required this.user,
      required this.chat_item,
      required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 80,
      child: ListTile(
        leading: Container(
          width: 60,
          height: 60,
          clipBehavior: Clip.hardEdge,
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(27)),
          child: Container(
            child: Image.asset(
              'images/profile.jpg',
              fit: BoxFit.fill,
            ),
          ),
        ),
        subtitle: Row(children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsetsDirectional.only(
                    top: 12, bottom: 4, end: 0),
                child: Container(
                    width: 190,
                    height: 30,
                    child: Text(
                        chat_item.name ??
                            getChatName(chat_item.participants, user),
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.black))),
              ),
              Padding(
                padding: const EdgeInsetsDirectional.only(end: 60.0),
                child: Container(
                  width: 120,
                  height: 20,
                  child: FittedBox(
                      clipBehavior: Clip.hardEdge,
                      child: Text(
                        overflow: TextOverflow.ellipsis,
                        chat_item.last_message?.message ?? '..',
                        maxLines: 1,
                        style: TextStyle(color: Colors.black),
                      )),
                ),
              ),
            ],
          ),
          Container(
              width: 0,
              child: FittedBox(
                  clipBehavior: Clip.hardEdge,
                  child: Padding(
                    padding:
                        const EdgeInsetsDirectional.only(top: 65.0, start: 30),
                    child: Container(
                        child: Text(utcToLocal(chat_item.updated_at))),
                  )))
        ]),
        onTap: () => onPressed(chat_item),
      ),
    );
  }
}
