import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:school_erp/bloc/blocs.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/view/main_view/chat/chat_list.dart';
import 'package:school_erp/ui/view/main_view/home_doctor_view/home_doctor_view.dart';
import 'package:school_erp/ui/view/main_view/home_student_view/home_student_view.dart';
import 'package:school_erp/ui/view/main_view/live_stream_view/live_stream_view.dart';
import 'package:school_erp/ui/view/main_view/main_controller.dart';
import 'package:school_erp/ui/view/main_view/main_view_widgets/bottom_navigation_widget.dart';

import '../../../livestream/presentation/global_widgets/livestream_view._wrapper.dart';

class MainView extends StatefulWidget {
  const MainView({super.key});

  @override
  State<MainView> createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  MainController controller = MainController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.mainWhiteColor,
      bottomNavigationBar: Obx(() {
        return BottomNavigationWidget(
          bottomNavigation: controller.selected.value,
          onTap: (select, pageNumber) {
            controller.selected.value = select;
            controller.pageController.jumpToPage(pageNumber);
          },
        );
      }),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: controller.pageController,
        children: [
          ChatListScreen(),
          context.read<AuthBloc>().state.user!.isDoctor == 1
              ? HomeDoctorView()
              : HomeStudentView(),
          getLiveStreamView(context.read<AuthBloc>().state.user!.isDoctor == 1),
        ],
      ),
    ));
  }
}
