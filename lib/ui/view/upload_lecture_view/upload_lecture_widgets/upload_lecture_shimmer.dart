import 'package:flutter/material.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:shimmer/shimmer.dart';

class UploadLectureShimmer extends StatefulWidget {
  const UploadLectureShimmer({super.key});

  @override
  State<UploadLectureShimmer> createState() => _UploadLectureShimmerState();
}

class _UploadLectureShimmerState extends State<UploadLectureShimmer> {
  List<int> index = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        baseColor: AppColors.mainGrey2Color,
        highlightColor: AppColors.mainGreyColor,
        child: GridView.count(
          crossAxisCount: 3,
          crossAxisSpacing: 5,
          scrollDirection: Axis.vertical,
          physics: ScrollPhysics(),
          shrinkWrap: true,
          children: index.map((e) {
            return Align(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: screenWidth(6),
                    height: screenHeight(13),
                    decoration: ShapeDecoration(
                        shape: StadiumBorder(),
                        color: AppColors.mainBlue250Color),
                  ),
                ],
              ),
            );
          }).toList(),
        ));
  }
}
