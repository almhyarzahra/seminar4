import 'package:flutter/material.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:shimmer/shimmer.dart';

class UploadCourseShimmer extends StatefulWidget {
  const UploadCourseShimmer({super.key});

  @override
  State<UploadCourseShimmer> createState() => _UploadCourseShimmerState();
}

class _UploadCourseShimmerState extends State<UploadCourseShimmer> {
  List<int> index = [0, 1, 2, 3, 4];

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        baseColor: AppColors.mainGrey2Color,
        highlightColor: AppColors.mainGreyColor,
        child: ListView.builder(
            itemCount: 7,
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              return Card(
                  elevation: 10,
                  margin: EdgeInsetsDirectional.symmetric(
                      horizontal: screenWidth(30), vertical: screenWidth(30)),
                  child: Padding(
                    padding: EdgeInsetsDirectional.symmetric(
                        horizontal: screenWidth(30), vertical: screenWidth(20)),
                    child: Container(
                      width: screenWidth(1),
                      height: screenHeight(10),
                    ),
                  ));
            })
        // GridView.count(
        //   crossAxisCount: 3,
        //   crossAxisSpacing: 5,
        //   scrollDirection: Axis.vertical,
        //   physics: ScrollPhysics(),
        //   shrinkWrap: true,
        //   children: index.map((e) {
        //     return Card(
        //         elevation: 10,
        //         margin: EdgeInsetsDirectional.symmetric(
        //             horizontal: screenWidth(30), vertical: screenWidth(30)),
        //         child: Padding(
        //           padding: EdgeInsetsDirectional.symmetric(
        //               horizontal: screenWidth(30), vertical: screenWidth(20)),
        //           child: Container(
        //             width: screenWidth(1.2),
        //           ),
        //         ));
        //   }).toList(),
        // ));
        );
  }
}
