import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:school_erp/bloc/auth/auth_bloc.dart';

import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_button.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text_button.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text_field.dart';

import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';
import 'package:school_erp/ui/view/upload_lecture_view/upload_lecture_controller.dart';
import 'package:school_erp/ui/view/upload_lecture_view/upload_lecture_widgets/upload_course_shimmer.dart';
import 'package:school_erp/ui/view/upload_lecture_view/upload_lecture_widgets/upload_lecture_shimmer.dart';

import '../../../enums/request_status.dart';

class UploadLectureView extends StatefulWidget {
  const UploadLectureView(
      {super.key,
      required this.subjectId,
      required this.subjectName,
      required this.type});
  final int subjectId;
  final String subjectName;
  final String type;

  @override
  State<UploadLectureView> createState() => _UploadLectureViewState();
}

class _UploadLectureViewState extends State<UploadLectureView> {
  late UploadLectureController controller;
  @override
  void initState() {
    controller = Get.put(UploadLectureController(
        subjectName: widget.subjectName,
        subjectId: widget.subjectId,
        type: widget.type));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      floatingActionButton: context.read<AuthBloc>().state.user!.isDoctor == 1
          ? FloatingActionButton(
              backgroundColor: AppColors.mainBlue250Color,
              onPressed: () {
                Get.bottomSheet(Container(
                  height: screenHeight(3.5),
                  decoration: BoxDecoration(
                      color: AppColors.mainWhiteColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(screenWidth(8)),
                        topRight: Radius.circular(screenWidth(8)),
                      )),
                  child: Obx(() {
                    return Column(
                      children: [
                        widget.type == "Subject"
                            ? Padding(
                                padding: EdgeInsetsDirectional.symmetric(
                                    horizontal: screenWidth(4)),
                                child: CustomTextField(
                                    typeInput: TextInputType.number,
                                    controller:
                                        controller.LactureController.value,
                                    labelText: "Lacture Number"),
                              )
                            : Padding(
                                padding: EdgeInsetsDirectional.symmetric(
                                    horizontal: screenWidth(20)),
                                child: Column(
                                  children: [
                                    CustomTextField(
                                        controller: controller
                                            .titleCourseController.value,
                                        labelText: "Title Course"),
                                    CustomTextField(
                                        controller:
                                            controller.LactureController.value,
                                        labelText: "Course Link"),
                                  ],
                                ),
                              ),
                        (screenWidth(35)).ph,
                        widget.type == "Subject"
                            ? GestureDetector(
                                onTap: () async {
                                  controller.selectFile();
                                },
                                child: CircleAvatar(
                                  radius: screenWidth(15),
                                  child: Icon(controller.visible.value == false
                                      ? Icons.file_upload
                                      : Icons.file_copy_outlined),
                                ),
                              )
                            : SizedBox.shrink(),
                        widget.type == "Subject"
                            ? Center(
                                child: CustomText(
                                    content: controller.visible.value == false
                                        ? "Upload File"
                                        : "Edit File"))
                            : SizedBox.shrink(),
                        controller.visible.value == true
                            ? Padding(
                                padding: EdgeInsetsDirectional.symmetric(
                                    horizontal: screenWidth(4)),
                                child: CustomButton(
                                  backgroundColor: AppColors.mainBlue250Color,
                                  onPressed: () {
                                    widget.type == "Subject"
                                        ? controller.uploadFile()
                                        : controller.uploadCourse();
                                  },
                                  Text: "Upload Complete",
                                  height: screenHeight(20),
                                ),
                              )
                            : SizedBox.shrink()
                      ],
                    );
                  }),
                ));
              },
              tooltip: widget.type == "Subject" ? 'Add Lecture' : 'Add Course',
              child: Icon(Icons.add),
            )
          : null,
      appBar: AppBar(
        title: CustomText(
          colorText: AppColors.mainWhiteColor,
          content: widget.type == "Subject"
              ? "${widget.subjectName.toCapitalized()} Lectures"
              : "${widget.subjectName.toCapitalized()} Courses",
          fontWeight: FontWeight.bold,
          fontSize: screenWidth(20),
        ),
        backgroundColor: AppColors.mainBlue250Color,
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: AppColors.mainWhiteColor,
            )),
      ),
      body: RefreshIndicator(
        color: AppColors.mainBlue250Color,
        onRefresh: () async {
          controller.onInit();
        },
        child: ListView(
          children: [
            //(screenWidth(20)).ph,

            Obx(() {
              return controller.requestStatus.value == RequestStatus.LOADING &&
                      widget.type == "Subject"
                  ? UploadLectureShimmer()
                  : controller.requestStatus.value == RequestStatus.LOADING &&
                          widget.type == "Course"
                      ? UploadCourseShimmer()
                      : controller.requestStatus.value ==
                                  RequestStatus.DEFUALT &&
                              controller.lectureModel.value.message == null
                          ? customFaildConnection(onPressed: () {
                              controller.onInit();
                            })
                          : controller.lectureData.length == 0
                              ? Padding(
                                  padding: EdgeInsetsDirectional.only(
                                      top: screenHeight(2.5)),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding:
                                            EdgeInsetsDirectional.symmetric(
                                                horizontal: screenWidth(20)),
                                        child: CustomText(
                                            content: widget.type == "Subject"
                                                ? "No lectures yet"
                                                : "No courses yet"),
                                      ),
                                      Lottie.asset("images/arrow.json",
                                          height: screenHeight(30))
                                    ],
                                  ),
                                )
                              : widget.type == "Subject"
                                  ? GridView.count(
                                      crossAxisCount: 3,
                                      crossAxisSpacing: 5,
                                      scrollDirection: Axis.vertical,
                                      physics: ScrollPhysics(),
                                      shrinkWrap: true,
                                      children: controller.lectureData.map((e) {
                                        return Align(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              e.id != controller.lectureId.value
                                                  ? Container(
                                                      width: screenWidth(6),
                                                      height: screenHeight(13),
                                                      decoration: ShapeDecoration(
                                                          shape:
                                                              StadiumBorder(),
                                                          color: AppColors
                                                              .mainBlue250Color),
                                                      child: IconButton(
                                                          onPressed: () async {
                                                            controller
                                                                .dialogSelectedOption(
                                                                    path:
                                                                        e.path!,
                                                                    id: e.id!,
                                                                    name: e
                                                                        .name!);
                                                          },
                                                          icon: Icon(
                                                            Icons
                                                                .subject_outlined,
                                                            color: AppColors
                                                                .mainWhiteColor,
                                                            size:
                                                                screenWidth(15),
                                                          )),
                                                    )
                                                  : Column(
                                                      children: [
                                                        CircularProgressIndicator(),
                                                        CustomText(
                                                            content:
                                                                "${controller.progress.value}")
                                                      ],
                                                    ),
                                              CustomText(
                                                content:
                                                    e.name!.toCapitalized(),
                                              )
                                            ],
                                          ),
                                        );
                                      }).toList(),
                                    )
                                  : Column(
                                      children:
                                          controller.lectureData.map((element) {
                                      return Card(
                                          elevation: 10,
                                          shadowColor: AppColors.mainBlueColor,
                                          margin:
                                              EdgeInsetsDirectional.symmetric(
                                                  horizontal: screenWidth(30),
                                                  vertical: screenWidth(30)),
                                          child: Padding(
                                            padding:
                                                EdgeInsetsDirectional.symmetric(
                                                    horizontal: screenWidth(30),
                                                    vertical: screenWidth(20)),
                                            child: Container(
                                              width: screenWidth(1.2),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  CustomText(
                                                    content:
                                                        "Title: ${element.name!}",
                                                    colorText: AppColors
                                                        .mainBlue250Color,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                  CustomTextButton(
                                                      decoration: TextDecoration
                                                          .underline,
                                                      decorationThickness: 1,
                                                      onPressed: () {
                                                        controller.showLink(
                                                            path:
                                                                element.path!);
                                                      },
                                                      text:
                                                          "URL: ${element.path!}"),
                                                ],
                                              ),
                                            ),
                                          ));
                                    }).toList());
            })
          ],
        ),
      ),
    ));
  }
}
