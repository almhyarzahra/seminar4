import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_file_downloader/flutter_file_downloader.dart';
import 'package:get/get.dart';
import 'package:school_erp/enums/message_type.dart';
import 'package:school_erp/models/apis/subject_properties_model.dart';
import 'package:school_erp/repositories/user/user_repository.dart';
import 'package:school_erp/services/base_controller.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_button.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_toast.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';
import 'package:school_erp/utils/laravel_echo/laravel_echo.dart';
import 'package:url_launcher/url_launcher.dart';

class UploadLectureController extends BaseController {
  UploadLectureController(
      {required String subjectName,
      required int subjectId,
      required String type}) {
    this.subjectName.value = subjectName;
    this.subjectId.value = subjectId;
    this.type.value = type;
  }
  RxString type = ''.obs;
  Rx<TextEditingController> LactureController = TextEditingController().obs;
  Rx<TextEditingController> titleCourseController = TextEditingController().obs;

  RxInt subjectId = 0.obs;
  FilePickerResult? result = null;
  File? file = null;
  RxBool visible = false.obs;
  RxString subjectName = "".obs;
  RxString data = "".obs;
  Rx<SubjectPropertiesModel> lectureModel = SubjectPropertiesModel().obs;
  RxList<Data> lectureData = <Data>[].obs;
  RxDouble progress = 0.0.obs;
  RxInt lectureId = 0.obs;
  final String baseUrl = PusherConfig.hostEndPoint;
  Uri url = Uri();
  @override
  void onInit() {
    type.value == "Subject"
        ? getLecture()
        : {getCourse(), visible.value = true};
    super.onInit();
  }

  void selectFile() async {
    result = await FilePicker.platform.pickFiles();
    if (result != null) {
      visible.value = true;
    }
  }

  void uploadFile() {
    runFullLoadingFutureFunction(
        function: UserRepository()
            .UploadFile(
                image: result!.paths[0], title: "lecture/${subjectName.value}")
            .then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: "Error, Please Try Agin",
            messageType: MessageType.REJECTED);
      }, (r) {
        data.value = r.data!;

        runFullLoadingFutureFunction(
            function: UserRepository()
                .storeLecture(
                    subjectId: subjectId.value,
                    name: LactureController.value.text.length == 0
                        ? "Lecture"
                        : "Lecture ${LactureController.value.text.toString()}",
                    path: "${baseUrl}${data.value}")
                .then((value) {
          value.fold((l) {
            CustomToast.showMessage(
                message: "Error, Please Try Agin",
                messageType: MessageType.REJECTED);
          }, (r) {
            CustomToast.showMessage(
                message: "Succsess Upload", messageType: MessageType.SUCCSESS);
            runFutureFunction(
                function: UserRepository()
                    .getAllLectureSubject(subjectId: subjectId.value)
                    .then((value) {
              value.fold((l) {
                CustomToast.showMessage(
                    message: "Error,Please Refresh",
                    messageType: MessageType.REJECTED);
              }, (r) {
                lectureModel.value = r;
                lectureData.value = r.data!;
                visible.value = false;
                LactureController.value.text = "";
                Get.back();
              });
            }));
          });
        }));
      });
    }));
  }

  void uploadCourse() {
    titleCourseController.value.text.isEmpty ||
            LactureController.value.text.isEmpty
        ? CustomToast.showMessage(
            message: "Please Fill Field ", messageType: MessageType.WARNING)
        : runFullLoadingFutureFunction(
            function: UserRepository()
                .storeCourses(
                    subjectId: subjectId.value,
                    name: titleCourseController.value.text.toString(),
                    path: LactureController.value.text.toString())
                .then((value) {
            value.fold((l) {
              CustomToast.showMessage(
                  message: "Error, Please Try Agin",
                  messageType: MessageType.REJECTED);
            }, (r) {
              CustomToast.showMessage(
                  message: "Succsess Upload",
                  messageType: MessageType.SUCCSESS);
              runFutureFunction(
                  function: UserRepository()
                      .getAllCoursesSubject(subjectId: subjectId.value)
                      .then((value) {
                value.fold((l) {
                  CustomToast.showMessage(
                      message: "Error,Please Refresh",
                      messageType: MessageType.REJECTED);
                }, (r) {
                  lectureModel.value = r;
                  lectureData.value = r.data!;

                  LactureController.value.text = "";
                  Get.back();
                });
              }));
            });
          }));
  }

  void getCourse() {
    runLoadingFutureFunction(
        function: UserRepository()
            .getAllCoursesSubject(subjectId: subjectId.value)
            .then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: "Wrong acsess internet",
            messageType: MessageType.REJECTED);
      }, (r) {
        lectureModel.value = r;
        lectureData.value = r.data!;

        LactureController.value.text = "";
      });
    }));
  }

  void getLecture() {
    runLoadingFutureFunction(
        function: UserRepository()
            .getAllLectureSubject(subjectId: subjectId.value)
            .then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: "Wrong acsess internet",
            messageType: MessageType.REJECTED);
      }, (r) {
        lectureModel.value = r;
        lectureData.value = r.data!;
        visible.value = false;
        LactureController.value.text = "";
      });
    }));
  }

  void showLink({required String path}) async {
    url = Uri.parse(path);
    if (!await launchUrl(url, mode: LaunchMode.externalApplication)) ;
  }

  void dialogSelectedOption(
      {required String path, required int id, required String name}) {
    Get.defaultDialog(
        title: "What do you want?",
        titleStyle: TextStyle(
            color: AppColors.mainBlue250Color, fontWeight: FontWeight.bold),
        content: Column(
          children: [
            Padding(
              padding:
                  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(10)),
              child: CustomButton(
                backgroundColor: AppColors.mainBlue250Color,
                onPressed: () {
                  lectureId.value = id;

                  FileDownloader.downloadFile(
                      url: path,
                      name: name,
                      onProgress: (String? fileName, double progress) {
                        this.progress.value = progress;
                      },
                      onDownloadCompleted: (String path) {
                        CustomToast.showMessage(
                            message: "Succsess Download",
                            messageType: MessageType.SUCCSESS);
                        lectureId.value = 0;
                      },
                      onDownloadError: (String error) {
                        CustomToast.showMessage(
                            message: "Error ${error}",
                            messageType: MessageType.REJECTED);
                        lectureId.value = 0;
                      });
                  Get.back();
                },
                Text: "Download Lecture",
                TextColor: AppColors.mainWhiteColor,
              ),
            ),
            (screenWidth(25)).ph,
            Padding(
              padding:
                  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(10)),
              child: CustomButton(
                backgroundColor: AppColors.mainBlue250Color,
                onPressed: () async {
                  url = Uri.parse(path);
                  if (!await launchUrl(url,
                      mode: LaunchMode.externalApplication)) ;
                  Get.back();
                },
                Text: "Show Lecture",
                TextColor: AppColors.mainWhiteColor,
              ),
            )
          ],
        ));
  }
}
