import 'package:bot_toast/bot_toast.dart';
import 'package:get/get.dart';
import 'package:school_erp/enums/request_status.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:flutter/material.dart';

class BaseController extends GetxController {
  Rx<RequestStatus> requestStatus = RequestStatus.DEFUALT.obs;
  //Rx<OperationType> operationType = OperationType.NONE.obs;

  Future runFutureFunction({required Future function}) async {
    await function;
  }

  Future runLoadingFutureFunction({
    required Future function,
    //OperationType? type = OperationType.NONE
  }) async {
    requestStatus.value = RequestStatus.LOADING;
    //operationType.value = type!;
    await function;

    requestStatus.value = RequestStatus.DEFUALT;
  }
  //requestStatus.value = RequestStatus.DEFUALT;

  // operationType.value = OperationType.NONE;
}

Future runFullLoadingFutureFunction({
  required Future function,
  bool closeCustom = true,
}) async {
  customLoader();
  await function;
  if (closeCustom == true) BotToast.closeAllLoading();
}
